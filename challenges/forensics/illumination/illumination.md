---
sidebar_position: 1
sidebar_label: 🌟 Illumination
description: A Junior Developer just switched to a new source control platform. Can you find the secret token?
last_update:
  date: 2022/07/10
  author: Raúl Sánchez
tags:
  - smbclient
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
  - HTB Challenge
---

# Illumination

> A Junior Developer just switched to a new source control platform. Can you find the secret token?

## Preparation

First of all, download the `zip` file of the challenge an **unzip** using the password **`hackthebox`**.

```bash
❯ unzip Illumination.zip
Archive:  Illumination.zip
   creating: Illumination.JS/
   creating: Illumination.JS/.git/
[Illumination.zip] Illumination.JS/.git/COMMIT_EDITMSG password: hackthebox
```

## Git

Then we will access the folder and list all its contents.

```bash
❯ cd Illumination.JS
❯ ls -la
drwxr-xr-x raul raul 4.0 KB Thu May 30 22:40:34 2019  .
drwxr-xr-x raul raul 4.0 KB Thu Jul 14 00:12:11 2022  ..
drwxr-xr-x raul raul 4.0 KB Thu Jul 14 00:17:50 2022  .git
.rw-r--r-- raul raul 2.6 KB Thu May 30 22:40:34 2019  bot.js
.rw-r--r-- raul raul 199 B  Thu May 30 22:40:12 2019  config.json
```

After that, we will access the `.git` folder to list the previous **commits** and look for the **token**.

```bash
❯ cd .git
❯ git log
commit edc5aabf933f6bb161ceca6cf7d0d2160ce333ec (HEAD -> master)
Author: SherlockSec <dan@lights.htb>
Date:   Fri May 31 14:16:43 2019 +0100

    Added some whitespace for readability!

commit 47241a47f62ada864ec74bd6dedc4d33f4374699
Author: SherlockSec <dan@lights.htb>
Date:   Fri May 31 12:00:54 2019 +0100

    Thanks to contributors, I removed the unique token as it was a security risk. Thanks for reporting responsibly!

commit ddc606f8fa05c363ea4de20f31834e97dd527381
Author: SherlockSec <dan@lights.htb>
Date:   Fri May 31 09:14:04 2019 +0100

    Added some more comments for the lovely contributors! Thanks for helping out!

commit 335d6cfe3cdc25b89cae81c50ffb957b86bf5a4a
Author: SherlockSec <dan@lights.htb>
Date:   Thu May 30 22:16:02 2019 +0100

    Moving to Git, first time using it. First Commit!
```

It seems that in commit **`47241a47f62ada864ec74bd6dedc4d33f4374699`** the **token** is removed. Let's see what happened...

```bash
❯ git show 47241a47f62ada864ec74bd6dedc4d33f4374699
commit 47241a47f62ada864ec74bd6dedc4d33f4374699
Author: SherlockSec <dan@lights.htb>
Date:   Fri May 31 12:00:54 2019 +0100

    Thanks to contributors, I removed the unique token as it was a security risk. Thanks for reporting responsibly!

diff --git a/config.json b/config.json
index 316dc21..6735aa6 100644
--- a/config.json
+++ b/config.json
@@ -1,6 +1,6 @@
 {
 
-       "token": "SFRCe3YzcnNpMG5fYzBudHIwbF9hbV9JX3JpZ2h0P30=",
+       "token": "Replace me with token when in use! Security Risk!",
        "prefix": "~",
        "lightNum": "1337",
        "username": "UmVkIEhlcnJpbmcsIHJlYWQgdGhlIEpTIGNhcmVmdWxseQ==",
```

## Base64 Decrypt

The token appears to be **`base64`** encrypted. Once it is decrypted we will get the **`flag`**!

```python
❯ echo "SFRCe3YzcnNpMG5fYzBudHIwbF9hbV9JX3JpZ2h0P30=" > token
❯ base64 --decode token
```

I hope you made it too, see you next timee! 🙃
