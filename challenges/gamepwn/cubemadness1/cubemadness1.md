---
sidebar_position: 1
sidebar_label: 🧊 CubeMadness1
description: Gotta collect them all.
last_update:
  date: 2022/07/11
  author: Raúl Sánchez
tags:
  - CheatEngine
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
  - HTB Challenge
---

# CubeMadness1

> Gotta collect them all.

## Preparation

First, we will download and `unzip` the challenge zip file on a **Windows** machine and we will run the application.

![](1cubemadness1.png)

## Cheat Engine

We will download [CheatEngine](https://www.cheatengine.org/) and open the **CubeMadness1** process.

![](1cubemadness2.png)

We will search for the value **0**.

![](1cubemadness3.png)

In the game, we will collect a cube to increase the counter and scan the value **1** again by clicking the `Next Scan` button.

![](1cubemadness4.png)

We will do the same for values **2,3** and **4** until we have three memory addresses. We will select them and **change** their **values**.

![](1cubemadness5.png)

We will assign them the value **20**.

![](1cubemadness6.png)

Back in the **game**, we will see the **flag** in the background.

![](1cubemadness7.png)

I hope you made it too, see you next timee! 🙃
