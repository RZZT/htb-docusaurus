---
sidebar_position: 1
sidebar_label: 🕴 Behind the Scenes
description: "After struggling to secure our secret strings for a long time, we finally figured out the solution to our problem: Make decompilation harder. Now it's impossible to figure out how our programs work!"
last_update:
  date: 2022/07/12
  author: Raúl Sánchez
tags:
  - git
  - Base64 Decrypt
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Behind the Scenes

> After struggling to secure our secret strings for a long time, we finally figured out the solution to our problem: Make decompilation harder. Now it's impossible to figure out how our programs work!

## Preparation

First of all, download the `zip` file of the challenge an **unzip** using the password **`hackthebox`**.

```bash
❯ unzip Behind\ the\ Scenes.zip
Archive:  Behind the Scenes.zip
   creating: rev_behindthescenes/
[Behind the Scenes.zip] rev_behindthescenes/behindthescenes password: 
  inflating: rev_behindthescenes/behindthescenes
```

## Identification

I found this [article](https://www.codementor.io/@packt/reverse-engineering-a-linux-executable-hello-world-rjceryk5d) about reverse engineering. We can identify the file type with the command **file**.

```bash
❯ cd rev_behindthescenes
❯ ls
behindthescenes
❯ file behindthescenes
behindthescenes: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=e60ae4c886619b869178148afd12d0a5428bfe18, for GNU/Linux 3.2.0, not stripped
❯ ./behindthescenes
./challenge <password>
❯ ./behindthescenes test
```

It is a 64-bit ELF file-type. ELF files are native executables on Linux 
platforms. Next stop, let's take a quick look at text strings with the 
**`strings`** command:

```c
❯ strings behindthescenes
/lib64/ld-linux-x86-64.so.2
libc.so.6
strncmp
puts
__stack_chk_fail
printf
strlen
sigemptyset
memset
sigaction
__cxa_finalize
__libc_start_main
GLIBC_2.4
GLIBC_2.2.5
_ITM_deregisterTMCloneTable
__gmon_start__
_ITM_registerTMCloneTable
u+UH
[]A\A]A^A_
./challenge <password>
> HTB{%s}
```

## Radare2 (r2)

We will use [r2](https://www.radare.org/n/radare2.html) with the flags `-w` to open file in write mode and `-A` to analyze all referenced code.

```bash
❯ r2 -w -A behindthescenes
Warning: run r2 with -e bin.cache=true to fix relocations in disassembly
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze function calls (aac)
[x] Analyze len bytes of instructions for references (aar)
[x] Finding and parsing C++ vtables (avrr)
[x] Type matching analysis for all functions (aaft)
[x] Propagate noreturn information (aanr)
[x] Use -AA or aaaa to perform additional experimental analysis.
```

To move around the file we are inspecting we will need to change the **offset** at which we are using the [s](https://book.rada.re/basic_commands/seeking.html) command. Then we will use the command [pd](https://book.rada.re/basic_commands/print_modes.html) to disassemble.

```properties
[0x00001140]> s main
[0x00001261]> pd
            ; DATA XREF from entry0 @ 0x1161
┌ 135: int main (int argc, char **argv);
│           ; var char **var_b0h @ rbp-0xb0
│           ; var int64_t var_a4h @ rbp-0xa4
│           ; var void *s @ rbp-0xa0
│           ; var int64_t var_18h @ rbp-0x18
│           ; var int64_t var_8h @ rbp-0x8
│           ; arg int argc @ rdi
│           ; arg char **argv @ rsi
│           0x00001261      f30f1efa       endbr64
│           0x00001265      55             push rbp
│           0x00001266      4889e5         mov rbp, rsp
│           0x00001269      4881ecb00000.  sub rsp, 0xb0
│           0x00001270      89bd5cffffff   mov dword [var_a4h], edi    ; argc
│           0x00001276      4889b550ffff.  mov qword [var_b0h], rsi    ; argv
│           0x0000127d      64488b042528.  mov rax, qword fs:[0x28]
│           0x00001286      488945f8       mov qword [var_8h], rax
│           0x0000128a      31c0           xor eax, eax
│           0x0000128c      488d8560ffff.  lea rax, [s]
│           0x00001293      ba98000000     mov edx, 0x98               ; size_t n
│           0x00001298      be00000000     mov esi, 0                  ; int c
│           0x0000129d      4889c7         mov rdi, rax                ; void *s
│           0x000012a0      e87bfeffff     call sym.imp.memset         ; void *memset(void *s, int c, size_t n)
│           0x000012a5      488d8560ffff.  lea rax, [s]
│           0x000012ac      4883c008       add rax, 8
│           0x000012b0      4889c7         mov rdi, rax
│           0x000012b3      e878feffff     call sym.imp.sigemptyset
│           0x000012b8      488d056affff.  lea rax, [sym.segill_sigaction] ; 0x1229
│           0x000012bf      48898560ffff.  mov qword [s], rax
│           0x000012c6      c745e8040000.  mov dword [var_18h], 4
│           0x000012cd      488d8560ffff.  lea rax, [s]
│           0x000012d4      ba00000000     mov edx, 0
│           0x000012d9      4889c6         mov rsi, rax
│           0x000012dc      bf04000000     mov edi, 4
│           0x000012e1      e8fafdffff     call sym.imp.sigaction
└           0x000012e6      0f0b           ud2
            0x000012e8      83bd5cffffff.  cmp dword [rbp - 0xa4], 2
        ┌─< 0x000012ef      741a           je 0x130b
        │   0x000012f1      0f0b           ud2
        │   0x000012f3      488d3d0a0d00.  lea rdi, str.._challenge__password_ ; 0x2004 ; "./challenge <password>"
        │   0x000012fa      e8d1fdffff     call sym.imp.puts           ; int puts(const char *s)
        │   0x000012ff      0f0b           ud2
        │   0x00001301      b801000000     mov eax, 1
       ┌──< 0x00001306      e92e010000     jmp 0x1439
       ││   ; CODE XREF from main @ +0x8e
       │└─> 0x0000130b      0f0b           ud2
       │    0x0000130d      488b8550ffff.  mov rax, qword [rbp - 0xb0]
       │    0x00001314      4883c008       add rax, 8
       │    0x00001318      488b00         mov rax, qword [rax]
       │    0x0000131b      4889c7         mov rdi, rax
       │    0x0000131e      e8cdfdffff     call sym.imp.strlen         ; size_t strlen(const char *s)
       │    0x00001323      4883f80c       cmp rax, 0xc
       │┌─< 0x00001327      0f8505010000   jne 0x1432
       ││   0x0000132d      0f0b           ud2
       ││   0x0000132f      488b8550ffff.  mov rax, qword [rbp - 0xb0]
       ││   0x00001336      4883c008       add rax, 8
       ││   0x0000133a      488b00         mov rax, qword [rax]
       ││   0x0000133d      ba03000000     mov edx, 3
       ││   0x00001342      488d35d20c00.  lea rsi, [0x0000201b]       ; "Itz"
       ││   0x00001349      4889c7         mov rdi, rax
       ││   0x0000134c      e86ffdffff     call sym.imp.strncmp        ; int strncmp(const char *s1, const char *s2, size_t n)
       ││   0x00001351      85c0           test eax, eax
      ┌───< 0x00001353      0f85d0000000   jne 0x1429
      │││   0x00001359      0f0b           ud2
      │││   0x0000135b      488b8550ffff.  mov rax, qword [rbp - 0xb0]
      │││   0x00001362      4883c008       add rax, 8
      │││   0x00001366      488b00         mov rax, qword [rax]
      │││   0x00001369      4883c003       add rax, 3
      │││   0x0000136d      ba03000000     mov edx, 3
      │││   0x00001372      488d35a60c00.  lea rsi, [0x0000201f]       ; "_0n"
      │││   0x00001379      4889c7         mov rdi, rax
      │││   0x0000137c      e83ffdffff     call sym.imp.strncmp        ; int strncmp(const char *s1, const char *s2, size_t n)
      │││   0x00001381      85c0           test eax, eax
      │││   0x00001383      0f8597000000   jne 0x1420
```

We will notice that in cell **0x0000201b** it says '**Itz'** let's search from there with the command [x](https://book.rada.re/search_bytes/basic_searches.html). Here we have our **flag**!

```properties
[0x00001261]> x/4s 0x0000201b
0x0000201b Itz
0x0000201e _0n
0x00002022 {FLAG}
0x00002026 {FLAG}
0x0000202a > HTB{%s}
```

I hope you made it too, see you next timee! 🙃


