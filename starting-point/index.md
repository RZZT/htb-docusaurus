---
sidebar_position: 0
sidebar_label: 🚩 Starting Point
last_update:
  date: 2022/05/31
  author: Raúl Sánchez
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
  - Starting Point
  - HTB Starting Point
slug: /
---

# Introduction

Here I will begin with the path of [Starting Point](https://app.hackthebox.com/starting-point). Basically it’s a series of machines rated easy that should be rooted in a sequence.