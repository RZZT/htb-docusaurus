---
sidebar_position: 4
sidebar_label: 🟥 Redeemer
description: This machine was so fun because I had never worked with redis before and it was very interesting to learn its main commands and functionalities.
last_update:
  date: 2022/06/08
  author: Raúl Sánchez
tags:
  - Redis
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Redeemer

> This machine was so fun because I had never worked with redis before and it was very interesting to learn its main commands and functionalities.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Redeemer Machine](redeemer1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.129.40`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.129.40
PING 10.129.129.40 (10.129.129.40) 56(84) bytes of data.
64 bytes from 10.129.129.40: icmp_seq=1 ttl=63 time=133 ms

--- 10.129.129.40 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 132.999/132.999/132.999/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.129.40

10.129.129.40 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::info

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* \-sC : to use built-in scripts.
* \-p- : to scan every TCP port \[0-65535].
* [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
* [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
* [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.

```python
❯ nmap -sV -sC -p- --min-rate 5000 --open -vvv 10.129.129.40
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-08 19:50 CEST
Completed Ping Scan at 19:50, 0.09s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 19:50
Completed Parallel DNS resolution of 1 host. at 19:50, 0.05s elapsed
Initiating Connect Scan at 19:50
Scanning 10.129.129.40 [65535 ports]
Discovered open port 6379/tcp on 10.129.129.40
```

We observe that port 6379/tcp is open. If we search on the web, we will discover that this port belongs to [redis](https://redis.io/).

:::note
**Redis** is an in-memory data structure store, used as a distributed, in-memory key–value database, cache and message broker, with optional durability.
:::

## 2. Foothold

To be able to interact remotely with the Redis server, we need to download the `redis-cli` utility.

```bash
sudo apt install redis-tools
```

Connect to the `redis` server using the following command:

```python
❯ redis-cli -h 10.129.129.40
10.129.129.40:6379> 
```

Use the info command to get information and statistics about the Redis server.

```plsql
10.129.129.40:6379> info
# Server
redis_version:5.0.7
redis_git_sha1:00000000
redis_git_dirty:0
redis_build_id:66bd629f924ac924

·
·
·

# Keyspace
db0:keys=4,expires=0,avg_ttl=0
```

Now, select a Redis logical database by using the `select` command followed by the index number of the database that needs to be selected :

```sql
10.129.129.40:6379> select 0
OK
```

We can list all the keys present in the database using the command :

```sql
10.129.129.40:6379> keys *
1) "flag"
2) "temp"
3) "stor"
4) "numb"
```

Finally, we can view the values stored for a corresponding key using the get command followed by the keynote :&#x20;

```sql
10.129.129.40:6379> get flag
```
