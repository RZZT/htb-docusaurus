---
sidebar_position: 1
sidebar_label: 🐱 Meow
description: This is the first Tier 0 Machine of the Starting Point.
last_update:
  date: 2022/06/08
  author: Raúl Sánchez
tags:
  - Telnet
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Meow

> This is the first Tier 0 Machine of the Starting Point.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Meow Machine](meow1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.253.147`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.253.147
PING 10.129.253.147 (10.129.253.147) 56(84) bytes of data.
64 bytes from 10.129.253.147: icmp_seq=1 ttl=63 time=104 ms

--- 10.129.253.147 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 103.953/103.953/103.953/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.253.147

10.129.253.147 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::info

Then, we will use the [nmap](https://nmap.org/) tool with the [-sV](https://nmap.org/book/man-version-detection.html) parameter to see the services and their version.

```python
❯ nmap -sV 10.129.253.147
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-08 16:22 CEST
Nmap scan report for 10.129.253.147
Host is up (0.13s latency).
Not shown: 999 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
23/tcp open  telnet  Linux telnetd
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 47.28 seconds
```

As we see, this machine has only one port open and that is [telnet](https://www.iana.org/assignments/telnet-options/telnet-options.xhtml).

:::note
**Telnet** is an application protocol used on the Internet or local area network to provide a bidirectional interactive text-oriented communication facility using a virtual terminal connection.
:::

## 2. Foothold

Let's connect to the machine via telnet. Some typical important accounts have self-explanatory names, such as: admin, administrator, **root**.

```python
❯ telnet 10.129.253.147
Trying 10.129.253.147...
Connected to 10.129.253.147.
Escape character is '^]'.

  █  █         ▐▌     ▄█▄ █          ▄▄▄▄
  █▄▄█ ▀▀█ █▀▀ ▐▌▄▀    █  █▀█ █▀█    █▌▄█ ▄▀▀▄ ▀▄▀
  █  █ █▄█ █▄▄ ▐█▀▄    █  █ █ █▄▄    █▌▄█ ▀▄▄▀ █▀█


Meow login: root
Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-77-generic x86_64)
Last login: Mon Sep  6 15:15:23 UTC 2021 from 10.10.14.18 on pts/0
root@Meow:~#
```

Success! We have logged into the target system. We can now use the `ls` command to take a look around the directory.&#x20;

```bash
root@Meow:~# ls
flag.txt  snap

root@Meow:~# cat flag.txt 
```

We can read the file to have the hash value displayed in the terminal using the `cat` command.
