---
sidebar_position: 
sidebar_label: 
description: ''
last_update:
  date: 2022/12/31
  author: Raúl Sánchez
tags:
  - 
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# 

> 

## Preparation

First of all, download the `zip` file of the challenge an **unzip** using the password **`hackthebox`**.

```bash

```
