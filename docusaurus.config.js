const config = {
  title: 'HTB Write-ups',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  favicon: '/icons/hackthebox.svg',
  onBrokenLinks: 'ignore',
  onBrokenMarkdownLinks: 'ignore',
  onDuplicateRoutes: 'ignore',
  tagline: 'Hack The Box Walkthrough',

  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'starting-point',
        path: 'starting-point',
        routeBasePath: 'starting-point',
        showLastUpdateAuthor: true,
        showLastUpdateTime: true,
      }, 
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'machines',
        path: 'machines',
        routeBasePath: 'machines',
        showLastUpdateAuthor: true,
        showLastUpdateTime: true,
      }, 
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'challenges',
        path: 'challenges',
        routeBasePath: 'challenges',
        showLastUpdateAuthor: true,
        showLastUpdateTime: true,
      }, 
    ],
],

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        pages: {
          path: 'src/pages',
          routeBasePath: "/",
        },
        docs: false,
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        googleAnalytics: {
          trackingID: 'UA-236102779-2',
          anonymizeIP: true,
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      algolia: {
        appId: '1T048HVIXK',
        apiKey: 'c86e379ba98e1b9c79296c46a090ccc8',
        indexName: 'raulsanchezzt',
      },
      colorMode: {
        defaultMode: 'dark',
        disableSwitch: true,
        respectPrefersColorScheme: false,
      },
      image: 'https://imgs.search.brave.com/fyz3x5G4mzUTI4XdmN8ZpEs89c4uALDL7NTKuNkx_YM/rs:fit:900:900:1/g:ce/aHR0cHM6Ly95dDMu/Z2dwaHQuY29tL2Ev/QUFUWEFKejBINGFl/ZVFzT21BeVdOWFU5/V0piLWtSYVFnRlVX/RlEzOUVRPXM5MDAt/Yy1rLWMweGZmZmZm/ZmZmLW5vLXJqLW1v',
      metadata: [
        {
          name: 'description',
          content: 'Hack The Box Write-ups and Walkthroughs'
        }
      ],
      /* announcementBar: {
        id: 'support_us',
        content:
          'We are looking to revamp our docs, please fill <a target="_blank" rel="noopener noreferrer" href="#">this survey</a>',
        backgroundColor: '#fafbfc',
        textColor: '#091E42',
        isCloseable: false,
      }, */
      navbar: {
        title: 'HTB Write-ups',
        logo: {
          alt: 'HTB',
          src: '/icons/hackthebox.svg',
        },
        items: [
          {
            label: "🚩 Starting Point",
            to: '/starting-point/', 
            position: "left",
          },
          {
            label: "📦 Machines",
            to: '/machines/',
            position: "left",
          },
          {
            label: "🏅 Challenges",
            to: '/challenges/',
            position: "left",
          },
          {
            label: "🔗 Tags",
            position: "left",
            items: [
              {
                label: '🚩 Starting Point',
                href: '/starting-point/tags',
              },
              {
                label: '📦 Machines',
                href: '/machines/tags',
              },
              {
                label: '🏅 Challenges',
                href: '/challenges/tags',
              },
            ]
          },
          {
            label: "👤 About me",
            to: 'https://raulsanchezzt.com',
            position: 'left',
          },
        ],
      },
      prism: {
        // Available: dracula, duotoneDark, nightOwl, oceanicNext, okaidia, palenight, shadesOfPurple, synthwave84, vsDark
        darkTheme: require('prism-react-renderer/themes/shadesOfPurple'),
        defaultLanguage: 'python',
      },
      /* footer: {
        logo: {
          alt: '',
          src: '',
          href: '',
          width: 170,
          height: 71,
        },
        copyright: `Copyright © ${new Date().getFullYear()} Raúl Sánchez. Built with Docusaurus.`,
      },
      */
    }),
};

module.exports = config;