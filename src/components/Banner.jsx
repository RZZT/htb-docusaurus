import React from "react";
import Link from "@docusaurus/Link";

function BannerText() {
  return (
    <div className="bannerContent">
      
      <h1>
        Hack The Box Write-ups
      </h1>

      <p>
      This is a collection of Write-ups and Walkthroughs of my adventures through <a target="_blank" href="https://hackthebox.com">Hack The Box</a>. For this I will use a <a target="_blank" href="https://www.kali.org/">Kali Linux</a> virtual machine with bspwm. Check <a target="_blank" href="https://github.com/RaulSanchezzt/kali-bspmw">kali-bspwm</a> if you want to install it with a script or if you want to use my <a target="_blank" href="https://github.com/RaulSanchezzt/dotfiles">dotfiles</a>.
      </p>

      <div className="bannerBtns">
        <Link
          to="https://raulsanchezzt.com"
          style={{ textDecoration: "none" }}
        >
          <button className="banner_btn">
          <img src="/icons/user.svg" />
          About me
          </button>
        </Link>

        <Link
        // https://www.hackthebox.com/badge/image/1009669
          to="https://hackthebox.com/profile/1009669"
          style={{ textDecoration: "none" }}
        >
          <button className="banner_btn">
          <img src="/icons/at.svg" />
          Profile
          </button>
        </Link>
      </div>
    </div>
  );
}

function Banner() {
  return (
    <div className="banner">
      <BannerText />

      <img
        className="bannerImg"
        src="https://media1.giphy.com/media/l41Ys4Fb5r8hcPhde/giphy.gif?cid=790b7611807d2c1fd1f4cc1706bbb78826f15b24a1364764&rid=giphy.gif&ct=g"
        alt="GIF"
      />
    </div>
  );
}

export default Banner;
