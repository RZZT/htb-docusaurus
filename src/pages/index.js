import "../css/variables.css";
import "../css/custom.css";
import "../css/components/home.css";

import React from "react";
import Layout from "@theme/Layout";

import Banner from "../components/Banner";

// Home page

function Home() {
  return (
    <Layout description="Detailed Write-ups and Walktrough of Hack The Box Machines and more.">
      <main className="homepage">
        <Banner />
      </main>
    </Layout>
  );
}

export default function () {
  return <Home />;
}
