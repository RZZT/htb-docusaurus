# Hack The Box Write-ups

> Visit my new [blog](https://blog.raulsanchezzt.com).

This is the page where I used to post articles about **Hack The Box** machines.

![](assests/htb-home.png)

![](assests/htb-machine.png)

# Set-up this web locally

1. Clone this repository

```bash
git clone git@gitlab.com:RZZT/htb-docusaurus.git
```

2. Change the directory

```bash
cd htb-docusaurus
```

3. Install the necessary dependencies

```bash
yarn install
```

4. Start the server

```bash
yarn start
```